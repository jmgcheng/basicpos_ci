<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "c_home";
$route['404_override'] = '';

$route['user/register_form'] = 'c_user/register_form';
$route['user/register'] = 'c_user/register';
$route['user/register_activation_required'] = 'c_user/register_activation_required';
$route['user/register_activation/.+'] = 'c_user/register_activation';
$route['user/update_captcha_register_form_ajx'] = 'c_user/update_captcha_register_form_ajx';
$route['user/login_form'] = 'c_user/login_form';
$route['user/login'] = 'c_user/login';
$route['user/logout'] = 'c_user/logout';
$route['user/read_all/(:any)'] = 'c_user/read_all';
$route['user/read_all'] = 'c_user/read_all';

$route['product/create_form'] = 'c_product/create_form';
$route['product/create'] = 'c_product/create';
$route['product/read_all/(:any)'] = 'c_product/read_all';
$route['product/read_all'] = 'c_product/read_all';
$route['product/read_all_ajx'] = 'c_product/read_all_ajx';
$route['product/read_stocks/(:any)'] = 'c_product/read_stocks';
$route['product/read_stocks'] = 'c_product/read_stocks';
$route['product/update_form/(:any)'] = 'c_product/update_form';
$route['product/update/(:any)'] = 'c_product/update';

$route['purchase_order/create_form'] = 'c_purchase_order/create_form';
$route['purchase_order/create'] = 'c_purchase_order/create';
$route['purchase_order/create_ajx'] = 'c_purchase_order/create_ajx';
$route['purchase_order/read_all/(:any)'] = 'c_purchase_order/read_all';
$route['purchase_order/read_all'] = 'c_purchase_order/read_all';
$route['purchase_order/update_form/(:any)'] = 'c_purchase_order/update_form';
$route['purchase_order/update/(:any)'] = 'c_purchase_order/update';
$route['purchase_order/update_ajx/(:any)'] = 'c_purchase_order/update_ajx';

$route['sales_invoice/create_form'] = 'c_sales_invoice/create_form';
$route['sales_invoice/create'] = 'c_sales_invoice/create';
$route['sales_invoice/create_ajx'] = 'c_sales_invoice/create_ajx';
$route['sales_invoice/read_all/(:any)'] = 'c_sales_invoice/read_all';
$route['sales_invoice/read_all'] = 'c_sales_invoice/read_all';
$route['sales_invoice/update_form/(:any)'] = 'c_sales_invoice/update_form';
$route['sales_invoice/update/(:any)'] = 'c_sales_invoice/update';
$route['sales_invoice/update_ajx/(:any)'] = 'c_sales_invoice/update_ajx';

$route['official_receipt/create_form'] = 'c_official_receipt/create_form';
$route['official_receipt/create'] = 'c_official_receipt/create';
$route['official_receipt/create_ajx'] = 'c_official_receipt/create_ajx';
$route['official_receipt/read_all/(:any)'] = 'c_official_receipt/read_all';
$route['official_receipt/read_all'] = 'c_official_receipt/read_all';
$route['official_receipt/update_form/(:any)'] = 'c_official_receipt/update_form';
$route['official_receipt/update/(:any)'] = 'c_official_receipt/update';
$route['official_receipt/update_ajx/(:any)'] = 'c_official_receipt/update_ajx';

$route['missed_purchase_order/create_form'] = 'c_missed_purchase_order/create_form';
$route['missed_purchase_order/create'] = 'c_missed_purchase_order/create';
$route['missed_purchase_order/create_ajx'] = 'c_missed_purchase_order/create_ajx';
$route['missed_purchase_order/read_all/(:any)'] = 'c_missed_purchase_order/read_all';
$route['missed_purchase_order/read_all'] = 'c_missed_purchase_order/read_all';
$route['missed_purchase_order/update_form/(:any)'] = 'c_missed_purchase_order/update_form';
$route['missed_purchase_order/update/(:any)'] = 'c_missed_purchase_order/update';
$route['missed_purchase_order/update_ajx/(:any)'] = 'c_missed_purchase_order/update_ajx';

$route['wrong_send_purchase_order/create_form'] = 'c_wrong_send_purchase_order/create_form';
$route['wrong_send_purchase_order/create'] = 'c_wrong_send_purchase_order/create';
$route['wrong_send_purchase_order/create_ajx'] = 'c_wrong_send_purchase_order/create_ajx';
$route['wrong_send_purchase_order/read_all/(:any)'] = 'c_wrong_send_purchase_order/read_all';
$route['wrong_send_purchase_order/read_all'] = 'c_wrong_send_purchase_order/read_all';
$route['wrong_send_purchase_order/update_form/(:any)'] = 'c_wrong_send_purchase_order/update_form';
$route['wrong_send_purchase_order/update/(:any)'] = 'c_wrong_send_purchase_order/update';
$route['wrong_send_purchase_order/update_ajx/(:any)'] = 'c_wrong_send_purchase_order/update_ajx';

$route['included_inventory/create_form'] = 'c_included_inventory/create_form';
$route['included_inventory/create'] = 'c_included_inventory/create';
$route['included_inventory/create_ajx'] = 'c_included_inventory/create_ajx';
$route['included_inventory/read_all/(:any)'] = 'c_included_inventory/read_all';
$route['included_inventory/read_all'] = 'c_included_inventory/read_all';
$route['included_inventory/update_form/(:any)'] = 'c_included_inventory/update_form';
$route['included_inventory/update/(:any)'] = 'c_included_inventory/update';
$route['included_inventory/update_ajx/(:any)'] = 'c_included_inventory/update_ajx';

$route['excluded_inventory/create_form'] = 'c_excluded_inventory/create_form';
$route['excluded_inventory/create'] = 'c_excluded_inventory/create';
$route['excluded_inventory/create_ajx'] = 'c_excluded_inventory/create_ajx';
$route['excluded_inventory/read_all/(:any)'] = 'c_excluded_inventory/read_all';
$route['excluded_inventory/read_all'] = 'c_excluded_inventory/read_all';
$route['excluded_inventory/update_form/(:any)'] = 'c_excluded_inventory/update_form';
$route['excluded_inventory/update/(:any)'] = 'c_excluded_inventory/update';
$route['excluded_inventory/update_ajx/(:any)'] = 'c_excluded_inventory/update_ajx';

$route['delivery_receipt/create_form'] = 'c_delivery_receipt/create_form';
$route['delivery_receipt/create'] = 'c_delivery_receipt/create';
$route['delivery_receipt/create_ajx'] = 'c_delivery_receipt/create_ajx';
$route['delivery_receipt/read_all/(:any)'] = 'c_delivery_receipt/read_all';
$route['delivery_receipt/read_all'] = 'c_delivery_receipt/read_all';
$route['delivery_receipt/update_form/(:any)'] = 'c_delivery_receipt/update_form';
$route['delivery_receipt/update/(:any)'] = 'c_delivery_receipt/update';
$route['delivery_receipt/update_ajx/(:any)'] = 'c_delivery_receipt/update_ajx';

$route['transfer_order/create_form'] = 'c_transfer_order/create_form';
$route['transfer_order/create'] = 'c_transfer_order/create';
$route['transfer_order/create_ajx'] = 'c_transfer_order/create_ajx';
$route['transfer_order/read_all/(:any)'] = 'c_transfer_order/read_all';
$route['transfer_order/read_all'] = 'c_transfer_order/read_all';
$route['transfer_order/update_form/(:any)'] = 'c_transfer_order/update_form';
$route['transfer_order/update/(:any)'] = 'c_transfer_order/update';
$route['transfer_order/update_ajx/(:any)'] = 'c_transfer_order/update_ajx';

$route['received_transfer_order/create_form'] = 'c_received_transfer_order/create_form';
$route['received_transfer_order/create'] = 'c_received_transfer_order/create';
$route['received_transfer_order/create_ajx'] = 'c_received_transfer_order/create_ajx';
$route['received_transfer_order/read_all/(:any)'] = 'c_received_transfer_order/read_all';
$route['received_transfer_order/read_all'] = 'c_received_transfer_order/read_all';
$route['received_transfer_order/update_form/(:any)'] = 'c_received_transfer_order/update_form';
$route['received_transfer_order/update/(:any)'] = 'c_received_transfer_order/update';
$route['received_transfer_order/update_ajx/(:any)'] = 'c_received_transfer_order/update_ajx';

$route['received_purchase_order/create_form'] = 'c_received_purchase_order/create_form';
$route['received_purchase_order/create'] = 'c_received_purchase_order/create';
$route['received_purchase_order/create_ajx'] = 'c_received_purchase_order/create_ajx';
$route['received_purchase_order/read_all/(:any)'] = 'c_received_purchase_order/read_all';
$route['received_purchase_order/read_all'] = 'c_received_purchase_order/read_all';
$route['received_purchase_order/update_form/(:any)'] = 'c_received_purchase_order/update_form';
$route['received_purchase_order/update/(:any)'] = 'c_received_purchase_order/update';
$route['received_purchase_order/update_ajx/(:any)'] = 'c_received_purchase_order/update_ajx';




















/*
$route['default_controller'] = "c_default";
$route['404_override'] = '';

$route['a/change_captcha_image'] = 'ajx_actions_v1/change_captcha_image';

$route['user/register_form'] = 'c_user/register_form';
$route['user/register'] = 'c_user/register';
$route['user/register_activation_required'] = 'c_user/register_activation_required';
$route['user/register_activation/.+'] = 'c_user/register_activation';
$route['user/login_form'] = 'c_user/login_form';
$route['user/login'] = 'c_user/login';
$route['user/logout'] = 'c_user/logout';
$route['user/show_user/(:any)'] = 'c_user/show_user';
$route['user/show_user'] = 'c_user/show_user';

$route['product/add_form'] = 'c_product/add_form';
$route['product/add'] = 'c_product/add';
$route['product/update_form/(:any)'] = 'c_product/update_form';
$route['product/update/(:any)'] = 'c_product/update';
$route['product/show_all/(:any)'] = 'c_product/show_all';
$route['product/show_all'] = 'c_product/show_all';
$route['product/ajx_read_all'] = 'c_product/ajx_read_all';

$route['product/read_stocks_quantity/(:any)'] = 'c_product/read_stocks_quantity';
$route['product/read_stocks_quantity'] = 'c_product/read_stocks_quantity';

$route['purchase_order/create_form'] = 'c_purchase_order/create_form';
$route['purchase_order/create'] = 'c_purchase_order/create';
$route['purchase_order/show_all/(:any)'] = 'c_purchase_order/show_all';
$route['purchase_order/show_all'] = 'c_purchase_order/show_all';

$route['purchase_order/ajx_create'] = 'c_purchase_order/ajx_create';
*/
















/*
css
	type noun (version)
	css_style
js
	type action noun
	js_detect_browser
	js_register_user
	js_update_user
	js_update_purchase_order
normal scripts
	noun
	browser_detection
controllers
	type noun
	c_user
	c_inventory
models
	type noun
	m_user
views
	type noun
	v_user_profile
	v_user_registration
normal php files
	noun
	user
	inventory
element name
	elementType noun (action) id|cls (version)
	div_form_cls
	div_table_cls
	segment_user_list_id
	frm_user_registration
	txt_user_registraion_username
	txt_user_registraion_password
	frm_purchase_order_update
	opt_purchase_order_detail_add
variable name
	type noun
	i_counter
	b_stop
functions name
	action noun
	get_user()
	count_inventory()
	create_
	read_
	update_
	delete_
	register_form
*/








/* End of file routes.php */
/* Location: ./application/config/routes.php */