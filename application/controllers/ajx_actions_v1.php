<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajx_actions_v1 extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function change_captcha_image()
	{
		$this->load->library(array('session'));
		$this->load->helper(array('captcha'));
		
		$a_ci_captha_attributes = array(
			'word'	 => random_string('alpha', 4),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url() . '/captcha/',
			'img_width'	 => 90,
			'img_height' => 45,
			'font_path'	 => './fonts/MyriadPro-Regular.otf',
			'expiration' => 500
		);
		$a_captcha_details = create_captcha($a_ci_captha_attributes);
		$s_sample_form_captcha =  $a_captcha_details['image'];
		$a_session_captcha_details = array(
			'a_session_captcha_details'  => $a_captcha_details
		);
		$this->session->set_userdata($a_session_captcha_details);

		echo json_encode( 
			array(
					's_act_result' => 'success',
					's_captcha_img' => $s_sample_form_captcha
				)
		);

	}
	
}