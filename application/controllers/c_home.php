<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_home extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		
		//======================================================
		
		$a_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_header_data, true);
		
		
		$a_main_data = array();
		$s_view_main = $this->load->view('templates_v1/v_home_v1', $a_main_data, true);
		
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
}

/* End of file */
/* Location: ./application/controllers/ */