<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_product extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	* is_existing
	* @desc		addon callback function for form validation. Check if data is existing.
				Opposite to what is_unique[...] is used
	*
	**/
	public function is_existing( $m_var, $s_table_detail )
	{
		//= Declare Start-Up Variables Here ====================
		$b_result = FALSE;
		$a_table_detail = array();
		
		//======================================================
		
		
		//======================================================
		$this->load->library('l_def_sql');
	
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_table_detail = explode('.', $s_table_detail);
	
	
		$a_query_where = array();
		$a_query_params = array();
		array_push( $a_query_where, array( 's_field' => $a_table_detail[0] . '.' . $a_table_detail[1], 'a_data' => $m_var ) );
		$a_query_params['a_where'] = $a_query_where;
		$a_query_params['s_table_fields'] = $a_table_detail[0] . '.' . $a_table_detail[1];
		$a_query_params['s_table_name'] = $a_table_detail[0];
		$a_product_duplicate_result = array();
		$a_product_duplicate_result = $this->l_def_sql->read_data( $a_query_params );
		
		
		if( isset($a_product_duplicate_result) && !empty($a_product_duplicate_result) )
		{
			$b_result = TRUE;
		}
		else
		{
			$this->form_validation->set_message('is_existing', 'The value entered at %s does not existing in the database');
		}
		
		return $b_result;
		//======================================================
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		
	}
		
	
	/**
	* create_form
	* @desc		
	*
	**/
	public function create_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			Get product_colors
		*/
		$a_product_colors_query_where = array();
		$a_product_colors_query_params = array();
		$a_product_colors_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_colors_fields;
		$a_product_colors_query_params['s_table_name'] = 'product_colors';
		$a_product_colors_result = $this->l_def_sql->read_data( $a_product_colors_query_params );
		
		
		/*
			Get product_dimensions
		*/
		$a_product_dimensions_query_where = array();
		$a_product_dimensions_query_params = array();
		$a_product_dimensions_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_dimensions_fields;
		$a_product_dimensions_query_params['s_table_name'] = 'product_dimensions';
		$a_product_dimensions_result = $this->l_def_sql->read_data( $a_product_dimensions_query_params );
		
		
		/*
			Get product_units
		*/
		$a_product_units_query_where = array();
		$a_product_units_query_params = array();
		$a_product_units_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_units_fields;
		$a_product_units_query_params['s_table_name'] = 'product_units';
		$a_product_units_result = $this->l_def_sql->read_data( $a_product_units_query_params );
		
		
		/*
			Get product_status_names
		*/
		$a_product_status_names_query_where = array();
		$a_product_status_names_query_params = array();
		$a_product_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_status_names_fields;
		$a_product_status_names_query_params['s_table_name'] = 'product_status_names';
		$a_product_status_names_result = $this->l_def_sql->read_data( $a_product_status_names_query_params );
		
		
		//======================================================
		
		
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_view_header_data = array();
		$a_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_form_notice'] = $a_form_notice;
		$a_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$a_view_main_data['a_product_colors_result'] = $a_product_colors_result;
		$a_view_main_data['a_product_dimensions_result'] = $a_product_dimensions_result;
		$a_view_main_data['a_product_units_result'] = $a_product_units_result;
		$a_view_main_data['a_product_status_names_result'] = $a_product_status_names_result;
		$s_view_main = $this->load->view('templates_v1/v_product_create_form_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	/**
	* create
	* @desc		
	*
	**/
	public function create()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_product_create_name', 'Name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('txt_product_create_model', 'Model', 'trim|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('opt_product_create_dimension', 'Dimension', 'trim|required|xss_clean|callback_is_existing[product_dimensions.i_id]');
			$this->form_validation->set_rules('opt_product_create_color', 'Color', 'trim|required|xss_clean|callback_is_existing[product_colors.i_id]');
			$this->form_validation->set_rules('opt_product_create_unit', 'Unit', 'trim|required|xss_clean|callback_is_existing[product_units.i_id]');
			$this->form_validation->set_rules('txt_product_create_description', 'Description', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('opt_product_create_status', 'Status', 'trim|required|xss_clean|callback_is_existing[product_status_names.i_id]');
			
			/*
				Check if product with same details already exist
			*/
			$a_p_query_where = array();
			$a_p_query_params = array();
			array_push( $a_p_query_where, array( 's_field' => 'products.s_name', 'a_data' => $_POST['txt_product_create_name'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.s_model', 'a_data' => $_POST['txt_product_create_model'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pd_id', 'a_data' => $_POST['opt_product_create_dimension'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pc_id', 'a_data' => $_POST['opt_product_create_color'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pu_id', 'a_data' => $_POST['opt_product_create_unit'] ) );
			$a_p_query_params['a_where'] = $a_p_query_where;
			$a_p_query_params['s_table_fields'] = $this->m_def_table_fields->s_products_fields;
			$a_p_query_params['s_table_name'] = 'products';
			/**/
			$b_product_duplicate_result = false;
			$a_product_duplicate_result = array();
			$a_product_duplicate_result = $this->l_def_sql->read_data( $a_p_query_params );
			if( isset($a_product_duplicate_result) && !empty($a_product_duplicate_result) )
			{
				$b_product_duplicate_result = true;
			}
			
			
			if(		$this->form_validation->run() == FALSE 
				||	$b_product_duplicate_result == TRUE
			)
			{
				if( $b_product_duplicate_result == TRUE )
				{
					array_push( $a_site_response_error, 'This Product with the same details already exist.' );
				}
				
				$a_form_notice['s_txt_product_create_name_error'] = form_error('txt_product_create_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_create_name_error']) && !empty($a_form_notice['s_txt_product_create_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_create_name_error'] );
				}
				$a_form_notice['s_txt_product_create_model_error'] = form_error('txt_product_create_model', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_create_model_error']) && !empty($a_form_notice['s_txt_product_create_model_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_create_model_error'] );
				}
				$a_form_notice['s_opt_product_create_dimension_error'] = form_error('opt_product_create_dimension', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_create_dimension_error']) && !empty($a_form_notice['s_opt_product_create_dimension_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_create_dimension_error'] );
				}
				$a_form_notice['s_opt_product_create_color_error'] = form_error('opt_product_create_color', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_create_color_error']) && !empty($a_form_notice['s_opt_product_create_color_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_create_color_error'] );
				}
				$a_form_notice['s_opt_product_create_unit_error'] = form_error('opt_product_create_unit', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_create_unit_error']) && !empty($a_form_notice['s_opt_product_create_unit_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_create_unit_error'] );
				}
				$a_form_notice['s_txt_product_create_description_error'] = form_error('txt_product_create_description', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_create_description_error']) && !empty($a_form_notice['s_txt_product_create_description_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_create_description_error'] );
				}
				$a_form_notice['s_opt_product_create_status_error'] = form_error('opt_product_create_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_create_status_error']) && !empty($a_form_notice['s_opt_product_create_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_create_status_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->create_form($a_form_notice);
			}
			else
			{
				$a_new_data = array();
				$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
				$a_new_data['s_name'] = $_POST['txt_product_create_name'];
				$a_new_data['s_model'] = $_POST['txt_product_create_model'];
				$a_new_data['s_dimension'] = '';
				$a_new_data['s_color'] = '';
				$a_new_data['s_unit'] = '';
				$a_new_data['s_description'] = $_POST['txt_product_create_description'];
				$a_new_data['i_psn_id'] = $_POST['opt_product_create_status'];
				$a_new_data['i_pc_id'] = $_POST['opt_product_create_color'];
				$a_new_data['i_pd_id'] = $_POST['opt_product_create_dimension'];
				$a_new_data['i_pu_id'] = $_POST['opt_product_create_unit'];
				
				$a_add_product_result = array();
				$a_add_product_params['s_table_name'] = 'products';
				$a_add_product_params['a_new_data'] = $a_new_data;
				$a_add_product_result = $this->l_def_sql->create_data( $a_add_product_params );
				
				if( 	isset($a_add_product_result) && !empty($a_add_product_result) 
					&&	array_key_exists("i_sql_result", $a_add_product_result)
					&& 	$a_add_product_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_success, 'Successful Adding Product.' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->create_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Adding Product. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->create_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'product/create_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* read_all
	* @desc		
	*
	**/
	public function read_all()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order', 'status');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 100; }
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 'products.i_id'; }
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 1; } // 1 = Activated, 2 = DeActivated
		
		
		/**/
		$a_p_query_where = array();
		$a_p_query_table_join = array();
		$a_p_query_order_by = array();
		$a_p_query_limit = array();
		$a_p_query_params = array();
		
		array_push( $a_p_query_where, array( 's_field' => 'products.i_psn_id', 'a_data' => $a_assoc_uri['status'] ) );
		array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_status_names', 's_table_join_condition' => 'product_status_names.i_id = products.i_psn_id' ) );
		array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_colors', 's_table_join_condition' => 'product_colors.i_id = products.i_pc_id' ) );
		array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_dimensions', 's_table_join_condition' => 'product_dimensions.i_id = products.i_pd_id' ) );
		array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_units', 's_table_join_condition' => 'product_units.i_id = products.i_pu_id' ) );
		array_push( $a_p_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
		$a_p_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
		$a_p_query_params['a_where'] = $a_p_query_where;
		$a_p_query_params['a_order_by'] = $a_p_query_order_by;
		$a_p_query_params['a_limit'] = $a_p_query_limit;
		$a_p_query_params['a_table_join'] = $a_p_query_table_join;
		$a_p_query_params['s_table_fields'] = $this->m_def_table_fields->s_products_fields . ', ' . $this->m_def_table_fields->s_product_status_names_fields . ', ' . $this->m_def_table_fields->s_product_colors_fields. ', ' . $this->m_def_table_fields->s_product_dimensions_fields. ', ' . $this->m_def_table_fields->s_product_units_fields;
		$a_p_query_params['s_table_name'] = 'products';
		
		
		/**/
		$a_product_result = $this->l_def_sql->read_data( $a_p_query_params );
		$a_product_count_result = $this->l_def_sql->read_count_data( $a_p_query_params );
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'product/read_all/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/status/' .$a_assoc_uri['status']. '/offset/';
		$a_pagination_config['total_rows'] = $a_product_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 20;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//======================================================
		
		
		$a_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$a_view_main_data['a_product_result'] = $a_product_result;
		$a_view_main_data['a_product_count_result'] = $a_product_count_result;
		$a_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main = $this->load->view('templates_v1/v_product_read_all', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	/**
	* read_all_ajx
	* @desc		
	*
	**/
	public function read_all_ajx()
	{
		//= Declare Start-Up Variables Here ====================
		$a_result = array();
		
		//======================================================
		
		
		//======================================================
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{

			/**/
			$a_p_query_where = array();
			$a_p_query_table_join = array();
			$a_p_query_order_by = array();
			$a_p_query_limit = array();
			$a_p_query_params = array();
			array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_status_names', 's_table_join_condition' => 'product_status_names.i_id = products.i_psn_id' ) );
			array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_colors', 's_table_join_condition' => 'product_colors.i_id = products.i_pc_id' ) );
			array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_dimensions', 's_table_join_condition' => 'product_dimensions.i_id = products.i_pd_id' ) );
			array_push( $a_p_query_table_join, array( 's_table_join_name' => 'product_units', 's_table_join_condition' => 'product_units.i_id = products.i_pu_id' ) );
			array_push( $a_p_query_order_by, array( 's_field' => 'products.i_id', 'a_data' => 'asc' ) );
			$a_p_query_limit = array('i_limit' => 1000, 'i_offset' => 0);
			$a_p_query_params['a_order_by'] = $a_p_query_order_by;
			$a_p_query_params['a_limit'] = $a_p_query_limit;
			$a_p_query_params['a_table_join'] = $a_p_query_table_join;
			$a_p_query_params['s_table_fields'] = $this->m_def_table_fields->s_products_fields . ', ' . $this->m_def_table_fields->s_product_status_names_fields . ', ' . $this->m_def_table_fields->s_product_colors_fields. ', ' . $this->m_def_table_fields->s_product_dimensions_fields. ', ' . $this->m_def_table_fields->s_product_units_fields;
			$a_p_query_params['s_table_name'] = 'products';
			
			/**/
			$a_product_result = array();
			$a_product_result = $this->l_def_sql->read_data( $a_p_query_params );
			
			$a_result['s_result'] = 'success';
			$a_result['a_product_result'] = $a_product_result;
		}
		else
		{ 
			$a_result['s_result'] = 'fail';
			$a_result['s_message'] = 'No Rights';
		}
		
		echo json_encode( $a_result );
		//======================================================
	}
	
	
	
	/**
	* update_form
	* @desc		
	*
	**/
	public function update_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('product_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['product_id']) || empty($a_assoc_uri['product_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		/**/
		$a_p_query_where = array();
		$a_p_query_params = array();
		array_push( $a_p_query_where, array( 's_field' => 'products.i_id', 'a_data' => $a_assoc_uri['product_id'] ) );
		$a_p_query_params['a_where'] = $a_p_query_where;
		$a_p_query_params['s_table_fields'] = $this->m_def_table_fields->s_products_fields;
		$a_p_query_params['s_table_name'] = 'products';

		/**/
		$a_product_row_result = array();
		$a_product_result = $this->l_def_sql->read_data( $a_p_query_params );
		if( isset($a_product_result) && !empty($a_product_result) )
		{ $a_product_row_result = $a_product_result[0]; }
		else
		{ redirect( base_url(), 'refresh'); }
		
		
		/*
			Get product_colors
		*/
		$a_product_colors_query_where = array();
		$a_product_colors_query_params = array();
		$a_product_colors_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_colors_fields;
		$a_product_colors_query_params['s_table_name'] = 'product_colors';
		$a_product_colors_result = $this->l_def_sql->read_data( $a_product_colors_query_params );
		
		
		/*
			Get product_dimensions
		*/
		$a_product_dimensions_query_where = array();
		$a_product_dimensions_query_params = array();
		$a_product_dimensions_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_dimensions_fields;
		$a_product_dimensions_query_params['s_table_name'] = 'product_dimensions';
		$a_product_dimensions_result = $this->l_def_sql->read_data( $a_product_dimensions_query_params );
		
		
		/*
			Get product_units
		*/
		$a_product_units_query_where = array();
		$a_product_units_query_params = array();
		$a_product_units_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_units_fields;
		$a_product_units_query_params['s_table_name'] = 'product_units';
		$a_product_units_result = $this->l_def_sql->read_data( $a_product_units_query_params );
		
		
		/*
			Get product_status_names
		*/
		$a_product_status_names_query_where = array();
		$a_product_status_names_query_params = array();
		$a_product_status_names_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_status_names_fields;
		$a_product_status_names_query_params['s_table_name'] = 'product_status_names';
		$a_product_status_names_result = $this->l_def_sql->read_data( $a_product_status_names_query_params );
		
		
		
		
		
		
		//======================================================
		
		
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_view_header_data = array();
		$a_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_form_notice'] = $a_form_notice;
		$a_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$a_view_main_data['a_product_colors_result'] = $a_product_colors_result;
		$a_view_main_data['a_product_dimensions_result'] = $a_product_dimensions_result;
		$a_view_main_data['a_product_units_result'] = $a_product_units_result;
		$a_view_main_data['a_product_status_names_result'] = $a_product_status_names_result;
		$a_view_main_data['a_product_result'] = $a_product_result;
		$a_view_main_data['a_product_row_result'] = $a_product_row_result;
		$s_view_main = $this->load->view('templates_v1/v_product_update_form_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}

	
	/**
	* update
	* @desc		
	*
	**/
	public function update()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('product_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['product_id']) || empty($a_assoc_uri['product_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_product_update_name', 'Name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('txt_product_update_model', 'Model', 'trim|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('opt_product_update_dimension', 'Dimension', 'trim|required|xss_clean|callback_is_existing[product_dimensions.i_id]');
			$this->form_validation->set_rules('opt_product_update_color', 'Color', 'trim|required|xss_clean|callback_is_existing[product_colors.i_id]');
			$this->form_validation->set_rules('opt_product_update_unit', 'Unit', 'trim|required|xss_clean|callback_is_existing[product_units.i_id]');
			$this->form_validation->set_rules('txt_product_update_description', 'Description', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('txt_product_update_qty_low_alert', 'QTY Low Alert', 'trim|required|xss_clean|numeric');
			$this->form_validation->set_rules('opt_product_update_status', 'Status', 'trim|required|xss_clean|callback_is_existing[product_status_names.i_id]');
			
			
			/*
				Check if product with same details already exist
			*/
			$a_p_query_where = array();
			$a_p_query_params = array();
			array_push( $a_p_query_where, array( 's_field' => 'products.i_id !=', 'a_data' => $a_assoc_uri['product_id'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.s_name', 'a_data' => $_POST['txt_product_update_name'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.s_model', 'a_data' => $_POST['txt_product_update_model'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pd_id', 'a_data' => $_POST['opt_product_update_dimension'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pc_id', 'a_data' => $_POST['opt_product_update_color'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_pu_id', 'a_data' => $_POST['opt_product_update_unit'] ) );
			array_push( $a_p_query_where, array( 's_field' => 'products.i_psn_id', 'a_data' => $_POST['opt_product_update_status'] ) );
			$a_p_query_params['a_where'] = $a_p_query_where;
			$a_p_query_params['s_table_fields'] = $this->m_def_table_fields->s_products_fields;
			$a_p_query_params['s_table_name'] = 'products';
			/**/
			$b_product_duplicate_result = false;
			$a_product_duplicate_result = array();
			$a_product_duplicate_result = $this->l_def_sql->read_data( $a_p_query_params );
			if( isset($a_product_duplicate_result) && !empty($a_product_duplicate_result) )
			{
				$b_product_duplicate_result = true;
			}
			
			
			if( 	$this->form_validation->run() == FALSE 
				||	$b_product_duplicate_result == TRUE
			)
			{
				if( $b_product_duplicate_result == TRUE )
				{
					array_push( $a_site_response_error, 'This Product with the same details already exist in the database.' );
				}
				
				$a_form_notice['s_txt_product_update_name_error'] = form_error('txt_product_update_name', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_update_name_error']) && !empty($a_form_notice['s_txt_product_update_name_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_update_name_error'] );
				}
				$a_form_notice['s_txt_product_update_model_error'] = form_error('txt_product_update_model', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_update_model_error']) && !empty($a_form_notice['s_txt_product_update_model_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_update_model_error'] );
				}
				$a_form_notice['s_opt_product_update_dimension_error'] = form_error('opt_product_update_dimension', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_update_dimension_error']) && !empty($a_form_notice['s_opt_product_update_dimension_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_update_dimension_error'] );
				}
				$a_form_notice['s_opt_product_update_color_error'] = form_error('opt_product_update_color', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_update_color_error']) && !empty($a_form_notice['s_opt_product_update_color_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_update_color_error'] );
				}
				$a_form_notice['s_opt_product_update_unit_error'] = form_error('opt_product_update_unit', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_update_unit_error']) && !empty($a_form_notice['s_opt_product_update_unit_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_update_unit_error'] );
				}
				$a_form_notice['s_txt_product_update_description_error'] = form_error('txt_product_update_description', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_update_description_error']) && !empty($a_form_notice['s_txt_product_update_description_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_update_description_error'] );
				}
				$a_form_notice['s_txt_product_update_qty_low_alert_error'] = form_error('txt_product_update_qty_low_alert', ' ', ' ');
				if( isset($a_form_notice['s_txt_product_update_qty_low_alert_error']) && !empty($a_form_notice['s_txt_product_update_qty_low_alert_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_product_update_qty_low_alert_error'] );
				}
				$a_form_notice['s_opt_product_update_status_error'] = form_error('opt_product_update_status', ' ', ' ');
				if( isset($a_form_notice['s_opt_product_update_status_error']) && !empty($a_form_notice['s_opt_product_update_status_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_opt_product_update_status_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->update_form($a_form_notice);
			}
			else
			{
				/*
					update products table
				*/
				$a_update_p_query_where = array();
				$a_update_p_query_params = array();
				array_push( $a_update_p_query_where, array( 's_field' => 'products.i_id', 'a_data' => $a_assoc_uri['product_id'] ) );
				$a_update_data = array();
				$a_update_data['s_name'] = $_POST['txt_product_update_name'];
				$a_update_data['s_model'] = $_POST['txt_product_update_model'];
				$a_update_data['s_description'] = $_POST['txt_product_update_description'];
				$a_update_data['i_qty_low_alert'] = $_POST['txt_product_update_qty_low_alert'];
				$a_update_data['i_psn_id'] = $_POST['opt_product_update_status'];
				$a_update_data['i_pc_id'] = $_POST['opt_product_update_color'];
				$a_update_data['i_pd_id'] = $_POST['opt_product_update_dimension'];
				$a_update_data['i_pu_id'] = $_POST['opt_product_update_unit'];
				$a_update_p_query_params['a_where'] = $a_update_p_query_where;
				$a_update_p_query_params['a_update_data'] = $a_update_data;
				$a_update_p_query_params['s_table_name'] = 'products';
				$a_update_product_result = $this->l_def_sql->update_data( $a_update_p_query_params );
				
				if( 	isset($a_update_product_result) && !empty($a_update_product_result) 
					&&	array_key_exists("i_sql_result", $a_update_product_result)
					&& 	$a_update_product_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_success, 'Successful Updating Product.' );
					$a_form_notice['a_site_response_success'] = $a_site_response_success;
					$this->update_form($a_form_notice);
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Updating Product. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->update_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'product/update_form/', 'refresh');
		}
		//======================================================
	}
	
	
	
	/**
	* read_stocks
	* @desc		
	*
	**/
	public function read_stocks()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library('l_def_sql');
		$this->load->library('l_stocks_quantity');
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 3, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			at model
			$a_params['i_pc_id'] = 0;
			$a_params['i_pd_id'] = 0;
			$a_params['i_id_id'] = 0;
			$a_params['i_qa'] = 0;
			$a_params['i_psn_id'] = 0;
			$a_params['s_sort'] = '';
			$a_params['s_order'] = '';
			$a_params['i_limit'] = 500;
			$a_params['i_offset'] = 0;
		*/
	
		/*
			parse uri first
		*/
		$a_expected_uri = array('i_pc_id', 'i_pd_id', 'i_id_id', 'i_qa', 'i_psn_id', 's_sort', 's_order', 'i_limit', 'i_offset');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		/*
			check if filter is involved
		*/
		if( isset($_POST['opt_filter_read_stocks_color']) && !empty($_POST['opt_filter_read_stocks_color']) )
		{ 
			$a_assoc_uri['i_pc_id'] = $_POST['opt_filter_read_stocks_color']; 
		}
		if( isset($_POST['opt_filter_read_stocks_dimension']) && !empty($_POST['opt_filter_read_stocks_dimension']) )
		{ 
			$a_assoc_uri['i_pd_id'] = $_POST['opt_filter_read_stocks_dimension']; 
		}
		if( isset($_POST['opt_filter_read_stocks_depot_id']) && !empty($_POST['opt_filter_read_stocks_depot_id']) )
		{ 
			$a_assoc_uri['i_id_id'] = $_POST['opt_filter_read_stocks_depot_id']; 
		}
		if( isset($_POST['opt_filter_read_stocks_qa']) && !empty($_POST['opt_filter_read_stocks_qa']) )
		{ 
			$a_assoc_uri['i_qa'] = $_POST['opt_filter_read_stocks_qa']; 
		}
		if( isset($_POST['opt_filter_read_stocks_psn']) && !empty($_POST['opt_filter_read_stocks_psn']) )
		{ 
			$a_assoc_uri['i_psn_id'] = $_POST['opt_filter_read_stocks_psn']; 
		}
		if( isset($_POST['opt_filter_read_stocks_sort']) && !empty($_POST['opt_filter_read_stocks_sort']) )
		{ 
			$a_assoc_uri['s_sort'] = $_POST['opt_filter_read_stocks_sort']; 
		}
		if( isset($_POST['opt_filter_read_stocks_order']) && !empty($_POST['opt_filter_read_stocks_order']) )
		{ 
			$a_assoc_uri['s_order'] = $_POST['opt_filter_read_stocks_order']; 
		}
		if( isset($_POST['txt_filter_read_stocks_limit']) && !empty($_POST['txt_filter_read_stocks_limit']) )
		{ 
			$a_assoc_uri['i_limit'] = $_POST['txt_filter_read_stocks_limit']; 
		}
		if( isset($_POST['i_offset']) && !empty($_POST['i_offset']) )
		{ 
			$a_assoc_uri['i_offset'] = $_POST['i_offset']; 
		}
		
		/*
			ready assoc before using sql
		*/
		$a_expected_sort = array(''=>'Default');
		$a_expected_order = array(''=>'Default', 'ASC'=>'ASC', 'DESC'=>'DESC');
		$a_expected_qa = array('0'=>'0', '12'=>'12', '24'=>'24', '36'=>'36', '48'=>'48');
		$a_expected_psn = array('0'=>'All', '1'=>'Activated', '2'=>'Deactivated');
		
		if( !isset($a_assoc_uri['i_pc_id']) || empty($a_assoc_uri['i_pc_id']) )
		{ $a_assoc_uri['i_pc_id'] = 0; }
		if( !isset($a_assoc_uri['i_pd_id']) || empty($a_assoc_uri['i_pd_id']) )
		{ $a_assoc_uri['i_pd_id'] = 0; }
		if( !isset($a_assoc_uri['i_id_id']) || empty($a_assoc_uri['i_id_id']) )
		{ $a_assoc_uri['i_id_id'] = 0; }
		if( !isset($a_assoc_uri['i_qa']) || empty($a_assoc_uri['i_qa']) )
		{ $a_assoc_uri['i_qa'] = 0; }
		if( !isset($a_assoc_uri['i_psn_id']) || empty($a_assoc_uri['i_psn_id']) )
		{ $a_assoc_uri['i_psn_id'] = 0; }
		/*
			NOTE:
				in assoc_uri. Dangerous to set a default value of '', URL will not show it.
		*/
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = ''; }
		else
		{
			if( !array_key_exists($a_assoc_uri['sort'], $a_expected_sort) )
			{ $a_assoc_uri['sort'] = ''; }
		}
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = ''; }
		else
		{
			if( !array_key_exists($a_assoc_uri['order'], $a_expected_order) )
			{ $a_assoc_uri['order'] = ''; }
		}
		if( !isset($a_assoc_uri['i_limit']) || empty($a_assoc_uri['i_limit']) )
		{ $a_assoc_uri['i_limit'] = 500; }
		if( !isset($a_assoc_uri['i_offset']) || empty($a_assoc_uri['i_offset']) )
		{ $a_assoc_uri['i_offset'] = 0; }
		
		
		/*
			Get stock quantity
			
			at model
			$a_params['i_pc_id'] = 0;
			$a_params['i_pd_id'] = 0;
			$a_params['i_id_id'] = 0;
			$a_params['i_qa'] = 0;
			$a_params['i_psn_id'] = 0;
			$a_params['s_sort'] = '';
			$a_params['s_order'] = '';
			$a_params['i_limit'] = 500;
			$a_params['i_offset'] = 0;
		*/
		$a_query_params = array();
		$a_query_params['i_pc_id'] = $a_assoc_uri['i_pc_id'];
		$a_query_params['i_pd_id'] = $a_assoc_uri['i_pd_id'];
		$a_query_params['i_id_id'] = $a_assoc_uri['i_id_id'];
		$a_query_params['i_qa'] = $a_assoc_uri['i_qa'];
		$a_query_params['i_psn_id'] = $a_assoc_uri['i_psn_id'];
		$a_query_params['s_sort'] = $a_assoc_uri['s_sort'];
		$a_query_params['s_order'] = $a_assoc_uri['s_order'];
		$a_query_params['i_limit'] = $a_assoc_uri['i_limit'];
		$a_query_params['i_offset'] = $a_assoc_uri['i_offset'];
		$a_stock_quantity_result = $this->l_stocks_quantity->read_stocks_quantity($a_query_params);
		$a_stock_quantity_count_result = $this->l_stocks_quantity->read_count_stocks_quantity($a_query_params);
		
		
		/*
			Paging
			NOTE:
				in assoc_uri. Dangerous to set a default value of '', URL will not show it.
		*/
		$i_page_uri_segment = 20;
		$a_pagination_config['base_url'] = base_url() . 'product/read_stocks/i_pc_id/' . $a_assoc_uri['i_pc_id'] . '/i_pd_id/' . $a_assoc_uri['i_pd_id'] . '/i_id_id/' . $a_assoc_uri['i_id_id'] . '/i_qa/' . $a_assoc_uri['i_qa'] . '/i_psn_id/' . $a_assoc_uri['i_psn_id'] . '/s_sort/' . $a_assoc_uri['s_sort'] . '/s_order/' . $a_assoc_uri['s_order'] . '/i_limit/' . $a_assoc_uri['i_limit'] . '/i_offset/';
		$a_pagination_config['total_rows'] = $a_stock_quantity_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['i_limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 20;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//=======
		
		
		$a_pc_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_colors_fields;
		$a_pc_query_params['s_table_name'] = 'product_colors';
		$a_pc_result = $this->l_def_sql->read_data( $a_pc_query_params );
		
		$a_pd_query_params['s_table_fields'] = $this->m_def_table_fields->s_product_dimensions_fields;
		$a_pd_query_params['s_table_name'] = 'product_dimensions';
		$a_pd_result = $this->l_def_sql->read_data( $a_pd_query_params );
		
		$a_id_query_params['s_table_fields'] = $this->m_def_table_fields->s_inventory_depot_fields;
		$a_id_query_params['s_table_name'] = 'inventory_depot';
		$a_id_result = $this->l_def_sql->read_data( $a_id_query_params );

		//======================================================
		
		
		$a_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$a_view_main_data['a_stock_quantity_result'] = $a_stock_quantity_result;
		$a_view_main_data['a_stock_quantity_count_result'] = $a_stock_quantity_count_result;
		$a_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$a_view_main_data['a_expected_sort'] = $a_expected_sort;
		$a_view_main_data['a_expected_order'] = $a_expected_order;
		$a_view_main_data['a_expected_qa'] = $a_expected_qa;
		$a_view_main_data['a_expected_psn'] = $a_expected_psn;
		$a_view_main_data['a_pc_result'] = $a_pc_result;
		$a_view_main_data['a_pd_result'] = $a_pd_result;
		$a_view_main_data['a_id_result'] = $a_id_result;
		$s_view_main = $this->load->view('templates_v1/v_product_read_stocks_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/angular.min.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');
		array_push($a_current_webpage_inc_js_batch2, 'js/jquery.floatThead.min.js');
		array_push($a_current_webpage_inc_js_batch2, 'js/js_read_stocks.js');
		
		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

/* End of file */
/* Location: ./application/controllers/ */