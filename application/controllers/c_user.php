<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_user extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	

	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		
	}
	
	
	/**
	* update_captcha_register_form_ajx
	* @desc		I need to improve this function someday. This should trap if only URL is sent and stop all action
	*
	**/
	function update_captcha_register_form_ajx()
	{
		$this->load->library(array('session'));
		$this->load->helper(array('captcha'));
		
		$a_ci_captha_attributes = array(
			'word'	 => random_string('alpha', 4),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url() . '/captcha/',
			'img_width'	 => 90,
			'img_height' => 45,
			'font_path'	 => './fonts/MyriadPro-Regular.otf',
			'expiration' => 500
		);
		$a_captcha_details = create_captcha($a_ci_captha_attributes);
		$s_sample_form_captcha =  $a_captcha_details['image'];
		$a_session_captcha_details = array(
			'a_session_captcha_details'  => $a_captcha_details
		);
		$this->session->set_userdata($a_session_captcha_details);

		echo json_encode( 
			array(
					's_act_result' => 'success',
					's_captcha_img' => $s_sample_form_captcha
				)
		);

	}
	
	
	/**
	* register_form
	* @desc		register form
	*
	**/
	public function register_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		
		$s_view_site_responses = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !empty($a_user_details) )
		{
			redirect( base_url() . 'user/logout', 'refresh');
		}
		
		$a_ci_captha_attributes = array(
			'word'	 => random_string('alpha', 4),
			'img_path'	 => './captcha/',
			'img_url'	 => base_url() . 'captcha/',
			'img_width'	 => 90,
			'img_height' => 45,
			'font_path'	 => './fonts/MyriadPro-Regular.otf',
			'expiration' => 500
		);
		$a_captcha_details = create_captcha($a_ci_captha_attributes);
		$s_user_registration_form_captcha =  $a_captcha_details['image'];
		$a_session_captcha_details = array(
			'a_session_captcha_details'  => $a_captcha_details
		);
		$this->session->set_userdata($a_session_captcha_details);
		
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		
		$s_view_header_data = array();
		$s_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['s_user_registration_form_captcha'] = $s_user_registration_form_captcha;
		$s_view_main_data['a_form_notice'] = $a_form_notice;
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main = $this->load->view('templates_v1/v_user_registration_form_v1', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');
		array_push($a_current_webpage_inc_js_batch2, 'js/js_register_user.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
		
	
	/**
	* register
	* @desc		register
	*
	**/
	public function register()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		
		$this->load->library('l_user');
		$this->load->library('l_def_sql');
		
		
		$this->load->helper(array('captcha','form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !empty($a_user_details) )
		{
			redirect( base_url() . 'user/logout', 'refresh');
		}
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_user_registration_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean|is_unique[users.s_username]');
			$this->form_validation->set_rules('txt_user_registration_email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[users.s_email]');
			$this->form_validation->set_rules('txt_user_registration_password', 'Password', 'trim|required|matches[txt_user_registration_password_conf]|md5|xss_clean');
			$this->form_validation->set_rules('txt_user_registration_password_conf', 'Password Confirmation', 'trim|required|xss_clean');
			$this->form_validation->set_rules('txt_user_registration_captcha', 'Captcha', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_user_registration_username_error'] = form_error('txt_user_registration_username', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_username_error']) && !empty($a_form_notice['s_txt_user_registration_username_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_username_error'] );
				}
				$a_form_notice['s_txt_user_registration_email_error'] = form_error('txt_user_registration_email', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_email_error']) && !empty($a_form_notice['s_txt_user_registration_email_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_email_error'] );
				}
				$a_form_notice['s_txt_user_registration_password_error'] = form_error('txt_user_registration_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_password_error']) && !empty($a_form_notice['s_txt_user_registration_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_password_error'] );
				}
				$a_form_notice['s_txt_user_registration_password_conf_error'] = form_error('txt_user_registration_password_conf', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_password_conf_error']) && !empty($a_form_notice['s_txt_user_registration_password_conf_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_password_conf_error'] );
				}
				$a_form_notice['s_txt_user_registration_captcha_error'] = form_error('txt_user_registration_captcha', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_captcha_error']) && !empty($a_form_notice['s_txt_user_registration_captcha_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_captcha_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;

				$this->register_form($a_form_notice);
			}
			else
			{
				$a_old_session_captcha_details = $this->session->userdata('a_session_captcha_details');
				if( isset($a_old_session_captcha_details) && ( $_POST['txt_user_registration_captcha'] == $a_old_session_captcha_details['word'] ) )
				{
					$s_unique_key = md5( ( strtotime('now') . $_POST['txt_user_registration_username'] ) );
					
					$a_new_data = array();
					$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
					$a_new_data['s_email'] = $_POST['txt_user_registration_email'];
					$a_new_data['s_username'] = $_POST['txt_user_registration_username']; 
					$a_new_data['s_password'] = $_POST['txt_user_registration_password'];
					$a_new_data['s_unique_key'] = $s_unique_key;
					$a_new_data['i_usn_id'] = 3;
					
					$a_add_user_result = array();
					$a_add_user_params['s_table_name'] = 'users';
					$a_add_user_params['a_new_data'] = $a_new_data;
					$a_add_user_result = $this->l_def_sql->create_data( $a_add_user_params );
	
					if( 	isset($a_add_user_result) && !empty($a_add_user_result) 
						&&	array_key_exists("i_sql_result", $a_add_user_result)
						&& 	$a_add_user_result["i_sql_result"] == 1 
					)
					{
						$a_send_email_activation_params = array();
						$a_send_email_activation_params['s_u_email'] = $_POST['txt_user_registration_email'];
						$a_send_email_activation_params['s_u_key'] = $s_unique_key;
						$a_send_email_result = array();
						$a_send_email_result = $this->l_user->send_email_activation( $a_send_email_activation_params );
						
						if( 	isset($a_send_email_result) && !empty($a_send_email_result) 
							&&	array_key_exists("i_send_email_result", $a_send_email_result)
							&& 	$a_send_email_result['i_send_email_result'] == 1 
						
						)
						{
							$this->register_activation_required();
						}
						else
						{
							array_push( $a_site_response_info, 'Email Activation Not Sent. Please Contact Administrator' );
							$a_form_notice['a_site_response_info'] = $a_site_response_info;
							$this->register_form($a_form_notice);
						}
					}
					else
					{
						array_push( $a_site_response_info, 'Unsuccessful Registration. Please Contact Administrator' );
						$a_form_notice['a_site_response_info'] = $a_site_response_info;
						$this->register_form($a_form_notice);
					}
				}
				else
				{
					$a_form_notice['s_txt_user_registration_captcha_error'] = 'Invalid Captcha';
					array_push( $a_site_response_error, 'Invalid Captcha' );
					
					$a_form_notice['a_site_response_error'] = $a_site_response_error;
					
					$this->register_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'user/register_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* register_activation_required
	* @desc		register_activation_required
	*
	**/
	public function register_activation_required()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$s_view_main_data = array();
		$s_view_main = $this->load->view('templates_v1/v_register_activation_required', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* login_form
	* @desc		
	*
	**/
	public function login_form( $a_form_notice=array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_div_alert_messages = '';
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !empty($a_user_details) )
		{
			redirect( base_url() . 'user/logout', 'refresh');
		}
		
		$s_view_site_responses_data = array();
		$s_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $s_view_site_responses_data, true);
		
		$s_view_header_data = array();
		$s_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		
		$s_view_main_data = array();
		$s_view_main_data['a_form_notice'] = $a_form_notice;
		$s_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_main = $this->load->view('templates_v1/v_user_login_form_v1', $s_view_main_data, true);
		
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	/**
	* login
	* @desc		
	*
	**/
	public function login()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_user');
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_user_login_emailorusername', 'Email or Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('txt_user_login_password', 'Password', 'trim|required|md5|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_user_login_emailorusername_error'] = form_error('txt_user_login_emailorusername', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_login_emailorusername_error']) && !empty($a_form_notice['s_txt_user_login_emailorusername_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_login_emailorusername_error'] );
				}
				
				$a_form_notice['s_txt_user_login_password_error'] = form_error('txt_user_login_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_login_password_error']) && !empty($a_form_notice['s_txt_user_login_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_login_password_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;
				
				$this->login_form($a_form_notice);
			}
			else
			{
				$a_login_params = array();
				$a_login_params['s_u_email_or_username'] = $_POST['txt_user_login_emailorusername'];
				$a_login_params['s_u_password'] = $_POST['txt_user_login_password'];
				
				$a_account_login_result = array();
				$a_account_login_result = $this->l_user->login( $a_login_params );
				
				if( isset($a_account_login_result) && !empty($a_account_login_result) && array_key_exists("i_result", $a_account_login_result) )
				{
					if( $a_account_login_result['i_result'] == 1 )
					{
						/*
							For Autologin later
							if( isset($_POST['chk_login_remember_me']) && ($_POST['chk_login_remember_me'] == 1) )
							{
								$this->l_user->set_autologin();
							}
						*/
						
						/**/
						$a_user_details = $this->session->userdata('a_user_details');
						
						/*
							Get user_roles
						*/
						$a_user_roles_query_where = array();
						$a_user_roles_query_table_join = array();
						$a_user_roles_query_params = array();
						array_push( $a_user_roles_query_where, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_user_details['i_u_id'] ) );
						$a_user_roles_query_params['a_where'] = $a_user_roles_query_where;
						array_push( $a_user_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
						$a_user_roles_query_params['a_table_join'] = $a_user_roles_query_table_join;
						$a_user_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
						$a_user_roles_query_params['s_table_name'] = 'user_roles';
						$a_user_roles_result = $this->l_def_sql->read_data( $a_user_roles_query_params );
						
						/*
							reIndexed $a_user_roles_result
						*/
						$a_ur_result_reindex = array();
						if( isset($a_user_roles_result) && !empty($a_user_roles_result) )
						{
							foreach( $a_user_roles_result AS $a_user_roles_result_details )
							{
								$a_ur_result_reindex[ $a_user_roles_result_details['i_ur_role_name_id'] ] = $a_user_roles_result_details;
							}
							$a_user_roles_result = $a_ur_result_reindex;
						}
						
						/**/
						$a_session_user_details = array(
							'a_user_roles_result'  => $a_user_roles_result
						);
						$this->session->set_userdata($a_session_user_details);
						
						
						
						
						
						redirect( base_url(), 'refresh');
						
					}
					else
					{
						array_push( $a_site_response_info, $a_account_login_result['s_message_notice'] );
						$a_form_notice['a_site_response_info'] = $a_site_response_info;
						$this->login_form($a_form_notice);
					}
				}
				else
				{
					array_push( $a_site_response_info, 'Unable to Login. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
					$this->login_form($a_form_notice);
				}
			}
		}
		else
		{
			redirect( base_url() . 'user/login_form/', 'refresh');
		}
		//======================================================
	}
	
	
	/**
	* logout
	* @desc		
	*
	**/
	public function logout()
	{
		$this->session->sess_destroy();
		//delete_cookie('sitename_autologin');

		//redirect( base_url() . 'user/login', 'refresh');
		redirect( base_url(), 'refresh');
	}
	
	
	/**
	* read_all
	* @desc		
	*
	**/
	public function read_all()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library(array('l_user'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');

		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 2, $a_user_roles_result ) //has Users role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }

		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order', 'status');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 10; }
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 's_username'; }
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 1; } // 1 = Activated, 2 = DeActivated, 3 = Activation Required
		
		
		/**/
		$a_u_query_where = array();
		$a_u_query_table_join = array();
		$a_u_query_order_by = array();
		$a_u_query_limit = array();
		$a_u_query_params = array();
		array_push( $a_u_query_where, array( 's_field' => 'users.i_usn_id', 'a_data' => $a_assoc_uri['status'] ) );
		if( array_key_exists( 1, $a_user_roles_result ) ) //ok if admin
		{}
		else
		{ array_push( $a_u_query_where, array( 's_field' => 'users.i_id !=', 'a_data' => 1 ) ); }
		array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
		array_push( $a_u_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
		$a_u_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
		$a_u_query_params['a_where'] = $a_u_query_where;
		$a_u_query_params['a_order_by'] = $a_u_query_order_by;
		$a_u_query_params['a_limit'] = $a_u_query_limit;
		$a_u_query_params['a_table_join'] = $a_u_query_table_join;
		$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
		$a_u_query_params['s_table_name'] = 'users';
		
		
		/**/
		$a_user_result = $this->l_def_sql->read_data( $a_u_query_params );
		$a_user_count_result = $this->l_def_sql->read_count_data( $a_u_query_params );
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'user/read_all/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/status/' .$a_assoc_uri['status']. '/offset/';
		$a_pagination_config['total_rows'] = $a_user_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 5;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		/*
			Get user_roles
		*/
		$a_user_roles_query_where = array();
		$a_user_roles_query_table_join = array();
		$a_user_roles_query_params = array();
		array_push( $a_user_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
		$a_user_roles_query_params['a_table_join'] = $a_user_roles_query_table_join;
		$a_user_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
		$a_user_roles_query_params['s_table_name'] = 'user_roles';
		$a_user_roles_result = $this->l_def_sql->read_data( $a_user_roles_query_params );
		
		
		/*
			Get inventory_depot_users
		*/
		$a_inventory_depot_users_query_where = array();
		$a_inventory_depot_users_query_table_join = array();
		$a_inventory_depot_users_query_params = array();
		array_push( $a_inventory_depot_users_query_table_join, array( 's_table_join_name' => 'inventory_depot', 's_table_join_condition' => 'inventory_depot.i_id = inventory_depot_users.i_id_id' ) );
		$a_inventory_depot_users_query_params['a_table_join'] = $a_inventory_depot_users_query_table_join;
		$a_inventory_depot_users_query_params['s_table_fields'] = $this->m_def_table_fields->s_inventory_depot_users_fields . ', ' . $this->m_def_table_fields->s_inventory_depot_fields;
		$a_inventory_depot_users_query_params['s_table_name'] = 'inventory_depot_users';
		$a_inventory_depot_users_result = $this->l_def_sql->read_data( $a_inventory_depot_users_query_params );
		
		//======================================================
		
		
		$s_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $s_view_header_data, true);
		
		$s_view_main_data = array();
		$s_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$s_view_main_data['a_user_result'] = $a_user_result;
		$s_view_main_data['a_user_count_result'] = $a_user_count_result;
		$s_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main_data['a_user_roles_result'] = $a_user_roles_result;
		$s_view_main_data['a_inventory_depot_users_result'] = $a_inventory_depot_users_result;
		$s_view_main = $this->load->view('templates_v1/v_user_read_all', $s_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_current_webpage);
		//======================================================
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

/* End of file */
/* Location: ./application/controllers/ */