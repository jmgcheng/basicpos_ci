<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_missed_purchase_order extends CI_Controller {
	
	/**
	* contructor
	* @desc		contructor for 
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	* is_existing
	* @desc		addon callback function for form validation. Check if data is existing.
				Opposite to what is_unique[...] is used
	*
	**/
	public function is_existing( $m_var, $s_table_detail )
	{
		//= Declare Start-Up Variables Here ====================
		$b_result = FALSE;
		$a_table_detail = array();
		
		//======================================================
		
		
		//======================================================
		$this->load->library('l_def_sql');
	
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_table_detail = explode('.', $s_table_detail);
	
	
		$a_query_where = array();
		$a_query_params = array();
		array_push( $a_query_where, array( 's_field' => $a_table_detail[0] . '.' . $a_table_detail[1], 'a_data' => $m_var ) );
		$a_query_params['a_where'] = $a_query_where;
		$a_query_params['s_table_fields'] = $a_table_detail[0] . '.' . $a_table_detail[1];
		$a_query_params['s_table_name'] = $a_table_detail[0];
		$a_product_duplicate_result = array();
		$a_product_duplicate_result = $this->l_def_sql->read_data( $a_query_params );
		
		
		if( isset($a_product_duplicate_result) && !empty($a_product_duplicate_result) )
		{
			$b_result = TRUE;
		}
		else
		{
			$this->form_validation->set_message('is_existing', 'The value entered at %s does not existing in the database');
		}
		
		return $b_result;
		//======================================================
	}
	
	
	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		
	}
	
	
	/**
	* create_form
	* @desc		
	*
	**/
	public function create_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 12, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			Get inventory_depot
		*/
		$a_inventory_depot_query_where = array();
		$a_inventory_depot_query_params = array();
		$a_inventory_depot_query_params['s_table_fields'] = $this->m_def_table_fields->s_inventory_depot_fields;
		$a_inventory_depot_query_params['s_table_name'] = 'inventory_depot';
		$a_inventory_depot_result = $this->l_def_sql->read_data( $a_inventory_depot_query_params );
		
		
		
		
		
		//======================================================
		
		
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_view_header_data = array();
		$a_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_form_notice'] = $a_form_notice;
		$a_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$a_view_main_data['a_inventory_depot_result'] = $a_inventory_depot_result;
		$s_view_main = $this->load->view('templates_v1/v_missed_purchase_order_create_form_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');
		array_push($a_current_webpage_inc_js_batch2, 'js/js_create_missed_purchase_order.js');
		
		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	/**
	* create_ajx
	* @desc		
	*
	**/
	public function create_ajx()
	{
		//= Declare Start-Up Variables Here ====================
		$a_result = array();
		$a_notice_ajx = array();
		
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 12, $a_user_roles_result ) //has Products role
		)
		{
			if( isset($_POST) && !empty($_POST) )
			{
				$this->form_validation->set_rules('opt_missed_purchase_order_receiving_depot', 'Depot', 'trim|required|xss_clean|callback_is_existing[inventory_depot.i_id]');
				$this->form_validation->set_rules('txt_missed_purchase_order_poh_id', 'Purchase Order', 'trim|required|xss_clean|callback_is_existing[purchase_order_header.i_id]');
				$this->form_validation->set_rules('txt_missed_purchase_order_comment', 'Comment', 'trim|required|max_length[255]|xss_clean');
				$this->form_validation->set_rules('txt_missed_purchase_order_detail_p_id', 'Order Details', 'required');
				$this->form_validation->set_rules('txt_missed_purchase_order_detail_p_quantity_order', 'Order Details', 'required');
				
				
				/*
					My simple brain is not working now. We should check each product in detail really exist before we save them someday
					$a_p_query_params = array();
					$a_p_query_params['s_table_fields'] = 'products.i_id AS i_p_id';
					$a_p_query_params['s_table_name'] = 'products';
					$a_product_result = array();
					$a_product_result = $this->l_def_sql->read_data( $a_p_query_params );
					if( isset($a_product_result) && !empty($a_product_result) )
					{
						$a_temp = array();
						foreach( $a_product_result AS $a_product_result_row )
						{
							$a_temp[$a_product_result_row['i_p_id']] = $a_product_result_row;
						}
						$a_product_result = $a_temp;
					}
					else
					{
						array_push( $a_site_response_info, 'No Products in Database' );
					}
				*/
				
				
				if( $this->form_validation->run() == FALSE )
				{
					$a_result['s_result'] = 'fail';
					array_push( $a_site_response_error, 'Form Details InCorrect' );
					
					$a_form_notice['s_opt_missed_purchase_order_receiving_depot_error'] = form_error('opt_missed_purchase_order_receiving_depot', ' ', ' ');
					if( isset($a_form_notice['s_opt_missed_purchase_order_receiving_depot_error']) && !empty($a_form_notice['s_opt_missed_purchase_order_receiving_depot_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_opt_missed_purchase_order_receiving_depot_error'] );
					}
					
					$a_form_notice['s_txt_missed_purchase_order_poh_id_error'] = form_error('txt_missed_purchase_order_poh_id', ' ', ' ');
					if( isset($a_form_notice['s_txt_missed_purchase_order_poh_id_error']) && !empty($a_form_notice['s_txt_missed_purchase_order_poh_id_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_txt_missed_purchase_order_poh_id_error'] );
					}
					
					$a_form_notice['s_txt_missed_purchase_order_comment_error'] = form_error('txt_missed_purchase_order_comment', ' ', ' ');
					if( isset($a_form_notice['s_txt_missed_purchase_order_comment_error']) && !empty($a_form_notice['s_txt_missed_purchase_order_comment_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_txt_missed_purchase_order_comment_error'] );
					}
					
				}
				else
				{
					$a_new_data = array();
					$a_new_data['i_poh_id'] = $_POST['txt_missed_purchase_order_poh_id'];
					$a_new_data['i_id_id'] = $_POST['opt_missed_purchase_order_receiving_depot'];
					$a_new_data['i_u_received_by'] = $a_user_details['i_u_id'];
					$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
					$a_new_data['s_comment'] = $_POST['txt_missed_purchase_order_comment'];
					$a_new_data['i_mposn_id'] = 1;
					
					/*
						insert header
					*/
					$a_add_mpoh_result = array();
					$a_add_mpoh_params['s_table_name'] = 'missed_po_header';
					$a_add_mpoh_params['a_new_data'] = $a_new_data;
					$a_add_mpoh_result = $this->l_def_sql->create_data( $a_add_mpoh_params );
					
					/*
						insert details
					*/
					if( 	isset($a_add_mpoh_result) && !empty($a_add_mpoh_result) 
							&&	array_key_exists("i_sql_result", $a_add_mpoh_result)
							&& 	$a_add_mpoh_result["i_sql_result"] == 1 
							&&	array_key_exists("i_insert_id", $a_add_mpoh_result)
							&& 	isset($a_add_mpoh_result['i_insert_id']) && !empty($a_add_mpoh_result['i_insert_id']) 
					)
					{
						$a_new_order_detail_param = array();
						for( $i_counter = 0; $i_counter < count($_POST['txt_missed_purchase_order_detail_p_id']); $i_counter++ )
						{
							$a_new_order_detail_template = array();
							$a_new_order_detail_template['i_mpoh_id'] = $a_add_mpoh_result['i_insert_id'];
							$a_new_order_detail_template['i_p_id'] = $_POST['txt_missed_purchase_order_detail_p_id'][$i_counter];
							$a_new_order_detail_template['i_quantity_missed'] = $_POST['txt_missed_purchase_order_detail_p_quantity_order'][$i_counter];
							
							array_push( $a_new_order_detail_param, $a_new_order_detail_template );
						}
						
						if( !empty($a_new_order_detail_param) )
						{
							$a_new_order_detail = array();
							$a_new_order_detail['a_new_data'] = $a_new_order_detail_param;
							$a_new_order_detail['s_table_name'] = 'missed_po_detail';
							$a_add_mpod_result = $this->l_def_sql->create_batch_data( $a_new_order_detail );
							
							if( 	isset($a_add_mpod_result) && !empty($a_add_mpod_result) 
									&&	array_key_exists("i_sql_result", $a_add_mpod_result)
									&& 	$a_add_mpod_result["i_sql_result"] == 1 
							)
							{
								$a_result['s_result'] = 'success';
								array_push( $a_site_response_success, 'Order Added' );
							}
							else
							{	
								$a_result['s_result'] = 'fail';
								array_push( $a_site_response_error, 'Details Not Saved' );
							}	
						}
					}
					else
					{
						$a_result['s_result'] = 'fail';
						array_push( $a_site_response_error, 'Details Not Saved' );
					}
				}
			}
			else
			{
				$a_result['s_result'] = 'fail';
				array_push( $a_site_response_error, 'No Data' );
			}
		}
		else
		{ 
			$a_result['s_result'] = 'fail';
			array_push( $a_site_response_error, 'No Rights' );
		}
		
		
		//======================================================
		$a_notice_ajx['a_site_response_error'] = $a_site_response_error;
		$a_notice_ajx['a_site_response_success'] = $a_site_response_success;
		$a_notice_ajx['a_site_response_info'] = $a_site_response_info;
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_notice_ajx;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_result['s_view_site_responses'] = $s_view_site_responses;
		//======================================================
		
		
		echo json_encode( $a_result );
		//======================================================
	}
	
	
	/**
	* read_all
	* @desc		
	*
	**/
	public function read_all()
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();

		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library(array('pagination'));
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 12, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order', 'status');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/**/
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 20; }
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 'missed_po_header.i_id'; }
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'asc'; }
		if( !isset($a_assoc_uri['status']) || empty($a_assoc_uri['status']) )
		{ $a_assoc_uri['status'] = 1; } // 1 = Activated, 2 = DeActivated
		
		
		/**/
		$a_missed_po_header_query_where = array();
		$a_missed_po_header_query_table_join = array();
		$a_missed_po_header_query_order_by = array();
		$a_missed_po_header_query_limit = array();
		$a_missed_po_header_query_params = array();
		array_push( $a_missed_po_header_query_where, array( 's_field' => 'missed_po_header.i_mposn_id', 'a_data' => $a_assoc_uri['status'] ) );
		array_push( $a_missed_po_header_query_table_join, array( 's_table_join_name' => 'inventory_depot', 's_table_join_condition' => 'inventory_depot.i_id = missed_po_header.i_id_id' ) );
		array_push( $a_missed_po_header_query_table_join, array( 's_table_join_name' => 'users', 's_table_join_condition' => 'users.i_id = missed_po_header.i_u_received_by' ) );
		array_push( $a_missed_po_header_query_table_join, array( 's_table_join_name' => 'missed_po_status_names', 's_table_join_condition' => 'missed_po_status_names.i_id = missed_po_header.i_mposn_id' ) );
		array_push( $a_missed_po_header_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
		$a_missed_po_header_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
		$a_missed_po_header_query_params['a_table_join'] = $a_missed_po_header_query_table_join;
		$a_missed_po_header_query_params['a_where'] = $a_missed_po_header_query_where;
		$a_missed_po_header_query_params['a_order_by'] = $a_missed_po_header_query_order_by;
		$a_missed_po_header_query_params['a_limit'] = $a_missed_po_header_query_limit;
		$a_missed_po_header_query_params['s_table_fields'] = $this->m_def_table_fields->s_missed_po_header_fields . ', ' . $this->m_def_table_fields->s_inventory_depot_fields . ', ' . $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_missed_po_status_names_fields;
		$a_missed_po_header_query_params['s_table_name'] = 'missed_po_header';
		$a_missed_po_header_result = $this->l_def_sql->read_data( $a_missed_po_header_query_params );
		$a_missed_po_header_count_result = $this->l_def_sql->read_count_data( $a_missed_po_header_query_params );
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'missed_purchase_order/read_all/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/status/' .$a_assoc_uri['status']. '/offset/';
		$a_pagination_config['total_rows'] = $a_missed_po_header_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 20;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		//======================================================
		
		
		$a_view_header_data = array();
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_assoc_uri'] = $a_assoc_uri;
		$a_view_main_data['a_missed_po_header_result'] = $a_missed_po_header_result;
		$a_view_main_data['a_missed_po_header_count_result'] = $a_missed_po_header_count_result;
		$a_view_main_data['s_page_links_pagination'] = $s_page_links_pagination;
		$s_view_main = $this->load->view('templates_v1/v_missed_purchase_order_read_all_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		//======================================================

		
		//======================================================

		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');

		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_form
	* @desc		
	*
	**/
	public function update_form( $a_form_notice = array() )
	{
		//= Declare Start-Up Variables Here ====================
		$a_current_webpage_inc_metas = array();
		$a_current_webpage_inc_css = array();
		$a_current_webpage_inc_js_batch1 = array();
		$a_current_webpage_inc_js_batch2 = array();
		
		$s_view_header = '';
		$s_view_main = '';
		$s_view_footer = '';
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->model('m_def_table_fields');
		$this->load->helper(array('form'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 12, $a_user_roles_result ) //has Products role
		)
		{}
		else
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('missed_purchase_order_id');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		/**/
		if( !isset($a_assoc_uri['missed_purchase_order_id']) || empty($a_assoc_uri['missed_purchase_order_id']) )
		{
			redirect( base_url(), 'refresh');
		}
		
		
		/**/
		$a_mpoh_query_where = array();
		$a_mpoh_query_params = array();
		array_push( $a_mpoh_query_where, array( 's_field' => 'missed_po_header.i_id', 'a_data' => $a_assoc_uri['missed_purchase_order_id'] ) );
		$a_mpoh_query_params['a_where'] = $a_mpoh_query_where;
		$a_mpoh_query_params['s_table_fields'] = $this->m_def_table_fields->s_missed_po_header_fields;
		$a_mpoh_query_params['s_table_name'] = 'missed_po_header';
		$a_missed_po_header_row_result = array();
		$a_missed_po_header_result = $this->l_def_sql->read_data( $a_mpoh_query_params );
		if( isset($a_missed_po_header_result) && !empty($a_missed_po_header_result) )
		{ $a_missed_po_header_row_result = $a_missed_po_header_result[0]; }
		else
		{ redirect( base_url(), 'refresh'); }
				
		
		/**/
		$a_mpod_query_where = array();
		$a_mpod_query_params = array();
		array_push( $a_mpod_query_where, array( 's_field' => 'missed_po_detail.i_mpoh_id', 'a_data' => $a_assoc_uri['missed_purchase_order_id'] ) );
		$a_mpod_query_params['a_where'] = $a_mpod_query_where;
		$a_mpod_query_params['s_table_fields'] = $this->m_def_table_fields->s_missed_po_detail_fields;
		$a_mpod_query_params['s_table_name'] = 'missed_po_detail';
		$a_missed_po_detail_row_result = array();
		$a_missed_po_detail_result = $this->l_def_sql->read_data( $a_mpod_query_params );
		if( isset($a_missed_po_detail_result) && !empty($a_missed_po_detail_result) )
		{ $a_missed_po_detail_row_result = $a_missed_po_detail_result; }
		else
		{ 
			//redirect( base_url(), 'refresh'); Purchase Orders should always have details
		}
		
		
		/*
			Get inventory_depot
		*/
		$a_inventory_depot_query_where = array();
		$a_inventory_depot_query_params = array();
		$a_inventory_depot_query_params['s_table_fields'] = $this->m_def_table_fields->s_inventory_depot_fields;
		$a_inventory_depot_query_params['s_table_name'] = 'inventory_depot';
		$a_inventory_depot_result = $this->l_def_sql->read_data( $a_inventory_depot_query_params );
		
		
		//======================================================
		
		
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_form_notice;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_view_header_data = array();
		$a_view_header_data['s_view_site_responses'] = $s_view_site_responses;
		$s_view_header = $this->load->view('templates_v1/v_header_v1', $a_view_header_data, true);
		
		$a_view_main_data = array();
		$a_view_main_data['a_form_notice'] = $a_form_notice;
		$a_view_main_data['s_view_site_responses'] = $s_view_site_responses;
		$a_view_main_data['a_inventory_depot_result'] = $a_inventory_depot_result;
		$a_view_main_data['a_missed_po_header_row_result'] = $a_missed_po_header_row_result;
		$a_view_main_data['a_missed_po_detail_row_result'] = $a_missed_po_detail_row_result;
		$s_view_main = $this->load->view('templates_v1/v_missed_purchase_order_update_form_v1', $a_view_main_data, true);
		
		$s_view_footer_data = array();
		$s_view_footer = $this->load->view('templates_v1/v_footer_v1', $s_view_footer_data, true);
		
		
		//======================================================
		
		
		//======================================================
		array_push($a_current_webpage_inc_css, 'css/css_reset.css');
		array_push($a_current_webpage_inc_css, 'css/css_style.css');

		array_push($a_current_webpage_inc_js_batch1, 'js/jquery-1.11.1.js');
		array_push($a_current_webpage_inc_js_batch1, 'js/js_detect_browser.js');
		array_push($a_current_webpage_inc_js_batch2, 'js/js_update_missed_purchase_order.js');
		
		$a_current_webpage_data['a_current_webpage_inc_metas'] = $a_current_webpage_inc_metas;
		$a_current_webpage_data['a_current_webpage_inc_css'] = $a_current_webpage_inc_css;
		$a_current_webpage_data['a_current_webpage_inc_js_batch1'] = $a_current_webpage_inc_js_batch1;
		$a_current_webpage_data['a_current_webpage_inc_js_batch2'] = $a_current_webpage_inc_js_batch2;
		$a_current_webpage_data['s_current_webpage_title'] = 'Basic POS';

		$a_current_webpage_data['s_current_webpage_header'] = $s_view_header;
		$a_current_webpage_data['s_current_webpage_main'] = $s_view_main;
		$a_current_webpage_data['s_current_webpage_footer'] = $s_view_footer;
		$s_v_current_webpage = $this->load->view('templates_v1/v_html_template_v1', $a_current_webpage_data, true);
		//======================================================

		
		//======================================================
		$this->output->set_output($s_v_current_webpage);
		//======================================================
	}
	
	
	/**
	* update_ajx
	* @desc		
	*
	**/
	public function update_ajx()
	{
		//= Declare Start-Up Variables Here ====================
		$a_result = array();
		$a_notice_ajx = array();
		
		$a_site_response_error = array();
		$a_site_response_info = array();
		$a_site_response_success = array();
		//======================================================
		
		
		//======================================================
		$this->load->library('form_validation');
		$this->load->library('l_def_sql');
		
		$this->load->helper(array('form'));
		
		$this->load->model('m_def_table_fields');
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');$a_user_roles_result = $this->session->userdata('a_user_roles_result');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( !isset($a_user_roles_result) || empty($a_user_roles_result) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		if( 	array_key_exists( 1, $a_user_roles_result ) //ok if admin
			||	array_key_exists( 12, $a_user_roles_result ) //has Products role
		)
		{
			/*
			parse uri first
			*/
			$a_expected_uri = array('missed_purchase_order_id');
			$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);


			/**/
			if( !isset($a_assoc_uri['missed_purchase_order_id']) || empty($a_assoc_uri['missed_purchase_order_id']) )
			{
				redirect( base_url(), 'refresh');
			}
			
			
			if( isset($_POST) && !empty($_POST) )
			{
				/*
					lots of lacking trappings. 
						what if i edited a title that is already existing in a different poh? Check ci_savour. I've done this before
				*/
				$this->form_validation->set_rules('opt_missed_purchase_order_receiving_depot', 'Depot', 'trim|required|xss_clean|callback_is_existing[inventory_depot.i_id]');
				$this->form_validation->set_rules('txt_missed_purchase_order_poh_id', 'Purchase Order', 'trim|required|xss_clean|callback_is_existing[purchase_order_header.i_id]');
				$this->form_validation->set_rules('txt_missed_purchase_order_comment', 'Comment', 'trim|required|max_length[255]|xss_clean');
				$this->form_validation->set_rules('txt_missed_purchase_order_detail_p_id', 'Order Details', 'required');
				$this->form_validation->set_rules('txt_missed_purchase_order_detail_p_quantity_order', 'Order Details', 'required');
				
				
				/*
					My simple brain is not working now. We should check each product in detail really exist before we save them someday
					$a_p_query_params = array();
					$a_p_query_params['s_table_fields'] = 'products.i_id AS i_p_id';
					$a_p_query_params['s_table_name'] = 'products';
					$a_product_result = array();
					$a_product_result = $this->l_def_sql->read_data( $a_p_query_params );
					if( isset($a_product_result) && !empty($a_product_result) )
					{
						$a_temp = array();
						foreach( $a_product_result AS $a_product_result_row )
						{
							$a_temp[$a_product_result_row['i_p_id']] = $a_product_result_row;
						}
						$a_product_result = $a_temp;
					}
					else
					{
						array_push( $a_site_response_info, 'No Products in Database' );
					}
				*/
				
				
				if( $this->form_validation->run() == FALSE )
				{
					$a_result['s_result'] = 'fail';
					array_push( $a_site_response_error, 'Form Details InCorrect' );
					
					$a_form_notice['s_opt_missed_purchase_order_receiving_depot_error'] = form_error('opt_missed_purchase_order_receiving_depot', ' ', ' ');
					if( isset($a_form_notice['s_opt_missed_purchase_order_receiving_depot_error']) && !empty($a_form_notice['s_opt_missed_purchase_order_receiving_depot_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_opt_missed_purchase_order_receiving_depot_error'] );
					}
					
					$a_form_notice['s_txt_missed_purchase_order_poh_id_error'] = form_error('txt_missed_purchase_order_poh_id', ' ', ' ');
					if( isset($a_form_notice['s_txt_missed_purchase_order_poh_id_error']) && !empty($a_form_notice['s_txt_missed_purchase_order_poh_id_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_txt_missed_purchase_order_poh_id_error'] );
					}

					$a_form_notice['s_txt_missed_purchase_order_comment_error'] = form_error('txt_missed_purchase_order_comment', ' ', ' ');
					if( isset($a_form_notice['s_txt_missed_purchase_order_comment_error']) && !empty($a_form_notice['s_txt_missed_purchase_order_comment_error']) )
					{
						array_push( $a_site_response_error, $a_form_notice['s_txt_missed_purchase_order_comment_error'] );
					}
					
					
				}
				else
				{
					/*
						update header
					*/
					$a_update_data = array();
					$a_update_mpoh_query_where = array();
					$a_update_mpoh_query_params = array();
					array_push( $a_update_mpoh_query_where, array( 's_field' => 'missed_po_header.i_id', 'a_data' => $a_assoc_uri['missed_purchase_order_id'] ) );
					$a_update_data['i_poh_id'] = $_POST['txt_missed_purchase_order_poh_id'];
					$a_update_data['i_id_id'] = $_POST['opt_missed_purchase_order_receiving_depot'];
					$a_update_data['s_comment'] = $_POST['txt_missed_purchase_order_comment'];
					$a_update_mpoh_query_params['a_where'] = $a_update_mpoh_query_where;
					$a_update_mpoh_query_params['a_update_data'] = $a_update_data;
					$a_update_mpoh_query_params['s_table_name'] = 'missed_po_header';
					$a_update_missed_po_header_header_result = $this->l_def_sql->update_data( $a_update_mpoh_query_params );
					
					
					/*
						delete details
					*/
					if( 	isset($a_update_missed_po_header_header_result) && !empty($a_update_missed_po_header_header_result) 
							&&	array_key_exists("i_sql_result", $a_update_missed_po_header_header_result)
							&& 	$a_update_missed_po_header_header_result["i_sql_result"] == 1 
							&&	isset($a_assoc_uri['missed_purchase_order_id']) && !empty($a_assoc_uri['missed_purchase_order_id']) 
					)
					{
						$a_delete_mpod_query_where = array();
						$a_delete_mpod_query_params = array();
						array_push( $a_delete_mpod_query_where, array( 's_field' => 'missed_po_detail.i_mpoh_id', 'a_data' => $a_assoc_uri['missed_purchase_order_id'] ) );
						$a_delete_mpod_query_params['a_where'] = $a_delete_mpod_query_where;
						$a_delete_mpod_query_params['s_table_name'] = 'missed_po_detail';
						$a_delete_missed_po_header_detail_result = $this->l_def_sql->delete_data( $a_delete_mpod_query_params );	
					}
					else
					{
						$a_result['s_result'] = 'fail';
						array_push( $a_site_response_error, 'Old Details Not Deleted' );
					}
					
					
					/*
						insert details again
					*/
					if( 	isset($a_update_missed_po_header_header_result) && !empty($a_update_missed_po_header_header_result) 
							&&	array_key_exists("i_sql_result", $a_update_missed_po_header_header_result)
							&& 	$a_update_missed_po_header_header_result["i_sql_result"] == 1 
							&&	isset($a_assoc_uri['missed_purchase_order_id']) && !empty($a_assoc_uri['missed_purchase_order_id']) 
					)
					{
						$a_new_order_detail_param = array();
						for( $i_counter = 0; $i_counter < count($_POST['txt_missed_purchase_order_detail_p_id']); $i_counter++ )
						{
							$a_new_order_detail_template = array();
							$a_new_order_detail_template['i_mpoh_id'] = $a_assoc_uri['missed_purchase_order_id'];
							$a_new_order_detail_template['i_p_id'] = $_POST['txt_missed_purchase_order_detail_p_id'][$i_counter];
							$a_new_order_detail_template['i_quantity_missed'] = $_POST['txt_missed_purchase_order_detail_p_quantity_order'][$i_counter];
							
							array_push( $a_new_order_detail_param, $a_new_order_detail_template );
						}
						
						if( !empty($a_new_order_detail_param) )
						{
							$a_new_order_detail = array();
							$a_new_order_detail['a_new_data'] = $a_new_order_detail_param;
							$a_new_order_detail['s_table_name'] = 'missed_po_detail';
							$a_add_mpod_result = $this->l_def_sql->create_batch_data( $a_new_order_detail );
							
							if( 	isset($a_add_mpod_result) && !empty($a_add_mpod_result) 
									&&	array_key_exists("i_sql_result", $a_add_mpod_result)
									&& 	$a_add_mpod_result["i_sql_result"] == 1 
							)
							{
								$a_result['s_result'] = 'success';
								array_push( $a_site_response_success, 'Order Updated' );
							}
							else
							{	
								$a_result['s_result'] = 'fail';
								array_push( $a_site_response_error, 'Details Not Saved' );
							}	
						}
					}
					else
					{
						$a_result['s_result'] = 'fail';
						array_push( $a_site_response_error, 'New Details Not Saved' );
					}
					
				}
			}
			else
			{
				$a_result['s_result'] = 'fail';
				array_push( $a_site_response_error, 'No Data' );
			}
		}
		else
		{ 
			$a_result['s_result'] = 'fail';
			array_push( $a_site_response_error, 'No Rights' );
		}
		
		
		//======================================================
		$a_notice_ajx['a_site_response_error'] = $a_site_response_error;
		$a_notice_ajx['a_site_response_success'] = $a_site_response_success;
		$a_notice_ajx['a_site_response_info'] = $a_site_response_info;
		$a_view_site_responses_data = array();
		$a_view_site_responses_data['a_form_notice'] = $a_notice_ajx;
		$s_view_site_responses = $this->load->view('templates_v1/v_site_responses', $a_view_site_responses_data, true);
		
		$a_result['s_view_site_responses'] = $s_view_site_responses;
		//======================================================
		
		
		echo json_encode( $a_result );
		//======================================================
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

/* End of file */
/* Location: ./application/controllers/ */