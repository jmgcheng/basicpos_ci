<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_user {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('m_user');
	}
	
	
	/**
	* add_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_u( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_user->add_u( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_urn( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_user->add_urn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_usn( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_user->add_usn( $a_params );
		
		return $a_result;
	}
		
	
	/**
	* insert_batch_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_ur( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->insert_batch_ur( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* send_email_activation
	*
	* @desc 	used to send an email to user for account acctivation
	* @param 1 	a_params
	* @return: 	int
	*
	**/
	public function send_email_activation( $a_params = array() )
	{
		$i_send_email_result = 0;
		$a_send_email_result = array();

		if( isset($a_params) && !empty($a_params) && !empty($a_params['s_u_email']) && !empty($a_params['s_u_key']) )
		{
			$s_u_email = $a_params['s_u_email'];
			$s_u_key = $a_params['s_u_key'];
			
			$this->CI->load->library('encrypt');
			
			$s_email_from = 'no_reply@trusted-freelancer.com';
			$s_email_to = $s_u_email;
			$s_email_subject = 'POS Email Activation';
			$s_email_message = '';
			$a_email_config = array();
			
			$s_email_user_activation_link = $s_u_key;
			$s_email_user_activation_link = $this->CI->encrypt->encode($s_email_user_activation_link);
			$s_email_user_activation_link = base64_encode($s_email_user_activation_link);

			$a_html_email_activation_data['s_email_subject'] = $s_email_subject;
			$a_html_email_activation_data['s_email_user_activation_link'] = $s_email_user_activation_link;
			$s_v_html_email_activation = $this->CI->load->view('templates_v1/v_html_email_activation_v1', $a_html_email_activation_data, true);
			$s_email_message = $s_v_html_email_activation;
			
			$a_send_email_params = array();
			$a_send_email_params['s_email_from'] = $s_email_from;
			$a_send_email_params['s_email_to'] = $s_email_to;
			$a_send_email_params['s_email_subject'] = $s_email_subject;
			$a_send_email_params['s_email_message'] = $s_email_message;
			$a_send_email_params['a_email_config'] = $a_email_config;
			
			$i_send_email_result = $this->send_email( $a_send_email_params );

			$a_send_email_result['i_send_email_result'] = $i_send_email_result;

		}
		
		return $a_send_email_result;
	}
	
	
	/**
	* send_email
	*
	* @desc 	used for sending a basic email
	* @param 1 	a_params
	* @return: 	int
	*
	**/
	public function send_email( $a_params = array() )
	{
		$i_result = 0;
		
		$this->CI->load->library(array('email'));
		
		if( isset($a_params) && !empty($a_params) && !empty($a_params['s_email_from']) && !empty($a_params['s_email_to']) && !empty($a_params['s_email_subject']) && !empty($a_params['s_email_message']) )
		{
			$s_email_from = $a_params['s_email_from'];
			$s_email_to = $a_params['s_email_to'];
			$s_email_subject = $a_params['s_email_subject'];
			$s_email_message = $a_params['s_email_message'];
			$a_email_config = $a_params['a_email_config'];
		
			if( empty($a_email_config) )
			{
				/*
				$a_email_config['mailtype'] = 'html';
				$a_email_config['protocol'] = 'smtp';
				$a_email_config['smtp_host'] = 'ssl://smtp.gmail.com';
				$a_email_config['smtp_port']    = '465';
				*/
				
				/*
				$a_email_config['protocol']    = 'sendmail';
				$a_email_config['smtp_host']    = 'ssl://smtp.gmail.com';
				$a_email_config['smtp_port']    = '465';
				$a_email_config['smtp_user']    = 'jmgcheng@gmail.com';
				$a_email_config['smtp_pass']    = 'Vp112168456120779+';
				*/
				//$a_email_config['smtp_timeout'] = '7';
				//$a_email_config['charset']    = 'utf-8';
				//$a_email_config['newline']    = "\r\n";
				$a_email_config['mailtype'] = 'html'; // or text
				//$a_email_config['validation'] = TRUE;
			}
			
			$this->CI->email->initialize($a_email_config);
			$this->CI->email->from($s_email_from);
			$this->CI->email->to($s_email_to);
			$this->CI->email->subject($s_email_subject);
			$this->CI->email->message($s_email_message);
			
			if ( $this->CI->email->send() )
			{
				$i_result = 1;
			}
			
			/*
			echo $this->CI->email->print_debugger();
			exit();
			*/

		}
		
		return $i_result;
	}
	
	
	/**
	* login
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function login( $a_params = array() )
	{
		$a_result = array();
		$a_result['i_result'] = 0;

		if( !empty($a_params) && !empty($a_params['s_u_email_or_username']) && !empty($a_params['s_u_password']) )
		{
			$a_user_details = $this->CI->m_user->get_u_by_upasswordnuemailousername( $a_params );

			if( !empty($a_user_details) )
			{
				/*
					check first if i_u_usn_id == 1
				*/
				if( $a_user_details['i_u_usn_id'] == 1 )
				{
					$a_session_user_details = array(
						'a_user_details'  => $a_user_details,
						'b_is_user_login'  => TRUE
					);
					
					$this->CI->session->set_userdata($a_session_user_details);

					$a_result['i_result'] = 1;
				}
				else
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_code'] = 'message';
					$a_result['s_message_subject'] = 'Login';
					$a_result['s_message_notice'] = 'Account NOT activated';
				}
			}
			else
			{
				$a_result['s_message_status'] = 'warning';
				$a_result['s_message_code'] = 'message';
				$a_result['s_message_subject'] = 'Login';
				$a_result['s_message_notice'] = 'Incorrect Details';
			}
		}

		return $a_result;
	}
	
	
	/**
	* get_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_ur( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_user->get_ur( $a_params );

		return $a_result;
	}
	
	
	/**
	* get_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_urn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->get_urn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_usn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->get_usn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_u( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->get_u( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_u( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->count_get_u( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_urn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->update_urn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_usn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->update_usn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_u( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->update_u( $a_params );
		
		return $a_result;
	}
	
		
	/**
	* delete_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_ur( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_user->delete_ur( $a_params );
		
		return $a_result;
	}
	
}