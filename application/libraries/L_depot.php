<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_depot {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('m_depot');
	}
	
	
	/**
	* add_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_id( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_depot->add_id( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_idt( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_depot->add_idt( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* insert_batch_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_idu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->insert_batch_idu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_id( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->get_id( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_id( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->count_get_id( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_idt( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->get_idt( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_idt( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->count_get_idt( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_idu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->get_idu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_idu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->count_get_idu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_id( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->update_id( $a_params );
		
		return $a_result;
	}
	
		
	/**
	* update_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_idt( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->update_idt( $a_params );
		
		return $a_result;
	}	
		
		
	/**
	* delete_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_idu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_depot->delete_idu( $a_params );
		
		return $a_result;
	}
	
}