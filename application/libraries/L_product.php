<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_product {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('m_product');
	}
	
	
	/**
	* add_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_p( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_p( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pd( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_pd( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pc( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_pc( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pu( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_pu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_psn( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_psn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* add_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_ptn( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_product->add_ptn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* insert_batch_pt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_pt( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->insert_batch_pt( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_p( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_p( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_p( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_p( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pd( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_pd( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pd( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_pd( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pc( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_pc( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pc( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_pc( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_pu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_pu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_psn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_psn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_psn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_psn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* get_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_ptn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->get_ptn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* count_get_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_ptn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->count_get_ptn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_p( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_p( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_pd( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_pd( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_pc( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_pc( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_pu( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_pu( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_psn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_psn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_ptn( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->update_ptn( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* delete_pt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_pt( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_product->delete_pt( $a_params );
		
		return $a_result;
	}
	
}