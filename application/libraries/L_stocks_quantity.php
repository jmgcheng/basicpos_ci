<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_stocks_quantity {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('m_stocks_quantity');
	}
	
	
	/**
	* read_stocks_quantity
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_stocks_quantity( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_stocks_quantity->read_stocks_quantity( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* read_count_stocks_quantity
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_count_stocks_quantity( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_stocks_quantity->read_count_stocks_quantity( $a_params );
		
		return $a_result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}