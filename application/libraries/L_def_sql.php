<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_def_sql {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('m_def_sql');
	}
	
	
	/**
	* create_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function create_data( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_def_sql->create_data( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* create_batch_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function create_batch_data( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_def_sql->create_batch_data( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* read_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_data( $a_params = array() )
	{
		$a_result = array();

		$a_result = $this->CI->m_def_sql->read_data( $a_params );

		return $a_result;
	}
	
	
	/**
	* read_count_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_count_data( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_def_sql->read_count_data( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* update_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function update_data( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_def_sql->update_data( $a_params );
		
		return $a_result;
	}
	
	
	/**
	* delete_data
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_data( $a_params = array() )
	{
		$a_result = array();
		
		$a_result = $this->CI->m_def_sql->delete_data( $a_params );
		
		return $a_result;
	}
	
}