<?php
class m_def_table_fields extends CI_Model{

	public $s_delivery_receipt_header_fields = 	'delivery_receipt_header.i_id AS i_drh_id, 
												delivery_receipt_header.i_sih_id AS i_drh_sih_id, 
												delivery_receipt_header.i_u_prepared_by AS i_drh_u_prepared_by, 
												delivery_receipt_header.i_u_approved_by AS i_drh_u_approved_by, 
												delivery_receipt_header.s_date_registration AS s_drh_date_registration, 
												delivery_receipt_header.s_title AS s_drh_title, 
												delivery_receipt_header.s_comment AS s_drh_comment, 
												delivery_receipt_header.i_drsn_id AS i_drh_drsn_id';

	public $s_delivery_receipt_status_names_fields = 	'delivery_receipt_status_names.i_id AS i_drsn_id,
														delivery_receipt_status_names.s_name AS s_drsn_name';
														
	public $s_delivery_receipt_detail_fields = 	'delivery_receipt_detail.i_drh_id AS i_drd_drh_id, 
												delivery_receipt_detail.i_p_id AS i_drd_p_id, 
												delivery_receipt_detail.i_quantity_delivering AS i_drd_quantity_delivering, 
												delivery_receipt_detail.s_product_serial AS s_drd_product_serial';
	
	public $s_excluded_inventory_header_fields = 	'excluded_inventory_header.i_id AS i_eih_id, 
													excluded_inventory_header.i_id_id AS i_eih_id_id, 
													excluded_inventory_header.i_u_prepared_by AS i_eih_u_prepared_by, 
													excluded_inventory_header.s_date_registration AS s_eih_date_registration, 
													excluded_inventory_header.s_comment AS s_eih_comment, 
													excluded_inventory_header.i_eisn_id	 AS i_eih_eisn_id';
	
	public $s_excluded_inventory_status_names_fields = 	'excluded_inventory_status_names.i_id AS i_eisn_id,
														excluded_inventory_status_names.s_name AS s_eisn_name';
	
	public $s_excluded_inventory_detail_fields = 	'excluded_inventory_detail.i_eih_id AS i_eid_id, 
													excluded_inventory_detail.i_p_id AS i_eid_p_id, 
													excluded_inventory_detail.i_quantity_excluded AS i_eid_quantity_excluded, 
													excluded_inventory_detail.s_product_serial AS s_eid_product_serial';
	
	public $s_included_inventory_header_fields = 	'included_inventory_header.i_id AS i_iih_id, 
													included_inventory_header.i_id_id AS i_iih_id_id, 
													included_inventory_header.i_u_prepared_by AS i_iih_u_prepared_by, 
													included_inventory_header.s_date_registration AS s_iih_date_registration, 
													included_inventory_header.s_comment AS s_iih_comment, 
													included_inventory_header.i_iisn_id AS i_iih_iisn_id';
	
	public $s_included_inventory_status_names_fields = 	'included_inventory_status_names.i_id AS i_iisn_id, 
														included_inventory_status_names.s_name AS s_iisn_name';
	
	public $s_included_inventory_detail_fields = 	'included_inventory_detail.i_iih_id AS i_iid_id, 
													included_inventory_detail.i_p_id AS i_iid_p_id, 
													included_inventory_detail.i_quantity_included AS i_iid_quantity_included, 
													included_inventory_detail.s_product_serial AS s_iid_product_serial';
	
	public $s_inventory_depot_fields = 'inventory_depot.i_id AS i_id_id, 
										inventory_depot.i_idt_id AS i_id_idt_id, 
										inventory_depot.s_name AS s_id_name, 
										inventory_depot.s_description AS s_id_description, 
										inventory_depot.s_address AS s_id_address';
		
	public $s_inventory_depot_type_fields = 'inventory_depot_type.i_id AS i_idt_id, 
											inventory_depot_type.s_type AS s_idt_type';
	
	public $s_inventory_depot_users_fields = 'inventory_depot_users.i_u_id AS i_idu_u_id, 
											inventory_depot_users.i_id_id AS i_idu_id_id';

	public $s_missed_po_header_fields = 'missed_po_header.i_id AS i_mpoh_id, 
										missed_po_header.i_poh_id AS i_mpoh_poh_id, 
										missed_po_header.i_id_id AS i_mpoh_id_id, 
										missed_po_header.i_u_received_by AS i_mpoh_u_received_by, 
										missed_po_header.i_u_approved_by AS i_mpoh_u_approved_by, 
										missed_po_header.s_date_registration AS s_mpoh_date_registration, 
										missed_po_header.s_comment AS s_mpoh_comment,
										missed_po_header.i_mposn_id AS i_orh_mposn_id';

	public $s_missed_po_detail_fields = 'missed_po_detail.i_mpoh_id AS i_mpod_mpoh_id, 
										missed_po_detail.i_p_id AS i_mpod_p_id, 
										missed_po_detail.i_quantity_missed AS i_mpod_quantity_missed';
										
	public $s_missed_po_status_names_fields = 'missed_po_status_names.i_id AS i_mposn_id,
												missed_po_status_names.s_name AS s_mposn_name';

	public $s_official_receipt_header_fields = 'official_receipt_header.i_id AS i_orh_id, 
												official_receipt_header.i_sih_id AS i_orh_sih_id, 
												official_receipt_header.i_id_id AS i_orh_id_id, 
												official_receipt_header.i_u_prepared_by AS i_orh_u_prepared_by, 
												official_receipt_header.i_u_approved_by AS i_orh_u_approved_by, 
												official_receipt_header.s_date_registration AS s_orh_date_registration, 
												official_receipt_header.s_comment AS s_orh_comment,
												official_receipt_header.i_orsn_id AS i_orh_orsn_id';

	public $s_official_receipt_status_names_fields = 'official_receipt_status_names.i_id AS i_orsn_id,
														official_receipt_status_names.s_name AS s_orsn_name';
														
	public $s_official_receipt_detail_fields = 'official_receipt_detail.i_orh_id AS i_ord_orh_id, 
												official_receipt_detail.i_p_id AS i_ord_p_id, 
												official_receipt_detail.i_quantity_released AS i_ord_quantity_released, 
												official_receipt_detail.s_product_serial AS s_ord_product_serial';
						

	public $s_products_fields = 'products.i_id AS i_p_id, 
								products.s_date_registration AS s_p_date_registration, 
								products.s_name AS s_p_name, 
								products.s_model AS s_p_model, 
								products.s_dimension AS s_p_dimension, 
								products.s_color AS s_p_color, 
								products.s_unit AS s_p_unit, 
								products.s_description AS s_p_description, 
								products.i_qty_low_alert AS i_p_qty_low_alert, 
								products.i_psn_id AS i_p_psn_id, 
								products.i_pc_id AS i_p_pc_id, 
								products.i_pd_id AS i_p_pd_id, 
								products.i_pu_id AS i_p_pu_id';

								
	public $s_product_colors_fields = 'product_colors.i_id AS i_pc_id,
										product_colors.s_name AS s_pc_name';
										
										
	public $s_product_dimensions_fields = 'product_dimensions.i_id AS i_pd_id,
										product_dimensions.s_name AS s_pd_name';
										
										
	public $s_product_units_fields = 'product_units.i_id AS i_pu_id,
										product_units.s_name AS s_pu_name';									
										
										
										
	public $s_product_status_names_fields = 'product_status_names.i_id AS i_psn_id,
											product_status_names.s_name AS s_psn_name';
								
								
	public $s_product_tag_names_fields = 'product_tag_names.i_id AS i_ptn_id,
										product_tag_names.s_name AS s_ptn_name';

										
	public $s_product_tags_fields = 'product_tags.i_p_id AS i_pt_p_id, 
										product_tags.i_ptn_id AS i_pt_ptn_id';
										
	public $s_purchase_order_header_fields = 'purchase_order_header.i_id AS i_poh_id, 
												purchase_order_header.i_id_id AS i_poh_id_id, 
												purchase_order_header.i_u_prepared_by AS i_poh_u_prepared_by, 
												purchase_order_header.i_u_approved_by AS i_poh_u_approved_by, 
												purchase_order_header.s_date_registration AS s_poh_date_registration, 
												purchase_order_header.s_title AS s_poh_title, 
												purchase_order_header.s_order_description AS s_poh_order_description, 
												purchase_order_header.s_comment AS s_poh_comment, 
												purchase_order_header.i_posn_id AS i_poh_posn_id';

	public $s_purchase_order_revision_fields = 'purchase_order_revision.i_id AS i_por_id, 
												purchase_order_revision.i_poh_id AS i_por_poh_id, 
												purchase_order_revision.s_date_updated AS s_por_date_updated, 
												purchase_order_revision.i_u_revised_by AS i_por_u_revised_by, 
												purchase_order_revision.s_comment AS s_por_comment';
	
	public $s_purchase_order_status_names_fields = 'purchase_order_status_names.i_id AS i_posn_id,
													purchase_order_status_names.s_name AS s_posn_name';

	public $s_purchase_order_detail_fields = 'purchase_order_detail.i_poh_id AS i_pod_poh_id, 
												purchase_order_detail.i_p_id AS i_pod_p_id, 
												purchase_order_detail.i_quantity_ordered AS i_pod_quantity_ordered';

	public $s_received_po_header_fields = 'received_po_header.i_id AS i_rpoh_id, 
											received_po_header.i_poh_id AS i_rpoh_poh_id, 
											received_po_header.i_id_id AS i_rpoh_id_id, 
											received_po_header.i_u_received_by AS i_rpoh_u_received_by, 
											received_po_header.i_u_approved_by AS i_rpoh_u_approved_by, 
											received_po_header.s_date_registration AS s_rpoh_date_registration, 
											received_po_header.s_comment AS s_rpoh_comment,
											received_po_header.i_rposn_id AS i_rpoh_rposn_id';
											
	public $s_received_po_status_names_fields = 'received_po_status_names.i_id AS i_rposn_id,
												received_po_status_names.s_name AS s_rposn_name';

	public $s_received_po_detail_fields = 'received_po_detail.i_rpoh_id AS i_rpod_rpoh_id, 
											received_po_detail.i_p_id AS i_rpod_p_id, 
											received_po_detail.i_quantity_received AS i_rpod_quantity_received, 
											received_po_detail.s_product_serial AS s_rpod_product_serial';
	
	public $s_received_to_header_fields = 'received_to_header.i_id AS i_rtoh_id, 
											received_to_header.i_toh_id AS i_rtoh_toh_id, 
											received_to_header.i_id_id AS i_rtoh_id_id, 
											received_to_header.i_u_prepared_by AS i_rtoh_u_prepared_by, 
											received_to_header.i_u_received_by AS i_rtoh_u_received_by, 
											received_to_header.s_date_registration AS s_rtoh_date_registration, 
											received_to_header.s_comment AS s_rtoh_comment,
											received_to_header.i_rtosn_id AS i_rtoh_rtosn_id';
											
	public $s_received_to_status_names_fields = 'received_to_status_names.i_id AS i_rtosn_id,
												received_to_status_names.s_name AS s_rtosn_name';
	
	public $s_received_to_detail_fields = 'received_to_detail.i_rtoh_id AS i_rtod_rtoh_id, 
											received_to_detail.i_p_id AS i_rtod_p_id, 
											received_to_detail.i_quantity_received AS i_rtod_quantity_received, 
											received_to_detail.s_product_serial AS s_rtod_product_serial';
	
	public $s_sales_invoice_header_fields = 'sales_invoice_header.i_id AS i_sih_id, 
											sales_invoice_header.i_id_id AS i_sih_id_id, 
											sales_invoice_header.i_u_prepared_by AS i_sih_u_prepared_by, 
											sales_invoice_header.i_u_approved_by AS i_sih_u_approved_by, 
											sales_invoice_header.s_date_registration AS s_sih_date_registration, 
											sales_invoice_header.s_title AS s_sih_title, 
											sales_invoice_header.s_comment AS s_sih_comment, 
											sales_invoice_header.i_sisn_id AS i_sih_sisn_id';

	public $s_sales_invoice_status_names_fields = 'sales_invoice_status_names.i_id AS i_sisn_id,
													sales_invoice_status_names.s_name AS s_sisn_name';

	public $s_sales_invoice_revision_fields = 'sales_invoice_revision.i_id AS i_sir_id, 
												sales_invoice_revision.i_sih_id AS i_sir_sih_id, 
												sales_invoice_revision.s_date_updated AS s_sir_date_updated, 
												sales_invoice_revision.i_u_revised_by AS i_sir_u_revised_by, 
												sales_invoice_revision.s_comment AS s_sir_comment';

	public $s_sales_invoice_detail_fields = 'sales_invoice_detail.i_sih_id AS i_sid_sih_id, 
											sales_invoice_detail.i_p_id AS i_sid_p_id, 
											sales_invoice_detail.i_quantity_to_release AS i_sid_quantity_to_release';
	
	public $s_transfer_order_header_fields = 'transfer_order_header.i_id AS i_toh_id, 
											transfer_order_header.i_releasing_id_id AS i_toh_releasing_id_id, 
											transfer_order_header.i_receiving_id_id AS i_toh_receiving_id_id, 
											transfer_order_header.i_u_prepared_by AS i_toh_u_prepared_by, 
											transfer_order_header.i_u_approved_by AS i_toh_u_approved_by, 
											transfer_order_header.s_date_registration AS s_toh_date_registration, 
											transfer_order_header.s_title AS s_toh_title, 
											transfer_order_header.s_transfer_description AS s_toh_transfer_description, 
											transfer_order_header.s_comment AS s_toh_comment, 
											transfer_order_header.i_tosn_id	 AS i_toh_tosn_id';
	
	public $s_transfer_order_status_names_fields = 'transfer_order_status_names.i_id AS i_tosn_id, 
													transfer_order_status_names.s_name AS s_tosn_name';
	
	public $s_transfer_order_detail_fields = 'transfer_order_detail.i_toh_id AS i_tod_toh_id, 
											transfer_order_detail.i_p_id AS i_tod_p_id, 
											transfer_order_detail.i_quantity_transfering AS i_tod_quantity_transfering, 
											transfer_order_detail.s_product_serial AS s_tod_product_serial';

	public $s_users_fields = 	'users.i_id AS i_u_id, 
								users.s_date_registration AS s_u_date_registration, 
								users.s_email AS s_u_email, 
								users.s_username AS s_u_username, 
								users.s_firstname AS s_u_firstname, 
								users.s_lastname AS s_u_lastname, 
								users.s_password AS s_u_password, 
								users.s_unique_key AS s_u_unique_key, 
								users.i_usn_id AS i_u_usn_id';
	
	public $s_user_roles_fields = 	'user_roles.i_u_id AS i_ur_user_id,
									user_roles.i_urn_id AS i_ur_role_name_id';

	public $s_user_role_names_fields = 		'user_role_names.i_id AS i_urn_id,
											user_role_names.s_name AS s_urn_name';

	public $s_user_status_names_fields = 	'user_status_names.i_id AS i_usn_id,
											user_status_names.s_name AS s_usn_name';
											
	public $s_wrong_send_po_header_fields = 'wrong_send_po_header.i_id AS i_wspoh_id, 
											wrong_send_po_header.i_poh_id AS i_wspoh_poh_id, 
											wrong_send_po_header.i_id_id AS i_wspoh_id_id, 
											wrong_send_po_header.i_u_received_by AS i_wspoh_u_received_by, 
											wrong_send_po_header.i_u_approved_by AS i_wspoh_u_approved_by, 
											wrong_send_po_header.s_date_registration AS s_wspoh_date_registration, 
											wrong_send_po_header.s_comment AS s_wspoh_comment,
											wrong_send_po_header.i_wsposn_id AS i_wspoh_wsposn_id';

	public $s_wrong_send_po_status_names_fields = 'wrong_send_po_status_names.i_id AS i_wsposn_id, 
													wrong_send_po_status_names.s_name AS s_wsposn_name';
											
	public $s_wrong_send_po_detail_fields = 'wrong_send_po_detail.i_wspoh_id AS i_wspod_wspoh_id, 
											wrong_send_po_detail.i_p_id AS i_wspod_p_id, 
											wrong_send_po_detail.i_quantity_ws AS i_wspod_quantity_ws, 
											wrong_send_po_detail.s_product_serial AS s_wspod_product_serial';

	public $s_product_reorder_alert_header_fields = 'product_reorder_alert_header.i_id AS i_prah_id, 
													product_reorder_alert_header.s_date_registration AS s_prah_date, 
													product_reorder_alert_header.s_name AS s_prah_name, 
													product_reorder_alert_header.i_p_id AS i_prah_p_id, 
													product_reorder_alert_header.i_qty AS i_prah_qty, 
													product_reorder_alert_header.i_prah_id AS i_prah_prasn_id';






	
	
}