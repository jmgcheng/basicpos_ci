<?php
class m_user extends CI_Model{
	
	public $s_users_fields = 	'users.i_id AS i_u_id, 
								users.s_date_registration AS s_u_date_registration, 
								users.s_email AS s_u_email, 
								users.s_username AS s_u_username, 
								users.s_firstname AS s_u_firstname, 
								users.s_lastname AS s_u_lastname, 
								users.s_password AS s_u_password, 
								users.s_unique_key AS s_u_unique_key, 
								users.i_usn_id AS i_u_usn_id';
	
	public $s_user_roles_fields = 	'user_roles.i_u_id AS i_ur_user_id,
									user_roles.i_urn_id AS i_ur_role_name_id';

	public $s_user_role_names_fields = 		'user_role_names.i_id AS i_urn_id,
											user_role_names.s_name AS s_urn_name';

	public $s_user_status_names_fields = 	'user_status_names.i_id AS i_usn_id,
											user_status_names.s_name AS s_usn_name';
	
	
	/**
	* add_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_u( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('users', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_urn( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('user_role_names', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_usn( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('user_status_names', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* insert_batch_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_ur( $a_params = array() )
	{
		$a_result = array();
		
		/*
			$a_params = array(
							   array(
								  'i_post_id' => 1,
								  'i_post_category_id' => 3
							   )
			);
		*/

		$this->db->insert_batch('user_roles', $a_params);
		$a_result['i_query_result'] = 1;
		
		return $a_result;
	}
	
	
	
	/**
	* get_u_by_upasswordnuemailouusername
	*
	* @desc 	get user data in 'users' table by 'users.s_password' AND '( users.s_email OR users.s_username )'
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_u_by_upasswordnuemailousername( $a_params = array() )
	{
		$a_result = array();

		if( !empty($a_params) && !empty($a_params['s_u_email_or_username']) && !empty($a_params['s_u_password']) )
		{
			$this->db->select( $this->s_users_fields );
			$this->db->where('users.s_password', $a_params['s_u_password']);
			$this->db->where("(users.s_email = '" . $a_params['s_u_email_or_username'] . "' OR users.s_username = '" . $a_params['s_u_email_or_username'] . "')"); 
			$this->db->limit(1);
			$o_query_result = $this->db->get('users');

			$a_result = $o_query_result->row_array();
		}
		
		return $a_result;
	}
	
	
	/**
	* get_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_ur( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_user_roles_fields );

		$o_query_result = $this->db->get('user_roles');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	
	/**
	* get_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_urn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_user_role_names_fields );

		$o_query_result = $this->db->get('user_role_names');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	
	/**
	* get_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_usn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_user_role_names_fields );

		$o_query_result = $this->db->get('user_status_names');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
		
	/**
	* get_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_u( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_users_fields );

		$o_query_result = $this->db->get('users');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_u( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_users_fields );

		$o_query_result = $this->db->get('users');

		return $o_query_result->num_rows();
	}
	
	
	
	/**
	* update_urn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_urn( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('user_role_names', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	
	
	/**
	* update_usn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_usn( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('user_status_names', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
		
	
	/**
	* update_u
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_u( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('users', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
		
	
	/**
	* delete_ur
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_ur( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}

			$i_result = $this->db->delete('user_roles'); 
		}

		return $i_result;
	}
	
}