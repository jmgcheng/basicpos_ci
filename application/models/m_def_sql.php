<?php
class m_def_sql extends CI_Model{
	
	
	/**
	* create_data
	*
	* @desc 		a default way to insert data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function create_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
			&& 	isset($a_params['a_new_data']) && !empty($a_params['a_new_data']) 
		)
		{
			$i_result = $this->db->insert( $a_params['s_table_name'], $a_params['a_new_data'] ); 
			$a_result['i_sql_result'] = $i_result;
			$a_result['i_insert_id'] = $this->db->insert_id();
		}

		return $a_result;
	}
	
	
	/**
	* create_batch_data
	*
	* @desc 		a default way to insert batch data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function create_batch_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
			&& 	isset($a_params['a_new_data']) && !empty($a_params['a_new_data']) 
		)
		{
			$this->db->insert_batch( $a_params['s_table_name'], $a_params['a_new_data'] ); 
			$a_result['i_sql_result'] = 1;
		}
		
		return $a_result;
	}
	
	
	/**
	* read_data
	*
	* @desc 		a default way to read data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function read_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 
				isset($a_params['s_table_fields']) && !empty($a_params['s_table_fields']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
			{
				foreach( $a_params['a_where'] AS $a_where_details )
				{
					$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
				}
			}
			
			if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
			{
				foreach( $a_params['a_order_by'] AS $s_order_by_details )
				{
					$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
				}
			}
			
			if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
			{
				$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
			}
			
			$this->db->select( $a_params['s_table_fields'] );
			$this->db->from( $a_params['s_table_name'] );
			
			if( isset($a_params['a_table_join']) && !empty($a_params['a_table_join']) )
			{
				foreach( $a_params['a_table_join'] AS $a_table_join_details )
				{
					$this->db->join( $a_table_join_details['s_table_join_name'], $a_table_join_details['s_table_join_condition'] );
				}
			}
			
			$o_query_result = $this->db->get();
			//$o_query_result = $this->db->get( $a_params['s_table_name'] );
			
			/*
				echo $this->db->last_query();
			*/
			
			$a_result = $o_query_result->result_array();
		}

		return $a_result;
	}
	
	
	/**
	* read_count_data
	*
	* @desc 		a default way to count the read data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function read_count_data( $a_params = array() )
	{
		$a_result = array();
		$a_result['i_num_rows'] = 0;
		
		if( 
				isset($a_params['s_table_fields']) && !empty($a_params['s_table_fields']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
			{
				foreach( $a_params['a_where'] AS $a_where_details )
				{
					$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
				}
			}
			
			$this->db->select( $a_params['s_table_fields'] );
			$this->db->from( $a_params['s_table_name'] );
			
			if( isset($a_params['a_table_join']) && !empty($a_params['a_table_join']) )
			{
				foreach( $a_params['a_table_join'] AS $a_table_join_details )
				{
					$this->db->join( $a_table_join_details['s_table_join_name'], $a_table_join_details['s_table_join_condition'] );
				}
			}

			$o_query_result = $this->db->get();
			//$o_query_result = $this->db->get( $a_params['s_table_name'] );
			/*
				echo $this->db->last_query();
			*/
			
			$a_result['i_num_rows'] = $o_query_result->num_rows();
		}

		return $a_result;
	}
	
	
	/**
	* update_data
	*
	* @desc 		a default way to update data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function update_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 
				isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) 
			&&	isset($a_params['a_where']) && !empty($a_params['a_where']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
			{
				foreach( $a_params['a_where'] AS $a_where_details )
				{
					$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
				}
			}

			$this->db->update( $a_params['s_table_name'] , $a_params['a_update_data'] ); 
			$a_result['i_sql_result'] = 1;
			
		}

		return $a_result;
	}
	
	
	/**
	* delete_data
	*
	* @desc 		a default way to delete data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
	**/
	public function delete_data( $a_params = array() )
	{
		$a_result = array();

		if( 
				isset($a_params['a_where']) && !empty($a_params['a_where']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}

			$i_result = $this->db->delete( $a_params['s_table_name'] ); 
			$a_result['i_sql_result'] = $i_result;
		}

		return $a_result;
	}
	
}