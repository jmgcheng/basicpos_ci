<?php
class m_depot extends CI_Model{
	
	
	public $s_inventory_depot_fields = 'inventory_depot.i_id AS i_id_id, 
										inventory_depot.i_idt_id AS i_id_idt_id, 
										inventory_depot.s_name AS s_id_name, 
										inventory_depot.s_description AS s_id_description, 
										inventory_depot.s_address AS s_id_address';
	
	
	public $s_inventory_depot_type_fields = 'inventory_depot_type.i_id AS i_idt_id, 
											inventory_depot_type.s_type AS s_idt_type';
	
	
	public $s_inventory_depot_users_fields = 'inventory_depot_users.i_u_id AS i_idu_u_id, 
											inventory_depot_users.i_id_id AS i_idu_id_id';
	
	
	/**
	* add_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_id( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('inventory_depot', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_idt( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('inventory_depot_type', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* insert_batch_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_idu( $a_params = array() )
	{
		$a_result = array();
		
		/*
			$a_params = array(
							   array(
								  'i_post_id' => 1,
								  'i_post_category_id' => 3
							   )
			);
		*/

		$this->db->insert_batch('inventory_depot_users', $a_params);
		$a_result['i_query_result'] = 1;
		
		return $a_result;
	}
	
	
	/**
	* get_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_id( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_inventory_depot_fields );

		$o_query_result = $this->db->get('inventory_depot');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_id( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_inventory_depot_fields );

		$o_query_result = $this->db->get('inventory_depot');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_idt( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_inventory_depot_type_fields );

		$o_query_result = $this->db->get('inventory_depot_type');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_idt( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_inventory_depot_type_fields );

		$o_query_result = $this->db->get('inventory_depot_type');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_idu( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_inventory_depot_users_fields );

		$o_query_result = $this->db->get('inventory_depot_users');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_idu( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_inventory_depot_users_fields );

		$o_query_result = $this->db->get('inventory_depot_users');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* update_id
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_id( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('inventory_depot', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_idt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_idt( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('inventory_depot_type', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* delete_idu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_idu( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}

			$i_result = $this->db->delete('inventory_depot_users'); 
		}

		return $i_result;
	}
	
}