<?php
class m_products extends CI_Model{


	public $s_products_fields = 'products.i_id AS i_p_id, 
								products.s_date_registration AS s_p_date_registration, 
								products.s_name AS s_p_name, 
								products.s_model AS s_p_model, 
								products.s_dimension AS s_p_dimension, 
								products.s_color AS s_p_color, 
								products.s_unit AS s_p_unit, 
								products.s_description AS s_p_description, 
								products.i_psn_id AS i_p_psn_id';

								
	public $s_product_dimensions_fields = 'product_dimensions.i_id AS i_pd_id,
										product_dimensions.s_name AS s_pd_name';
										
										
	public $s_product_colors_fields = 'product_colors.i_id AS i_pc_id,
										product_colors.s_name AS s_pc_name';
										
										
	public $s_product_units_fields = 'product_units.i_id AS i_pu_id,
										product_units.s_name AS s_pu_name';									
										
										
										
	public $s_product_status_names_fields = 'product_status_names.i_id AS i_psn_id,
											product_status_names.s_name AS s_psn_name';
								
								
	public $s_product_tag_names_fields = 'product_tag_names.i_id AS i_ptn_id,
										product_tag_names.s_name AS s_ptn_name';

										
	public $s_product_tags_fields = 'product_tags.i_p_id AS i_pt_p_id, 
										product_tags.i_ptn_id AS i_pt_ptn_id';

										
	/**
	* add_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_p( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('products', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pd( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('product_dimensions', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pc( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('product_colors', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_pu( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('product_units', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_psn( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('product_status_names', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* add_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function add_ptn( $a_params = array() )
	{
		$a_result = array();
		
		$i_result = $this->db->insert('s_product_tag_names_fields', $a_params); 
		
		$a_result['i_query_result'] = $i_result;
		$a_result['i_insert_id'] = $this->db->insert_id();

		return $a_result;
	}
	
	
	/**
	* insert_batch_pt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function insert_batch_pt( $a_params = array() )
	{
		$a_result = array();
		
		/*
			$a_params = array(
							   array(
								  'i_post_id' => 1,
								  'i_post_category_id' => 3
							   )
			);
		*/

		$this->db->insert_batch('product_tags', $a_params);
		$a_result['i_query_result'] = 1;
		
		return $a_result;
	}
	
	
	/**
	* get_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_p( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_products_fields );

		$o_query_result = $this->db->get('products');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_p( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_products_fields );

		$o_query_result = $this->db->get('products');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pd( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_product_dimensions_fields );

		$o_query_result = $this->db->get('product_dimensions');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pd( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_product_dimensions_fields );

		$o_query_result = $this->db->get('product_dimensions');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pc( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_product_colors_fields );

		$o_query_result = $this->db->get('product_colors');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pc( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_product_colors_fields );

		$o_query_result = $this->db->get('product_colors');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_pu( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_product_units_fields );

		$o_query_result = $this->db->get('product_units');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_pu( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_product_units_fields );

		$o_query_result = $this->db->get('product_units');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_psn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_product_status_names_fields );

		$o_query_result = $this->db->get('product_status_names');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_psn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_product_status_names_fields );

		$o_query_result = $this->db->get('product_status_names');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* get_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function get_ptn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
		{
			foreach( $a_params['a_order_by'] AS $s_order_by_details )
			{
				$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
			}
		}
		
		if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
		{
			$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
		}
		
		$this->db->select( $this->s_product_tag_names_fields );

		$o_query_result = $this->db->get('product_tag_names');
		/*
			echo $this->db->last_query();
		*/
		
		return $o_query_result->result_array();
	}
	
	
	/**
	* count_get_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function count_get_ptn( $a_params = array() )
	{
		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}
		
		$this->db->select( $this->s_product_tag_names_fields );

		$o_query_result = $this->db->get('product_tag_names');

		return $o_query_result->num_rows();
	}
	
	
	/**
	* update_p
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_p( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('products', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_pd
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_pd( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('product_dimensions', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_pc
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_pc( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('product_colors', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_pu
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_pu( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('product_units', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_psn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_psn( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('product_status_names', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* update_ptn
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	int
	*
	**/
	public function update_ptn( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}
		}

		if( isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) )
		{
			$this->db->update('product_tag_names', $a_params['a_update_data']); 
			$i_result = 1;
		}

		return $i_result;
	}
	
	
	/**
	* delete_pt
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function delete_pt( $a_params = array() )
	{
		$i_result = 0;

		if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}

			$i_result = $this->db->delete('product_tags'); 
		}

		return $i_result;
	}
	
}