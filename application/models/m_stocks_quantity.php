<?php
class m_stocks_quantity extends CI_Model{


	/**
	* read_stocks_quantity
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_stocks_quantity( $a_params = array() )
	{
		$a_result = array();
		$s_sql = '';

		
		/*
			print_r($a_params);
			exit();
			Array
			(
				[i_quantity_available] => 0
				[i_product_status] => 1
				[i_result_limit] => 500
				[i_result_offset] => 0
			)
			
			
			$a_params['i_pc_id'] = 0;
			$a_params['i_pd_id'] = 0;
			$a_params['i_id_id'] = 0;
			$a_params['i_qa'] = 0;
			$a_params['i_psn_id'] = 0;
			$a_params['s_sort'] = '';
			$a_params['s_order'] = '';
			$a_params['i_limit'] = 500;
			$a_params['i_offset'] = 0;
		*/
			
		
		$s_sql = "
					SELECT
						*
					FROM
						(
							SELECT
								products.i_id AS 'i_p_id',
								products.s_name AS 's_p_name',
								products.s_model AS 's_p_model',
								products.i_qty_low_alert AS 'i_p_qty_low_alert',
								COALESCE(tbl_included_inventory.i_product_total,0) AS 'i_included_inventory_total', 
								COALESCE(tbl_received_po.i_product_total,0) AS 'i_received_purchased_order_total', 
								COALESCE(tbl_received_to.i_product_total,0) AS 'i_received_transfer_order_total', 
								COALESCE(tbl_excluded_inventory.i_product_total,0) AS 'i_excluded_inventory_total', 
								COALESCE(tbl_transfer_order.i_product_total,0) AS 'i_transfer_order_total', 
								COALESCE(tbl_official_receipt.i_product_total,0) AS 'i_official_receipt_total', 
								COALESCE(tbl_missed_po.i_product_total,0) AS 'i_missed_purchased_order_total', 
								COALESCE(tbl_purchased_order.i_product_total,0) AS 'i_purchased_order_total', 
								COALESCE(tbl_wrong_send_po.i_product_total,0) AS 'i_wrong_send_purchased_order_total', 
								COALESCE(tbl_sales_return.i_product_total,0) AS 'i_sales_return_total', 
								
								(
									COALESCE(tbl_purchased_order.i_product_total,0)
									-
									(
										COALESCE(tbl_received_po.i_product_total,0)
										+
										COALESCE(tbl_missed_po.i_product_total,0)
									)
								) AS 'i_af',
								
								
								
								(
									COALESCE(tbl_excluded_inventory.i_product_total,0) +
									COALESCE(tbl_transfer_order.i_product_total,0) +
									COALESCE(tbl_official_receipt.i_product_total,0)
								) AS 'i_tr', 
								
								
								
								(
									COALESCE(tbl_included_inventory.i_product_total,0) +
									COALESCE(tbl_received_po.i_product_total,0) +
									COALESCE(tbl_received_to.i_product_total,0) +
									COALESCE(tbl_sales_return.i_product_total,0)
								) -
								(
									COALESCE(tbl_excluded_inventory.i_product_total,0) +
									COALESCE(tbl_transfer_order.i_product_total,0) +
									COALESCE(tbl_official_receipt.i_product_total,0)
								) 
								+ COALESCE(tbl_wrong_send_po.i_product_total,0)
								AS 'i_quantity_available',

								tbl_product_colors.i_id AS i_pc_id,
								tbl_product_dimensions.i_id AS i_pd_id,
								products.i_psn_id AS i_psn_id,
								
								tbl_product_colors.s_name AS s_pc_name,
								tbl_product_dimensions.s_name AS s_pd_name,
								tbl_product_units.s_name AS s_pu_name
								
							FROM 
								products
							LEFT JOIN 
								(
										SELECT
											included_inventory_detail.i_p_id,
											SUM(included_inventory_detail.i_quantity_included) AS i_product_total
										FROM
											included_inventory_header
										LEFT JOIN
											included_inventory_detail
											ON
											included_inventory_header.i_id = included_inventory_detail.i_iih_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														included_inventory_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											included_inventory_detail.i_p_id
								) 
								AS tbl_included_inventory ON tbl_included_inventory.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											received_po_detail.i_p_id,
											SUM(received_po_detail.i_quantity_received) AS i_product_total
										FROM
											received_po_header
										LEFT JOIN
											received_po_detail
											ON
											received_po_header.i_id = received_po_detail.i_rpoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														received_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											received_po_detail.i_p_id
								) 
								AS tbl_received_po ON tbl_received_po.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											received_to_detail.i_p_id,
											SUM(received_to_detail.i_quantity_received) AS i_product_total
										FROM
											received_to_header
										LEFT JOIN
											received_to_detail
											ON
											received_to_header.i_id = received_to_detail.i_rtoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														received_to_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											received_to_detail.i_p_id
								) 
								AS tbl_received_to ON tbl_received_to.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											excluded_inventory_detail.i_p_id,
											SUM(excluded_inventory_detail.i_quantity_excluded) AS i_product_total
										FROM
											excluded_inventory_header
										LEFT JOIN
											excluded_inventory_detail
											ON
											excluded_inventory_header.i_id = excluded_inventory_detail.i_eih_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														excluded_inventory_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											excluded_inventory_detail.i_p_id
								) 
								AS tbl_excluded_inventory ON tbl_excluded_inventory.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											transfer_order_detail.i_p_id,
											SUM(transfer_order_detail.i_quantity_transfering) AS i_product_total
										FROM
											transfer_order_header
										LEFT JOIN
											transfer_order_detail
											ON
											transfer_order_header.i_id = transfer_order_detail.i_toh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														transfer_order_header.i_releasing_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											transfer_order_detail.i_p_id
								) 
								AS tbl_transfer_order ON tbl_transfer_order.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											official_receipt_detail.i_p_id,
											SUM(official_receipt_detail.i_quantity_released) AS i_product_total
										FROM
											official_receipt_header
										LEFT JOIN
											official_receipt_detail
											ON
											official_receipt_header.i_id = official_receipt_detail.i_orh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														official_receipt_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											official_receipt_detail.i_p_id
								) 
								AS tbl_official_receipt ON tbl_official_receipt.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											missed_po_detail.i_p_id,
											SUM(missed_po_detail.i_quantity_missed) AS i_product_total
										FROM
											missed_po_header
										LEFT JOIN
											missed_po_detail
											ON
											missed_po_header.i_id = missed_po_detail.i_mpoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														missed_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "

										GROUP BY
											missed_po_detail.i_p_id
								) 
								AS tbl_missed_po ON tbl_missed_po.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											purchase_order_detail.i_p_id,
											SUM(purchase_order_detail.i_quantity_ordered) AS i_product_total
										FROM
											purchase_order_header
										LEFT JOIN
											purchase_order_detail
											ON
											purchase_order_header.i_id = purchase_order_detail.i_poh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														purchase_order_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "

										GROUP BY
											purchase_order_detail.i_p_id
								) 
								AS tbl_purchased_order ON tbl_purchased_order.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											wrong_send_po_detail.i_p_id,
											SUM(wrong_send_po_detail.i_quantity_ws) AS i_product_total
										FROM
											wrong_send_po_header
										LEFT JOIN
											wrong_send_po_detail
											ON
											wrong_send_po_header.i_id = wrong_send_po_detail.i_wspoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														wrong_send_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "

										GROUP BY
											wrong_send_po_detail.i_p_id
								) 
								AS tbl_wrong_send_po ON tbl_wrong_send_po.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											sales_return_detail.i_p_id,
											SUM(sales_return_detail.i_quantity_received) AS i_product_total
										FROM
											sales_return_header
										LEFT JOIN
											sales_return_detail
											ON
											sales_return_header.i_id = sales_return_detail.i_srh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														sales_return_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "

										GROUP BY
											sales_return_detail.i_p_id
								) 
								AS tbl_sales_return ON tbl_sales_return.i_p_id = products.i_id
							
							
							LEFT JOIN
								(
										SELECT
											product_colors.i_id,
											product_colors.s_name
										FROM
											product_colors
								)
								AS tbl_product_colors ON tbl_product_colors.i_id = products.i_pc_id
							LEFT JOIN
								(
										SELECT
											product_dimensions.i_id,
											product_dimensions.s_name
										FROM
											product_dimensions
								)
								AS tbl_product_dimensions ON tbl_product_dimensions.i_id = products.i_pd_id
							LEFT JOIN
								(
										SELECT
											product_units.i_id,
											product_units.s_name
										FROM
											product_units
								)
								AS tbl_product_units ON tbl_product_units.i_id = products.i_pc_id							

							
						) inventory_quantity
					" .
						(
							isset($a_params['i_qa']) && !empty($a_params['i_qa']) ?
							' WHERE 
									inventory_quantity.i_quantity_available >= ' . $a_params['i_qa']
							:
							' WHERE 
									inventory_quantity.i_quantity_available >= 0'
						)
					. "
					
					" .
						(
							isset($a_params['i_pd_id']) && !empty($a_params['i_pd_id']) ?
							' AND 
									inventory_quantity.i_pd_id = ' . $a_params['i_pd_id']
							:
							''
						)
					. "
					
					" .
						(
							isset($a_params['i_pc_id']) && !empty($a_params['i_pc_id']) ?
							' AND 
									inventory_quantity.i_pc_id = ' . $a_params['i_pc_id']
							:
							''
						)
					. "
					
					" .
						(
							isset($a_params['i_psn_id']) && !empty($a_params['i_psn_id']) ?
							' AND 
									inventory_quantity.i_psn_id = ' . $a_params['i_psn_id']
							:
							''
						)
					. "
					
					
					
					
					ORDER BY
						" . 
							(
								( isset($a_params['s_sort']) && !empty($a_params['s_sort']) && isset($a_params['s_order']) && !empty($a_params['s_order']) ) ? 
								$a_params['s_sort'] . ' ' . $a_params['s_order']
								: 
								'
									inventory_quantity.s_p_name ASC,
									inventory_quantity.s_pc_name ASC,
									inventory_quantity.s_pd_name ASC
								'
							)
						. "

				" . 
					(
						( isset($a_params['i_limit']) && isset($a_params['i_offset']) ) ? 
						' LIMIT ' 
							. $a_params['i_limit'] . 
						' OFFSET ' 
							. $a_params['i_offset'] 
						: 
						''
					)
				. "

		";
		/*
		echo $s_sql;
		exit();
		*/
		
		
		$o_query_result = $this->db->query($s_sql);
		/*
			echo $this->db->last_query();
			exit();
		*/
		
		
		$a_result = $o_query_result->result_array();
		/*
			print_r($a_result);
			exit();
		*/
		
		return $a_result;
	}
	
	
	/**
	* read_count_stocks_quantity
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function read_count_stocks_quantity( $a_params = array() )
	{
		$a_result = array();
		$s_sql = '';
		
		
		/*
			$a_params['i_pc_id'] = 0;
			$a_params['i_pd_id'] = 0;
			$a_params['i_id_id'] = 0;
			$a_params['i_qa'] = 0;
			$a_params['i_psn_id'] = 0;
			$a_params['s_sort'] = '';
			$a_params['s_order'] = '';
			$a_params['i_limit'] = 500;
			$a_params['i_offset'] = 0;
		*/
		
		
		$s_sql = "
					SELECT
						*
					FROM
						(
							SELECT
								products.i_id AS 'i_p_id',
								products.s_name AS 's_p_name',
								products.s_model AS 's_p_model',
								products.i_qty_low_alert AS 'i_p_qty_low_alert',
								COALESCE(tbl_included_inventory.i_product_total,0) AS 'i_included_inventory_total', 
								COALESCE(tbl_received_po.i_product_total,0) AS 'i_received_purchased_order_total', 
								COALESCE(tbl_received_to.i_product_total,0) AS 'i_received_transfer_order_total', 
								COALESCE(tbl_excluded_inventory.i_product_total,0) AS 'i_excluded_inventory_total', 
								COALESCE(tbl_transfer_order.i_product_total,0) AS 'i_transfer_order_total', 
								COALESCE(tbl_official_receipt.i_product_total,0) AS 'i_official_receipt_total', 
								COALESCE(tbl_missed_po.i_product_total,0) AS 'i_missed_purchased_order_total', 
								COALESCE(tbl_purchased_order.i_product_total,0) AS 'i_purchased_order_total', 
								COALESCE(tbl_wrong_send_po.i_product_total,0) AS 'i_wrong_send_purchased_order_total', 
								COALESCE(tbl_sales_return.i_product_total,0) AS 'i_sales_return_total', 
								
								
								(
									COALESCE(tbl_purchased_order.i_product_total,0)
									-
									(
										COALESCE(tbl_received_po.i_product_total,0)
										+
										COALESCE(tbl_missed_po.i_product_total,0)
									)
								) AS 'i_af',
								
								
								
								(
									COALESCE(tbl_excluded_inventory.i_product_total,0) +
									COALESCE(tbl_transfer_order.i_product_total,0) +
									COALESCE(tbl_official_receipt.i_product_total,0)
								) AS 'i_tr', 
								
								
								
								(
									COALESCE(tbl_included_inventory.i_product_total,0) +
									COALESCE(tbl_received_po.i_product_total,0) +
									COALESCE(tbl_received_to.i_product_total,0) +
									COALESCE(tbl_sales_return.i_product_total,0)
								) -
								(
									COALESCE(tbl_excluded_inventory.i_product_total,0) +
									COALESCE(tbl_transfer_order.i_product_total,0) +
									COALESCE(tbl_official_receipt.i_product_total,0)
								) 
								+ COALESCE(tbl_wrong_send_po.i_product_total,0)
								AS 'i_quantity_available',

								tbl_product_colors.i_id AS i_pc_id,
								tbl_product_dimensions.i_id AS i_pd_id,
								products.i_psn_id AS i_psn_id,
								
								tbl_product_colors.s_name AS s_pc_name,
								tbl_product_dimensions.s_name AS s_pd_name,
								tbl_product_units.s_name AS s_pu_name
								
							FROM 
								products
							LEFT JOIN 
								(
										SELECT
											included_inventory_detail.i_p_id,
											SUM(included_inventory_detail.i_quantity_included) AS i_product_total
										FROM
											included_inventory_header
										LEFT JOIN
											included_inventory_detail
											ON
											included_inventory_header.i_id = included_inventory_detail.i_iih_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														included_inventory_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											included_inventory_detail.i_p_id
								) 
								AS tbl_included_inventory ON tbl_included_inventory.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											received_po_detail.i_p_id,
											SUM(received_po_detail.i_quantity_received) AS i_product_total
										FROM
											received_po_header
										LEFT JOIN
											received_po_detail
											ON
											received_po_header.i_id = received_po_detail.i_rpoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														received_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											received_po_detail.i_p_id
								) 
								AS tbl_received_po ON tbl_received_po.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											received_to_detail.i_p_id,
											SUM(received_to_detail.i_quantity_received) AS i_product_total
										FROM
											received_to_header
										LEFT JOIN
											received_to_detail
											ON
											received_to_header.i_id = received_to_detail.i_rtoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														received_to_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
											
										GROUP BY
											received_to_detail.i_p_id
								) 
								AS tbl_received_to ON tbl_received_to.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											excluded_inventory_detail.i_p_id,
											SUM(excluded_inventory_detail.i_quantity_excluded) AS i_product_total
										FROM
											excluded_inventory_header
										LEFT JOIN
											excluded_inventory_detail
											ON
											excluded_inventory_header.i_id = excluded_inventory_detail.i_eih_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														excluded_inventory_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											excluded_inventory_detail.i_p_id
								) 
								AS tbl_excluded_inventory ON tbl_excluded_inventory.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											transfer_order_detail.i_p_id,
											SUM(transfer_order_detail.i_quantity_transfering) AS i_product_total
										FROM
											transfer_order_header
										LEFT JOIN
											transfer_order_detail
											ON
											transfer_order_header.i_id = transfer_order_detail.i_toh_id	
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														transfer_order_header.i_releasing_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
											
										GROUP BY
											transfer_order_detail.i_p_id
								) 
								AS tbl_transfer_order ON tbl_transfer_order.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											official_receipt_detail.i_p_id,
											SUM(official_receipt_detail.i_quantity_released) AS i_product_total
										FROM
											official_receipt_header
										LEFT JOIN
											official_receipt_detail
											ON
											official_receipt_header.i_id = official_receipt_detail.i_orh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														official_receipt_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
											
										GROUP BY
											official_receipt_detail.i_p_id
								) 
								AS tbl_official_receipt ON tbl_official_receipt.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											missed_po_detail.i_p_id,
											SUM(missed_po_detail.i_quantity_missed) AS i_product_total
										FROM
											missed_po_header
										LEFT JOIN
											missed_po_detail
											ON
											missed_po_header.i_id = missed_po_detail.i_mpoh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														missed_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
											
										GROUP BY
											missed_po_detail.i_p_id
								) 
								AS tbl_missed_po ON tbl_missed_po.i_p_id = products.i_id	
							LEFT JOIN 
								(
										SELECT
											purchase_order_detail.i_p_id,
											SUM(purchase_order_detail.i_quantity_ordered) AS i_product_total
										FROM
											purchase_order_header
										LEFT JOIN
											purchase_order_detail
											ON
											purchase_order_header.i_id = purchase_order_detail.i_poh_id	
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														purchase_order_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											purchase_order_detail.i_p_id
								) 
								AS tbl_purchased_order ON tbl_purchased_order.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											wrong_send_po_detail.i_p_id,
											SUM(wrong_send_po_detail.i_quantity_ws) AS i_product_total
										FROM
											wrong_send_po_header
										LEFT JOIN
											wrong_send_po_detail
											ON
											wrong_send_po_header.i_id = wrong_send_po_detail.i_wspoh_id	
											
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														wrong_send_po_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "
										
										GROUP BY
											wrong_send_po_detail.i_p_id
								) 
								AS tbl_wrong_send_po ON tbl_wrong_send_po.i_p_id = products.i_id
							LEFT JOIN 
								(
										SELECT
											sales_return_detail.i_p_id,
											SUM(sales_return_detail.i_quantity_received) AS i_product_total
										FROM
											sales_return_header
										LEFT JOIN
											sales_return_detail
											ON
											sales_return_header.i_id = sales_return_detail.i_srh_id	
										
										" .
											(
												isset($a_params['i_id_id']) && !empty($a_params['i_id_id']) ?
												' WHERE 
														sales_return_header.i_id_id = ' . $a_params['i_id_id']
												:
												''
											)
										. "

										GROUP BY
											sales_return_detail.i_p_id
								) 
								AS tbl_sales_return ON tbl_sales_return.i_p_id = products.i_id

							LEFT JOIN
								(
										SELECT
											product_colors.i_id,
											product_colors.s_name
										FROM
											product_colors
								)
								AS tbl_product_colors ON tbl_product_colors.i_id = products.i_pc_id
							LEFT JOIN
								(
										SELECT
											product_dimensions.i_id,
											product_dimensions.s_name
										FROM
											product_dimensions
								)
								AS tbl_product_dimensions ON tbl_product_dimensions.i_id = products.i_pd_id
							LEFT JOIN
								(
										SELECT
											product_units.i_id,
											product_units.s_name
										FROM
											product_units
								)
								AS tbl_product_units ON tbl_product_units.i_id = products.i_pc_id							
						
				
						) inventory_quantity
					" .
						(
							isset($a_params['i_qa']) && !empty($a_params['i_qa']) ?
							' WHERE 
									inventory_quantity.i_quantity_available >= ' . $a_params['i_qa']
							:
							' WHERE 
									inventory_quantity.i_quantity_available >= 0'
						)
					. "
					
					
					" .
						(
							isset($a_params['i_pd_id']) && !empty($a_params['i_pd_id']) ?
							' AND 
									inventory_quantity.i_pd_id = ' . $a_params['i_pd_id']
							:
							''
						)
					. "
					
					" .
						(
							isset($a_params['i_pc_id']) && !empty($a_params['i_pc_id']) ?
							' AND 
									inventory_quantity.i_pc_id = ' . $a_params['i_pc_id']
							:
							''
						)
					. "
					
					" .
						(
							isset($a_params['i_psn_id']) && !empty($a_params['i_psn_id']) ?
							' AND 
									inventory_quantity.i_psn_id = ' . $a_params['i_psn_id']
							:
							''
						)
					. "
					
		";
		/*
		echo $s_sql;
		exit();
		*/
		
		
		$o_query_result = $this->db->query($s_sql);
		/*
			echo $this->db->last_query();
			exit();
		*/
		
		
		$a_result['i_num_rows'] = $o_query_result->num_rows();
		/*
			print_r($a_result);
			exit();
		*/
		
		return $a_result;
	}
	
	
	
	
	
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
}