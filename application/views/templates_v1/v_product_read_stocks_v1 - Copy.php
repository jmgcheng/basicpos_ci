<?php
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show Stocks Quantity
					</h1>
					
					<br/>
					<p>
						for now this is still a default query. Check controller to see where clause. Echo model sql to see query.
						<br/>
						we need a lot of filter for this someday and its important to use dropdown option. Lets start with status then depot
					</p>
					<br/>
					<p>
						Color
						<select>
							<option>
							</option>
						</select>
						
						|
						
						Dimension
						<select>
							<option>
							</option>
						</select>
						
						|
						
						Status
						<select>
							<option>
								Activated
							</option>
						</select>
						
						|
						
						Depot
						<select>
							<option>
							</option>
						</select>
						
						|
						
						QTY Available >=
						<select>
							<option>
							</option>
						</select>
						
					</p>
						
						
						
						
						
						
						
						
				</header>
				
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
				<table class="clstbl_basicquery_1">
					<?php
						if( isset($a_stock_quantity_result) && !empty($a_stock_quantity_result) ) :
					?>
					<thead style="background-color:white;">
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Name
						</th>
						<th>
							Model
						</th>
						<th>
							Color
						</th>
						<th>
							Dimension
						</th>
						<th>
							II
						</th>
						<th>
							EI
						</th>
						<th>
							OT
						</th>
						<th>
							RF
						</th>
						<th>
							MS
						</th>
						<th>
							WS
						</th>
						<th>
							AF
						</th>
						<th title="Total Released">
							TR
						</th>
						<th title="Total Sold">
							TS
						</th>
						<th title="Quantity Available">
							QA
						</th>
						<th title="Need to reach 2 Doz">
							N2D
						</th>
						<th title="Need to reach 3 Doz">
							N3D
						</th>
						<th title="Need to reach 4 Doz">
							N4D
						</th>
					</tr>
					</thead>
						<?php
							foreach( $a_stock_quantity_result AS $a_stock_quantity_result_row ) :
						?>
					<tr>
						<td>
							<?php echo $a_stock_quantity_result_row['i_p_id']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_p_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_p_model']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_pc_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_pd_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_included_inventory_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_excluded_inventory_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_received_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_missed_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_wrong_send_purchased_order_total']; ?>
						</td>
						<td title="PO=<?php echo $a_stock_quantity_result_row['i_purchased_order_total']; ?> - (RPO=<?php echo $a_stock_quantity_result_row['i_received_purchased_order_total']; ?> + MPO=<?php echo $a_stock_quantity_result_row['i_missed_purchased_order_total']; ?>) ">
							<?php 
								echo 
									$a_stock_quantity_result_row['i_purchased_order_total']
									-
									(
										(
											$a_stock_quantity_result_row['i_received_purchased_order_total']
											+
											$a_stock_quantity_result_row['i_missed_purchased_order_total']
										)
									);
							?>
						</td>
						<td>
							<?php
								echo 
									$a_stock_quantity_result_row['i_excluded_inventory_total'] +
									$a_stock_quantity_result_row['i_transfer_order_total'] +
									$a_stock_quantity_result_row['i_official_receipt_total'];
							?>
						</td>
						<td>
							<?php
								echo 
									$a_stock_quantity_result_row['i_official_receipt_total'];
							?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 24 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 36 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 48 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
					</tr>
						<?php
							endforeach ;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif ;
					?>
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</main>
		
<?php
	endif;
?>		