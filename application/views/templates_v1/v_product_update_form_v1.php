		
		<main>
			<section class="clssection_basicupdate_1">
				<header>
					<h1>
						Products - Update Detail
					</h1>
				</header>
				
				<?php
					$s_frm_action_url = base_url() . 'product/update/';
					
					if( isset($a_product_row_result['i_p_id']) && !empty($a_product_row_result['i_p_id']) ) 
					{ 
						$s_frm_action_url = $s_frm_action_url . 'product_id/' . $a_product_row_result['i_p_id'] . '/';
					}
				?>
				<form id="frm_product_update" name="frm_product_update" action="<?php echo $s_frm_action_url; ?>" method="post">
					<table class="clstbl_basicupdateheader_1">
						
						<?php
							if(isset($a_product_row_result) && !empty($a_product_row_result)) :
							
						?>
						<tr>
							<th colspan="2">
								<input type="submit" value="Update">
							</th>
						</tr>
						<tr>
							<td>
								DB ID
							</td>
							<td>
								<?php echo $a_product_row_result['i_p_id']; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_product_update_name">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_product_update_name" name="txt_product_update_name" value="<?php echo $a_product_row_result['s_p_name']; ?>" />
								<?php if( isset($a_form_notice['s_txt_product_update_name_error']) && !empty($a_form_notice['s_txt_product_update_name_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_update_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_product_update_model">Model:</label>
							</td>
							<td>
								<input type="text" id="txt_product_update_model" name="txt_product_update_model" value="<?php echo $a_product_row_result['s_p_model']; ?>" />
								<?php if( isset($a_form_notice['s_txt_product_update_model_error']) && !empty($a_form_notice['s_txt_product_update_model_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_update_model_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						
						
						<?php 
							if( isset($a_product_dimensions_result) && !empty($a_product_dimensions_result) ) : 
						?>
						<tr>
							<td>
								Dimension
							</td>
							<td>
								<select name="opt_product_update_dimension">
									<?php
										foreach( $a_product_dimensions_result AS $a_product_dimensions_result_row ) :
									?>
									<option <?php if( isset($a_product_row_result['i_p_pd_id']) && !empty($a_product_row_result['i_p_pd_id']) && ( $a_product_row_result['i_p_pd_id'] == $a_product_dimensions_result_row['i_pd_id'] ) ) { echo 'selected="selected"'; } ?> value="<?php echo $a_product_dimensions_result_row['i_pd_id']; ?>">
										<?php echo $a_product_dimensions_result_row['s_pd_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_update_dimension_error']) && !empty($a_form_notice['s_opt_product_update_dimension_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_update_dimension_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						
						
						<?php 
							if( isset($a_product_colors_result) && !empty($a_product_colors_result) ) : 
						?>
						<tr>
							<td>
								Color
							</td>
							<td>
								<select name="opt_product_update_color">
									<?php
										foreach( $a_product_colors_result AS $a_product_colors_result_row ) :
									?>
									<option <?php if( isset($a_product_row_result['i_p_pd_id']) && !empty($a_product_row_result['i_p_pd_id']) && ( $a_product_row_result['i_p_pc_id'] == $a_product_colors_result_row['i_pc_id'] ) ) { echo 'selected="selected"'; } ?> value="<?php echo $a_product_colors_result_row['i_pc_id']; ?>">
										<?php echo $a_product_colors_result_row['s_pc_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_update_color_error']) && !empty($a_form_notice['s_opt_product_update_color_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_update_color_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						
						
						
						<?php 
							if( isset($a_product_units_result) && !empty($a_product_units_result) ) : 
						?>
						<tr>
							<td>
								Unit
							</td>
							<td>
								<select name="opt_product_update_unit">
									<?php
										foreach( $a_product_units_result AS $a_product_units_result_row ) :
									?>
									<option <?php if( isset($a_product_row_result['i_p_pd_id']) && !empty($a_product_row_result['i_p_pd_id']) && ( $a_product_row_result['i_p_pu_id'] == $a_product_units_result_row['i_pu_id'] ) ) { echo 'selected="selected"'; } ?> value="<?php echo $a_product_units_result_row['i_pu_id']; ?>">
										<?php echo $a_product_units_result_row['s_pu_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_update_unit_error']) && !empty($a_form_notice['s_opt_product_update_unit_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_update_unit_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						
						
						<tr>
							<td>
								Description
							</td>
							<td>
								<textarea id="txt_product_update_description" name="txt_product_update_description"><?php echo $a_product_row_result['s_p_description']; ?></textarea>
								<?php if( isset($a_form_notice['s_txt_product_update_description_error']) && !empty($a_form_notice['s_txt_product_update_description_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_update_description_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_product_update_qty_low_alert">Quantity Low Alert:</label>
							</td>
							<td>
								<input type="text" id="txt_product_update_qty_low_alert" name="txt_product_update_qty_low_alert" value="<?php echo $a_product_row_result['i_p_qty_low_alert']; ?>" />
								<?php if( isset($a_form_notice['s_txt_product_update_qty_low_alert_error']) && !empty($a_form_notice['s_txt_product_update_qty_low_alert_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_update_qty_low_alert_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php 
							if( isset($a_product_status_names_result) && !empty($a_product_status_names_result) ) : 
						?>
						<tr>
							<td>
								Product Status
							</td>
							<td>
								<select name="opt_product_update_status">
									<?php
										foreach( $a_product_status_names_result AS $a_product_status_names_result_row ) :
									?>
									<option <?php if( isset($a_product_row_result['i_p_pd_id']) && !empty($a_product_row_result['i_p_pd_id']) && ( $a_product_row_result['i_p_psn_id'] == $a_product_status_names_result_row['i_psn_id'] ) ) { echo 'selected="selected"'; } ?> value="<?php echo $a_product_status_names_result_row['i_psn_id']; ?>">
										<?php echo $a_product_status_names_result_row['s_psn_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_update_status_error']) && !empty($a_form_notice['s_opt_product_update_status_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_update_status_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						<?php
							else :
						?>
						<tr>
							<td>
								No Data for that ID
							</td>
						</tr>
						<?php
							endif ;
						?>
						
					</table>
					
				</form>
				
			</section>
		</main>
