		<header id="idheader_mainheader_1">
			<h1>
				<a href="<?php echo base_url(); ?>">
					Basic POS
				</a>
			</h1>
			
			<?php
				if( isset($s_view_site_responses) && !empty($s_view_site_responses) )
				{
					echo $s_view_site_responses;
				}
			?>
			
			<?php
				$a_user_details = $this->session->userdata('a_user_details');
				$a_user_roles_result = $this->session->userdata('a_user_roles_result');
				
				/*
				echo 'test';
				print_r($a_user_details);
				print_r($a_user_roles_result);
				echo 'test';
				*/
			?>
			
			
			<section id="idsection_mainmenu_1">
				<nav>
					<table>
						<tr>
							<td>
								<?php
									if( isset($a_user_details) && !empty($a_user_details) ) :
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
													||	array_key_exists( 7, $a_user_roles_result )
													||	array_key_exists( 8, $a_user_roles_result )
													||	array_key_exists( 9, $a_user_roles_result )
													||	array_key_exists( 12, $a_user_roles_result )
													||	array_key_exists( 13, $a_user_roles_result )
												)	
										) :
								?>
								<ul>
									<li>
										Transactions:
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 7, $a_user_roles_result )
													)	
											) :	
										?>
										<ul>
											<li>
												Purchase Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'purchase_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'purchase_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'purchase_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>
										
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 11, $a_user_roles_result )
													)	
											) :
										?>										
										<ul>
											<li>
												Received Purchase Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'received_purchase_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'received_purchase_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'received_purchase_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>		
										
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 8, $a_user_roles_result )
													)	
											) :	
										?>										
										<ul>
											<li>
												Sales Invoice
												<ul>
													<li>
														<a href="<?php echo base_url() . 'sales_invoice/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'sales_invoice/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'sales_invoice/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 9, $a_user_roles_result )
													)	
											) :	
										?>										
										<ul>
											<li>
												Official Receipt
												<ul>
													<li>
														<a href="<?php echo base_url() . 'official_receipt/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'official_receipt/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'official_receipt/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 12, $a_user_roles_result )
													)	
											) :	
										?>										
										<ul>
											<li>
												Missed Purchased Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'missed_purchase_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'missed_purchase_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'missed_purchase_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 13, $a_user_roles_result )
													)	
											) :	
										?>
										<ul>
											<li>
												Wrong Send Purchase Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'wrong_send_purchase_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'wrong_send_purchase_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'wrong_send_purchase_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>
									</li>
								</ul>
								<?php
										endif ;
									endif ;
								?>
							
							</td>
							
							<td>
								<?php
									if( isset($a_user_details) && !empty($a_user_details) ) :
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
													||	array_key_exists( 5, $a_user_roles_result )
													||	array_key_exists( 6, $a_user_roles_result )
													||	array_key_exists( 10, $a_user_roles_result )
													||	array_key_exists( 14, $a_user_roles_result )
												)	
										) :
								?>
								<ul>
									<li>
										Transactions:
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 5, $a_user_roles_result )
													)	
											) :
										?>
										<ul>
											<li>
												Include Inventory
												<ul>
													<li>
														<a href="<?php echo base_url() . 'included_inventory/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'included_inventory/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'included_inventory/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>													
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 6, $a_user_roles_result )
													)	
											) :
										?>										
										<ul>
											<li>
												Exclude Inventory
												<ul>
													<li>
														<a href="<?php echo base_url() . 'excluded_inventory/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'excluded_inventory/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'excluded_inventory/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>													
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 10, $a_user_roles_result )
													)	
											) :
										?>										
										<ul>
											<li>
												Delivery Receipt
												<ul>
													<li>
														<a href="<?php echo base_url() . 'delivery_receipt/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'delivery_receipt/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'delivery_receipt/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>													
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 14, $a_user_roles_result )
													)	
											) :
										?>										
										<ul>
											<li>
												Transfer Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'transfer_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'transfer_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'transfer_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>	
										
										
										<?php
											if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
												&& (
															array_key_exists( 1, $a_user_roles_result )
														||	array_key_exists( 15, $a_user_roles_result )
													)	
											) :
										?>										
										<ul>
											<li>
												Received Transfer Order
												<ul>
													<li>
														<a href="<?php echo base_url() . 'received_transfer_order/read_all'; ?>">
															Show All
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'received_transfer_order/read_all'; ?>">
															Update
														</a>
													</li>
													<li>
														<a href="<?php echo base_url() . 'received_transfer_order/create_form'; ?>">
															Create
														</a>
													</li>
												</ul>
											</li>
										</ul>
										<?php
											endif ;
										?>	



										
										
										
										
									</li>
								</ul>
								<?php
										endif ;
									endif ;
								?>
								
							</td>
							
							<td>
								<?php
									if( isset($a_user_details) && !empty($a_user_details) ) :
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
													||	array_key_exists( 3, $a_user_roles_result )
													||	array_key_exists( 4, $a_user_roles_result )
												)	
										) :
								?>
								<ul>
									<?php
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
													||	array_key_exists( 3, $a_user_roles_result )
												)	
										) :	
									?>
									<li>
										Products
										<ul>
											<li>
												<a href="<?php echo base_url() . 'product/read_all'; ?>">
													Show All
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() . 'product/read_all'; ?>">
													Update
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() . 'product/create_form'; ?>">
													Create
												</a>
											</li>
										</ul>
									</li>
									<?php
										endif ;
									?>
									<?php
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
													||	array_key_exists( 4, $a_user_roles_result )
												)	
										) :	
									?>									
									<li>
										Inventory Depot
										<ul>
											<li>
												<a href="#">
													Show All
												</a>
											</li>
											<li>
												<a href="#">
													Update
												</a>
											</li>
											<li>
												<a href="#">
													Create
												</a>
											</li>
										</ul>
									</li>
									<?php
										endif ;
									?>
									<?php
										if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
											&& (
														array_key_exists( 1, $a_user_roles_result )
												)	
										) :	
									?>									
									<li>
										Custom Query
										<ul>
											<li>
												<a href="<?php echo base_url() . 'product/read_stocks/'; ?>">
													Products - Stocks
												</a>
											</li>
										</ul>
									</li>
									<?php
										endif ;
									?>
								</ul>
								<?php
										endif ;
									endif ;
								?>
							</td>
							
							<td>
								<ul>
									<?php
										if( isset($a_user_details) && !empty($a_user_details) ) :
									?>
									<li>
										Account
										<ul>
											<li>
												<a href="#">
													Dashboard
												</a>
											</li>
											<li>
												<a href="#">
													Profile
												</a>
											</li>
										</ul>
									</li>
									<?php
										endif ;
									?>
									<li>
										User
										<ul>
											
											<?php
												if( isset($a_user_details) && !empty($a_user_details) ) :
											?>
											<li>
												<a href="<?php echo base_url() . 'user/logout'; ?>">
													Logout
												</a>
											</li>
											<?php
												if( 	isset($a_user_roles_result) && !empty($a_user_roles_result) 
													&& (
																array_key_exists( 1, $a_user_roles_result )
														)	
												) :
											?>
											<li>
												<a href="<?php echo base_url() . 'user/read_all'; ?>">
													Show All
												</a>
											</li>
											<?php
												endif ;
											?>
											<?php
												else :
											?>
											<li>
												<a href="<?php echo base_url() . 'user/register_form'; ?>">
													Register
												</a>
											</li>
											<li>
												<a href="#">
													Forgot Password
												</a>
											</li>
											<li>
												<a href="<?php echo base_url() . 'user/login_form'; ?>">
													Login
												</a>
											</li>
											<?php
												endif ;
											?>
										</ul>
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</nav>
			</section>
			
			<section id="idsection_maingreetings_1">
				<p>
					Hello, 
					
					<?php
						$a_user_details = $this->session->userdata('a_user_details');
						if( isset($a_user_details['s_u_username']) && !empty($a_user_details['s_u_username']) ):
							echo $a_user_details['s_u_username'];
						else:
					?>
							Guest
					<?php
						endif;
					?>
				</p>
			</section>
		</header>