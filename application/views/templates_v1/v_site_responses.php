<?php
	$b_show_site_response = false;
	$b_show_site_response_error = false;
	$b_show_site_response_success = false;
	$b_show_site_response_info = false;
	
	if( isset($a_form_notice['a_site_response_error']) && !empty($a_form_notice['a_site_response_error']) 
	)
	{
		$b_show_site_response = true;
		$b_show_site_response_error = true;
	}
	
	if( isset($a_form_notice['a_site_response_success']) && !empty($a_form_notice['a_site_response_success']) 
	)
	{
		$b_show_site_response = true;
		$b_show_site_response_success = true;
	}
	
	if( isset($a_form_notice['a_site_response_info']) && !empty($a_form_notice['a_site_response_info']) 
	)
	{
		$b_show_site_response = true;
		$b_show_site_response_info = true;
	}
?>

<section class="clssection_siteresponse_1" style=" <?php if( isset($b_show_site_response) && !empty($b_show_site_response) && $b_show_site_response == true ) { echo ' display:block; '; } ?> ">
	
	<div id="" class="clsdiv_siteresponse clsdiv_responseerror_1" style=" <?php if( isset($b_show_site_response_error) && !empty($b_show_site_response_error) && $b_show_site_response_error == true ) { echo ' display:block; '; } ?> ">
		<h4>
			Error
		</h4>
		<?php
			if( isset($a_form_notice['a_site_response_error']) && !empty($a_form_notice['a_site_response_error']) ):
		?>
			<ul>
			<?php
				foreach( $a_form_notice['a_site_response_error'] AS $s_site_response_error ):
			?>
				<li>
					<?php
						echo $s_site_response_error;
					?>
				</li>
			<?php
				endforeach;
			?>
			</ul>
		<?php
			endif;
		?>
	</div>
	
	<div id="" class="clsdiv_siteresponse clsdiv_responsesuccess_1" style=" <?php if( isset($b_show_site_response_success) && !empty($b_show_site_response_success) && $b_show_site_response_success == true ) { echo ' display:block; '; } ?> ">
		<h4>
			Success
		</h4>
		<?php
			if( isset($a_form_notice['a_site_response_success']) && !empty($a_form_notice['a_site_response_success']) ):
		?>
			<ul>
			<?php
				foreach( $a_form_notice['a_site_response_success'] AS $s_site_response_success ):
			?>
				<li>
					<?php
						echo $s_site_response_success;
					?>
				</li>
			<?php
				endforeach;
			?>
			</ul>
		<?php
			endif;
		?>
	</div>
	
	<div id="" class="clsdiv_siteresponse clsdiv_responseinfo_1" style=" <?php if( isset($b_show_site_response_info) && !empty($b_show_site_response_info) && $b_show_site_response_info == true ) { echo ' display:block; '; } ?> ">
		<h4>
			Info
		</h4>
		<?php
			if( isset($a_form_notice['a_site_response_info']) && !empty($a_form_notice['a_site_response_info']) ):
		?>
			<ul>
			<?php
				foreach( $a_form_notice['a_site_response_info'] AS $s_site_response_info ):
			?>
				<li>
					<?php
						echo $s_site_response_info;
					?>
				</li>
			<?php
				endforeach;
			?>
			</ul>
		<?php
			endif;
		?>
	</div>
	
</section>