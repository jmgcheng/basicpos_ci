<?php
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show All Delivery Receipt
						
						<?php
							$s_status = '';
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 1 )
							{
								
								$s_status = 'Activated';
							}
							else if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 2 )
							{
								$s_status = 'DeActivated';
							}
							else
							{
								$s_status = $a_assoc_uri['status'];
							}
						?>
						
						- <?php echo $s_status; ?>
						
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 1 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>delivery_receipt/read_all/status/1" class="" >Open</a>]&nbsp;
						<?php
							endif ;
						?>
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 2 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>delivery_receipt/read_all/status/2" class="">Closed</a>]&nbsp;
						<?php
							endif ;
						?>
						
					</h1>
				</header>
				
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
				<table class="clstbl_basicquery_1">
					<?php
						if( isset($a_delivery_receipt_header_result) && !empty($a_delivery_receipt_header_result) ) :
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Sales Invoice ID
						</th>
						<th>
							Prepared By
						</th>
						<th>
							Date Registration
						</th>
						<th>
							Title
						</th>
						<th>
							Comment
						</th>
						<th>
							Status
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
						<?php
							foreach( $a_delivery_receipt_header_result AS $a_delivery_receipt_header_result_row ) :
						?>
					<tr>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['i_drh_id']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['i_drh_sih_id']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['s_u_username']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['s_drh_date_registration']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['s_drh_title']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['s_drh_comment']; ?>
						</td>
						<td>
							<?php echo $a_delivery_receipt_header_result_row['s_drsn_name']; ?>
						</td>
						<td>
							<a href="<?php echo base_url(); ?>delivery_receipt/update_form/delivery_receipt_id/<?php echo $a_delivery_receipt_header_result_row['i_drh_id']; ?>">
								Update
							</a>
						</td>
					</tr>
						<?php
							endforeach;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</main>

<?php
	endif;
?>		
		