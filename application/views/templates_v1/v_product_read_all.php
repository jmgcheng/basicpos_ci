<?php
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show All Products
						
						<?php
							$s_status = '';
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 1 )
							{
								
								$s_status = 'Activated';
							}
							else if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 2 )
							{
								$s_status = 'DeActivated';
							}
							else
							{
								$s_status = $a_assoc_uri['status'];
							}
						?>
						
						- <?php echo $s_status; ?>
						
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 1 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>product/read_all/status/1" class="" >Activated</a>]&nbsp;
						<?php
							endif ;
						?>
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 2 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>product/read_all/status/2" class="">DeActivated</a>]&nbsp;
						<?php
							endif ;
						?>
						
					</h1>
				</header>
				
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
				<table class="clstbl_basicquery_1">
					
					<?php
						if( isset($a_product_result) && !empty($a_product_result) ) :
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Name
						</th>
						<th>
							Model
						</th>
						<th>
							Dimension
						</th>
						<th>
							Color
						</th>
						<th>
							Unit
						</th>
						<th>
							Description
						</th>
						<th>
							QTY Low Alert
						</th>
						<th>
							Product Status
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
						<?php
							foreach( $a_product_result AS $a_product_result_row ) :
						?>
					<tr>
						<td>
							<?php echo $a_product_result_row['i_p_id']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_p_name']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_p_model']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_pd_name']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_pc_name']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_pu_name']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_p_description']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['i_p_qty_low_alert']; ?>
						</td>
						<td>
							<?php echo $a_product_result_row['s_psn_name']; ?>
						</td>
						<td>
							<a href="<?php echo base_url(); ?>product/update_form/product_id/<?php echo $a_product_result_row['i_p_id']; ?>">
								Update
							</a>
						</td>
					</tr>
						<?php
							endforeach;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif ;
					?>
					
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</main>
<?php
	endif ;
?>		