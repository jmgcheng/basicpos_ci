		<main>
			<section class="clssection_basicupdate_1">
				<header>
					<h1>
						Purchase Order - Update
					</h1>
				</header>
				
				<form i_purchase_order_id="<?php if( isset($a_purchase_order_header_row_result['i_poh_id']) && !empty($a_purchase_order_header_row_result['i_poh_id']) ) { echo $a_purchase_order_header_row_result['i_poh_id']; } ?>" id="frm_purchase_order_update" name="frm_purchase_order_update" action="" method="post">
					
					<table class="clstbl_basicupdateheader_1">
						<tr>
							<td colspan="2" class="td_siteresponse_cls">
							</td>
						</tr>
						<tr>
							<th colspan="2">
								<input type="button" id="btn_purchase_order_submit" name="btn_purchase_order_submit" value="Update" />
							</th>
						</tr>
						<tr>
							<td>
								Inventory Depot
							</td>
							<td>
								<?php
									if( isset($a_inventory_depot_result) && !empty($a_inventory_depot_result) ) :
								?>
								<select id="opt_purchase_order_receiving_depot" name="opt_purchase_order_receiving_depot">
									<?php
										foreach( $a_inventory_depot_result AS $a_inventory_depot_result_row ) :
									?>
										<option <?php if( isset($a_purchase_order_header_row_result['i_poh_id_id']) && !empty($a_purchase_order_header_row_result['i_poh_id_id']) && ( $a_purchase_order_header_row_result['i_poh_id_id'] == $a_inventory_depot_result_row['i_id_id'] ) ) { echo ' selected="selected" '; } ?> value="<?php echo $a_inventory_depot_result_row['i_id_id']; ?>">
											<?php echo $a_inventory_depot_result_row['s_id_name']; ?>
										</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php
									endif ;
								?>
							</td>
						</tr>
						<tr>
							<td>
								Title
							</td>
							<td>
								<input type="text" id="txt_purchase_order_title" name="txt_purchase_order_title" value="<?php if( isset($a_purchase_order_header_row_result['s_poh_title']) && !empty($a_purchase_order_header_row_result['s_poh_title']) ) { echo $a_purchase_order_header_row_result['s_poh_title']; } ?>" />
							</td>
						</tr>
						<tr>
							<td>
								Order Description
							</td>
							<td>
								<textarea id="txt_purchase_order_description" name="txt_purchase_order_description"><?php if( isset($a_purchase_order_header_row_result['s_poh_order_description']) && !empty($a_purchase_order_header_row_result['s_poh_order_description']) ) { echo $a_purchase_order_header_row_result['s_poh_order_description']; } ?></textarea>
							</td>
						</tr>
						<tr>
							<td>
								Comment
							</td>
							<td>
								<textarea id="txt_purchase_order_comment" name="txt_purchase_order_comment"><?php if( isset($a_purchase_order_header_row_result['s_poh_comment']) && !empty($a_purchase_order_header_row_result['s_poh_comment']) ) { echo $a_purchase_order_header_row_result['s_poh_comment']; } ?></textarea>
							</td>
						</tr>
					</table>
					
					              
					<table class="clstbl_basicupdateadddetail_1">
						<tr>
							<td>
								Product Detail
							</td>
							<td>
								<select id="opt_purchase_order_product_select" name="opt_purchase_order_product_select">
									<option value="">
										Yalex Red Label: Teens - - - 10 - White - pieces
									</option>
								</select>
							</td>
						</tr>
						<tr>	
							<td>
								QTY Ordered
							</td>
							<td>
								<input type="text" id="txt_purchase_order_product_select_quantity" name="txt_purchase_order_product_select_quantity" value="" />
							</td>
						</tr>
						<tr>	
							<td>
								&nbsp;
							</td>
							<td>
								<input type="button" id="btn_purchase_order_product_select_add" name="btn_purchase_order_product_select_add" value="Add Detail" />
							</td>
						</tr>
						
					</table>
					
					
					<table class="clstbl_basicupdatedetail_1">
						<tbody>
							<tr class="clstr_basicupdatedetailheader_1">
								<th>
									Product ID
								</th>
								<th>
									Product Name
								</th>
								<th>
									Product Model
								</th>
								<th>
									Product Dimension
								</th>
								<th>
									Product Color
								</th>
								<th>
									Product Unit
								</th>
								<th>
									QTY Ordered
								</th>
								<th>
									&nbsp;
								</th>
							</tr>
						</tbody>
						<tbody class="tbody_purchase_order_details_cls">
						</tbody>
					</table>
				</form>
				
				<div id="div_purchase_order_form_asset_id" style="display:none;">
					<textarea id="txt_purchase_order_details_template_json_id"><?php if( isset($a_purchase_order_detail_row_result) && !empty($a_purchase_order_detail_row_result) ) { echo json_encode($a_purchase_order_detail_row_result); } ?></textarea>
					<table style="">
						<tbody id="tbody_purchase_order_details_template_id">
							<tr>
								<td>
									<input i_p_id="" type="text" id="" name="txt_purchase_order_detail_p_id[]" value="" readonly style="width:50px;" />
								</td>
								<td>
									<input s_p_name="" type="text" id="" name="txt_purchase_order_detail_p_name[]" value="" readonly />
								</td>
								<td>
									<input s_p_model="" type="text" id="" name="txt_purchase_order_detail_p_model[]" value="" readonly />
								</td>
								<td>
									<input s_pd_name="" type="text" id="" name="txt_purchase_order_detail_pd_name[]" value="" readonly />
								</td>
								<td>
									<input s_pc_name="" type="text" id="" name="txt_purchase_order_detail_pc_name[]" value="" readonly />
								</td>
								<td>
									<input s_pu_name="" type="text" id="" name="txt_purchase_order_detail_pu_name[]" value="" readonly />
								</td>
								<td>
									<input i_order_quantity="" type="text" id="" name="txt_purchase_order_detail_p_quantity_order[]" value="" readonly style="width:50px;" />
								</td>
								<td>
									<input i_remove_p_id="" class="btn_purchase_order_remove_order_detail_cls" type="button" value="Remove">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				
			
			</section>
		</main>