		<main>
			<section class="clssection_form_1">
				<header>
					<h1>
						User Registration
					</h1>
				</header>
				<form id="frm_user_registration" name="frm_user_registration" action="<?php echo base_url() . 'user/register'; ?>" method="post">
					<table id="idtbl_registerform_1">
						<tr>
							<td>
								<label for="txt_user_registration_username">Username:</label>
							</td>
							<td>
								<input type="text" id="txt_user_registration_username" name="txt_user_registration_username" placeholder="Username" value="<?php echo set_value('txt_user_registration_username'); ?>" />
								<?php if( isset($a_form_notice['s_txt_user_registration_username_error']) && !empty($a_form_notice['s_txt_user_registration_username_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_registration_username_error']; ?></p>
								<?php endif; ?>
							</td>
							
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_email">Email:</label>
							</td>
							<td>
								<input type="text" id="txt_user_registration_email" name="txt_user_registration_email" placeholder="youremail@domain.com" value="<?php echo set_value('txt_user_registration_email'); ?>" />
								<?php if( isset($a_form_notice['s_txt_user_registration_email_error']) && !empty($a_form_notice['s_txt_user_registration_email_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_registration_email_error']; ?></p>
								<?php endif; ?>								
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_password">Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_registration_password" name="txt_user_registration_password" />
								<?php if( isset($a_form_notice['s_txt_user_registration_password_error']) && !empty($a_form_notice['s_txt_user_registration_password_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_registration_password_error']; ?></p>
								<?php endif; ?>								
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_user_registration_password_conf">Retype Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_registration_password_conf" name="txt_user_registration_password_conf" />
								<?php if( isset($a_form_notice['s_txt_user_registration_password_conf_error']) && !empty($a_form_notice['s_txt_user_registration_password_conf_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_registration_password_conf_error']; ?></p>
								<?php endif; ?>								
							</td>
						</tr>
						
						<tr>
							<td>
								<label for="txt_user_registration_captcha">Captcha Code</label>
								<br/>
								<a href="#" id="anc_user_registration_update_captcha_id">
									Reload?
								</a>
							</td>
							<td>
								<div id="div_user_registration_captcha_image_id">
									<?php 
										if( isset($s_user_registration_form_captcha) && !empty($s_user_registration_form_captcha) )
										{
											echo $s_user_registration_form_captcha;
										}
									?>
								</div>
								<div>
									<input type="text" id="txt_user_registration_captcha" name="txt_user_registration_captcha" />
								</div>
								<?php if( isset($a_form_notice['s_txt_user_registration_captcha_error']) && !empty($a_form_notice['s_txt_user_registration_captcha_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_registration_captcha_error']; ?></p>
								<?php endif; ?>								
							</td>
						</tr>
						
						
						
						<tr>
							<td colspan="2">
								<input type="submit" value="Register">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="">
									Forgot Password
								</a>
							</td>
						</tr>
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						
					</table>
				</form>
			</section>
		</main>
