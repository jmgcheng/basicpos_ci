
		<main>
			<section class="clssection_form_1">
				<header>
					<h1>
						Login Form
					</h1>
				</header>
				<form id="frm_user_login" name="frm_user_login" action="<?php echo base_url() . 'user/login'; ?>" method="post">
					<table id="idtbl_loginform_1">
						<tr>
							<td>
								<label class="control-label" for="txt_user_login_emailorusername">Email or Username:</label>
							</td>
							<td>
								<input type="text" id="txt_user_login_emailorusername" name="txt_user_login_emailorusername" value="<?php echo set_value('txt_user_login_emailorusername'); ?>" />
								<?php if( isset($a_form_notice['s_txt_user_login_emailorusername_error']) && !empty($a_form_notice['s_txt_user_login_emailorusername_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_login_emailorusername_error']; ?></p>
								<?php endif; ?>	
							</td>
						</tr>
						<tr>
							<td>
								<label class="control-label" for="txt_user_login_password">Password:</label>
							</td>
							<td>
								<input type="password" id="txt_user_login_password" name="txt_user_login_password" />
								<?php if( isset($a_form_notice['s_txt_user_login_password_error']) && !empty($a_form_notice['s_txt_user_login_password_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_user_login_password_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="submit" value="Login">
							</td>
						</tr>
						
						<!-- 
						<tr>
							<td colspan="2">
								<input type="checkbox" name="nmecheckbox_" value=""> Remember Me?
							</td>
						</tr>
						-->
						
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
						
					</table>
				</form>
			</section>
		</main>
		
	