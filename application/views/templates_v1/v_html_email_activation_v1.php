<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</title>
		<style type="text/css"></style>
	</head>
	<body>
		<h1>
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</h1>
		<p>
			Good Day, 
			
			<br/><br/>
			
			Please CLICK the activation link below to activate your account.
			
			<br/><br/>
			
			<strong>Activation Link:</strong>
			<?php if( isset($s_email_user_activation_link) && !empty($s_email_user_activation_link) ) : ?>
				<a href="<?php echo base_url() . 'user/register_activation/' . $s_email_user_activation_link; ?>"><?php echo base_url() . 'user/register_activation/' . $s_email_user_activation_link; ?></a>
			<?php endif; ?>
			
			<br/><br/><br/>
			
			Many Thanks,
			
			<br/>
			
			<?php
				if( isset($s_email_subject) && !empty($s_email_subject) )
				{
					echo $s_email_subject;
				}
			?>
		</p>
		
	</body>
</html>