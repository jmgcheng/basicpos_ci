
		<main>
			<section class="clssection_basiccreate_1">
				<header>
					<h1>
						Products - Create
					</h1>
				</header>
				<form id="frm_product_create" name="frm_product_create" action="<?php echo base_url() . 'product/create'; ?>" method="post">
				
					<table class="clstbl_basiccreateheader_1">
						<tr>
							<th colspan="2">
								<input id="btn_submit" name="btn_submit" type="submit" value="Create">
							</th>
						</tr>
						<tr>
							<td>
								<label for="txt_product_create_name">Name:</label>
							</td>
							<td>
								<input type="text" id="txt_product_create_name" name="txt_product_create_name" placeholder="Product Name" value="<?php echo set_value('txt_product_create_name'); ?>" />
								<?php if( isset($a_form_notice['s_txt_product_create_name_error']) && !empty($a_form_notice['s_txt_product_create_name_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_create_name_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="txt_product_create_model">Model:</label>
							</td>
							<td>
								<input type="text" id="txt_product_create_model" name="txt_product_create_model" placeholder="Product Model" value="<?php echo set_value('txt_product_create_model'); ?>" />
								<?php if( isset($a_form_notice['s_txt_product_create_model_error']) && !empty($a_form_notice['s_txt_product_create_model_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_create_model_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php 
							if( isset($a_product_dimensions_result) && !empty($a_product_dimensions_result) ) : 
						?>
						<tr>
							<td>
								Dimension
							</td>
							<td>
								<select name="opt_product_create_dimension">
									<?php
										foreach( $a_product_dimensions_result AS $a_product_dimensions_result_row ) :
									?>
									<option value="<?php echo $a_product_dimensions_result_row['i_pd_id']; ?>">
										<?php echo $a_product_dimensions_result_row['s_pd_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_create_dimension_error']) && !empty($a_form_notice['s_opt_product_create_dimension_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_create_dimension_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						<?php 
							if( isset($a_product_colors_result) && !empty($a_product_colors_result) ) : 
						?>
						<tr>
							<td>
								Color
							</td>
							<td>
								<select name="opt_product_create_color">
									<?php
										foreach( $a_product_colors_result AS $a_product_colors_result_row ) :
									?>
									<option value="<?php echo $a_product_colors_result_row['i_pc_id']; ?>">
										<?php echo $a_product_colors_result_row['s_pc_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_create_color_error']) && !empty($a_form_notice['s_opt_product_create_color_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_create_color_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						<?php 
							if( isset($a_product_units_result) && !empty($a_product_units_result) ) : 
						?>
						<tr>
							<td>
								Unit
							</td>
							<td>
								<select name="opt_product_create_unit">
									<?php
										foreach( $a_product_units_result AS $a_product_units_result_row ) :
									?>
									<option value="<?php echo $a_product_units_result_row['i_pu_id']; ?>">
										<?php echo $a_product_units_result_row['s_pu_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_create_unit_error']) && !empty($a_form_notice['s_opt_product_create_unit_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_create_unit_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						<tr>
							<td>
								<label for="txt_product_create_description">Description:</label>
							</td>
							<td>
								<textarea id="txt_product_create_description" name="txt_product_create_description"><?php echo set_value('txt_product_create_description'); ?></textarea>
								<?php if( isset($a_form_notice['s_txt_product_create_description_error']) && !empty($a_form_notice['s_txt_product_create_description_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_txt_product_create_description_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php 
							if( isset($a_product_status_names_result) && !empty($a_product_status_names_result) ) : 
						?>
						<tr>
							<td>
								Product Status
							</td>
							<td>
								<select name="opt_product_create_status">
									<?php
										foreach( $a_product_status_names_result AS $a_product_status_names_result_row ) :
									?>
									<option value="<?php echo $a_product_status_names_result_row['i_psn_id']; ?>">
										<?php echo $a_product_status_names_result_row['s_psn_name']; ?>
									</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php if( isset($a_form_notice['s_opt_product_create_status_error']) && !empty($a_form_notice['s_opt_product_create_status_error']) ) : ?>
									<p class="clsp_texterror_1"><?php echo $a_form_notice['s_opt_product_create_status_error']; ?></p>
								<?php endif; ?>
							</td>
						</tr>
						<?php
							endif ;
						?>
						<?php
							if( isset($s_view_site_responses) && !empty($s_view_site_responses) ):
						?>
						<tr>
							<td colspan="2">
								<?php
									echo $s_view_site_responses;
								?>
							</td>
						</tr>
						<?php
							endif;
						?>
						
					</table>
					
				</form>
			
			</section>
		</main>
		