		<main>
			<section class="clssection_basiccreate_1">
				<header>
					<h1>
						Include Inventory - Create
					</h1>
				</header>
				
				<form id="frm_included_inventory_create" name="frm_included_inventory_create" action="" method="post">
					
					<table class="clstbl_basiccreateheader_1">
						<tr>
							<td colspan="2" class="td_siteresponse_cls">
							</td>
						</tr>
						<tr>
							<th colspan="2">
								<input type="button" id="btn_included_inventory_submit" name="btn_included_inventory_submit" value="Create" />
							</th>
						</tr>
						<tr>
							<td>
								Inventory Depot
							</td>
							<td>
								<?php
									if( isset($a_inventory_depot_result) && !empty($a_inventory_depot_result) ) :
								?>
								<select id="opt_included_inventory_receiving_depot" name="opt_included_inventory_receiving_depot">
									<?php
										foreach( $a_inventory_depot_result AS $a_inventory_depot_result_row ) :
									?>
										<option value="<?php echo $a_inventory_depot_result_row['i_id_id']; ?>">
											<?php echo $a_inventory_depot_result_row['s_id_name']; ?>
										</option>
									<?php
										endforeach ;
									?>
								</select>
								<?php
									endif ;
								?>
							</td>
						</tr>
						<tr>
							<td>
								Comment
							</td>
							<td>
								<textarea id="txt_included_inventory_comment" name="txt_included_inventory_comment"></textarea>
							</td>
						</tr>
					</table>
					
					
					<table class="clstbl_basiccreateadddetail_1">
						<tr>
							<td>
								Product Detail
							</td>
							<td>
								<select id="opt_included_inventory_product_select" name="opt_included_inventory_product_select">
									<option value="">
										Yalex Red Label: Teens - - - 10 - White - pieces
									</option>
								</select>
							</td>
						</tr>
						<tr>	
							<td>
								Product Serial (for Product with Seria No. only)
							</td>
							<td>
								<input type="text" id="txt_included_inventory_product_select_serial_no" name="txt_included_inventory_product_select_serial_no" value="" />
							</td>
						</tr>
						<tr>	
							<td>
								QTY
							</td>
							<td>
								<input type="text" id="txt_included_inventory_product_select_quantity" name="txt_included_inventory_product_select_quantity" value="" />
							</td>
						</tr>
						<tr>	
							<td>
								&nbsp;
							</td>
							<td>
								<input type="button" id="btn_included_inventory_product_select_add" name="btn_included_inventory_product_select_add" value="Add Detail" />
							</td>
						</tr>
						
					</table>
					
					
					<table class="clstbl_basiccreatedetail_1">
						<tbody>
							<tr class="clstr_basiccreatedetailheader_1">
								<th>
									Product ID
								</th>
								<th>
									Product Serial No.
								</th>
								<th>
									Product Name
								</th>
								<th>
									Product Model
								</th>
								<th>
									Product Dimension
								</th>
								<th>
									Product Color
								</th>
								<th>
									Product Unit
								</th>
								<th>
									QTY
								</th>
								<th>
									&nbsp;
								</th>
							</tr>
						</tbody>
						<tbody class="tbody_included_inventory_details_cls">
						</tbody>
					</table>
				</form>
				
				<div id="div_included_inventory_form_asset_id" style="display:none;">
					<table style="">
						<tbody id="tbody_included_inventory_details_template_id">
							<tr>
								<td>
									<input i_p_id="" type="text" id="" name="txt_included_inventory_detail_p_id[]" value="" readonly style="width:50px;" />
								</td>
								<td>
									<input s_p_serial="" type="text" id="" name="txt_included_inventory_detail_p_serial[]" value="" readonly style="width:50px;" />
								</td>
								<td>
									<input s_p_name="" type="text" id="" name="txt_included_inventory_detail_p_name[]" value="" readonly />
								</td>
								<td>
									<input s_p_model="" type="text" id="" name="txt_included_inventory_detail_p_model[]" value="" readonly />
								</td>
								<td>
									<input s_pd_name="" type="text" id="" name="txt_included_inventory_detail_pd_name[]" value="" readonly />
								</td>
								<td>
									<input s_pc_name="" type="text" id="" name="txt_included_inventory_detail_pc_name[]" value="" readonly />
								</td>
								<td>
									<input s_pu_name="" type="text" id="" name="txt_included_inventory_detail_pu_name[]" value="" readonly />
								</td>
								<td>
									<input i_order_quantity="" type="text" id="" name="txt_included_inventory_detail_p_quantity_order[]" value="" readonly style="width:50px;" />
								</td>
								<td>
									<input i_remove_p_id="" s_remove_p_serial="" class="btn_included_inventory_remove_order_detail_cls" type="button" value="Remove">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				
			
			</section>
		</main>