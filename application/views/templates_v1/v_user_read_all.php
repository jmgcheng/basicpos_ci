<?php
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show All Users 
						
						<?php
							$s_status = '';
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 1 )
							{
								
								$s_status = 'Activated';
							}
							else if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 2 )
							{
								$s_status = 'DeActivated';
							}
							else if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 3 )
							{
								$s_status = 'Activated Required';
							}
							else
							{
								$s_status = $a_assoc_uri['status'];
							}
						?>
						
						- <?php echo $s_status; ?>
						
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 1 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>user/read_all/status/1" class="" >Activated</a>]&nbsp;
						<?php
							endif ;
						?>
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 2 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>user/read_all/status/2" class="">DeActivated</a>]&nbsp;
						<?php
							endif ;
						?>
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 3 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>user/read_all/status/3" class="">Activated Required</a>]&nbsp;
						<?php
							endif ;
						?>
						
						
					</h1>
				</header>
				
				<table class="clstbl_basicquery_1">
					
					<?php
						if( isset($a_user_result) && !empty($a_user_result) ) :
							
							/*
								Segregate User Roles Now
									segregating user roles now to their respective user id rather than later
							*/
							$a_user_roles_segregated = array();
							foreach( $a_user_roles_result AS $a_user_roles_result_row )
							{
								//
								if( !array_key_exists($a_user_roles_result_row['i_ur_user_id'], $a_user_roles_segregated) )
								{
									$a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']] = array();
									$a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles'] = '';
								}
								
								//
								if( isset($a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles']) && !empty($a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles']) )
								{
									$a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles'] = $a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles'] . ', ' . $a_user_roles_result_row['s_urn_name'];
								}
								else
								{
									$a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']]['s_user_roles'] = $a_user_roles_result_row['s_urn_name'];
								}
								
								//
								array_push( $a_user_roles_segregated[$a_user_roles_result_row['i_ur_user_id']], $a_user_roles_result_row );
							}
							//print_r($a_user_roles_segregated);
							
							
							/*
								Segregate Inventory Depot Users
							*/
							$a_inventory_depot_users_segregated = array();
							foreach( $a_inventory_depot_users_result AS $a_inventory_depot_users_result_row )
							{
								//
								if( !array_key_exists($a_inventory_depot_users_result_row['i_idu_u_id'], $a_inventory_depot_users_segregated) )
								{
									$a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']] = array();
									$a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot'] = '';
								}
								
								//
								if( isset($a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot']) && !empty($a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot']) )
								{
									$a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot'] = $a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot'] . ', ' . $a_inventory_depot_users_result_row['s_id_name'];
								}
								else
								{
									$a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']]['s_user_inventory_depot'] = $a_inventory_depot_users_result_row['s_id_name'];
								}
								
								//
								array_push( $a_inventory_depot_users_segregated[$a_inventory_depot_users_result_row['i_idu_u_id']], $a_inventory_depot_users_result_row );
							}
							//print_r($a_inventory_depot_users_segregated);
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Email
						</th>
						<th>
							Username
						</th>
						<th>
							User Roles
						</th>
						<th>
							Assigned Inventory Depot
						</th>
						<th>
							User Status
						</th>
					</tr>
						<?php
							foreach( $a_user_result AS $a_user_result_row ) :
						?>
						
					<tr>
						<td>
							<?php echo $a_user_result_row['i_u_id']; ?>
						</td>
						<td>
							<?php echo $a_user_result_row['s_u_email']; ?>
						</td>
						<td>
							<?php echo $a_user_result_row['s_u_username']; ?>
						</td>
						<td>
							<?php
								if( isset($a_user_roles_segregated) && !empty($a_user_roles_segregated) && array_key_exists($a_user_result_row['i_u_id'], $a_user_roles_segregated) ) :
									echo $a_user_roles_segregated[$a_user_result_row['i_u_id']]['s_user_roles'];
								else :
							?>	
									&nbsp;
							<?php
								endif ;
							?>
						</td>
						<td>
							<?php
								if( isset($a_inventory_depot_users_segregated) && !empty($a_inventory_depot_users_segregated) && array_key_exists($a_user_result_row['i_u_id'], $a_inventory_depot_users_segregated) ) :
									echo $a_inventory_depot_users_segregated[$a_user_result_row['i_u_id']]['s_user_inventory_depot'];
								else :
							?>	
									&nbsp;
							<?php
								endif ;
							?>
						</td>
						<td>
							<?php echo $a_user_result_row['s_usn_name']; ?>
						</td>
					</tr>	
						
						
						<?php
							endforeach ;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif ;
					?>
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</main>
		
<?php
	endif ;
?>