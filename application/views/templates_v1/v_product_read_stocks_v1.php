<?php
	$a_qty_low_products_alerts = array();
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show Stocks Quantity
					</h1>
					<br/>
					<form id="frm_filter_read_stocks" name="frm_filter_read_stocks" action="<?php echo base_url() . 'product/read_stocks/'; ?>" method="post">
						<table style="float:right;">
							<tr>
								<th colspan="2">
									Filter
								</th>
							</tr>
							<tr>
								<td>
									Color
								</td>
								<td>
									<select name="opt_filter_read_stocks_color">
										<option value="0">All</option>
										<?php
											if( isset($a_pc_result) && !empty($a_pc_result) ):
												foreach( $a_pc_result AS $a_pc_result_row ):
										?>
										<option value="<?php echo $a_pc_result_row['i_pc_id']; ?>" <?php if( isset($a_assoc_uri['i_pc_id']) && !empty($a_assoc_uri['i_pc_id']) && $a_assoc_uri['i_pc_id'] == $a_pc_result_row['i_pc_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_pc_result_row['s_pc_name']; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Dimension
								</td>
								<td>
									<select name="opt_filter_read_stocks_dimension">
										<option value="0">All</option>
										<?php
											if( isset($a_pd_result) && !empty($a_pd_result) ):
												foreach( $a_pd_result AS $a_pd_result_row ):
										?>
										<option value="<?php echo $a_pd_result_row['i_pd_id']; ?>" <?php if( isset($a_assoc_uri['i_pd_id']) && !empty($a_assoc_uri['i_pd_id']) && $a_assoc_uri['i_pd_id'] == $a_pd_result_row['i_pd_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_pd_result_row['s_pd_name']; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Depot ID
								</td>
								<td>
									<select name="opt_filter_read_stocks_depot_id">
										<option value="0">All</option>
										<?php
											if( isset($a_id_result) && !empty($a_pd_result) ):
												foreach( $a_id_result AS $a_id_result_row ):
										?>
										<option value="<?php echo $a_id_result_row['i_id_id']; ?>" <?php if( isset($a_assoc_uri['i_id_id']) && !empty($a_assoc_uri['i_id_id']) && $a_assoc_uri['i_id_id'] == $a_id_result_row['i_id_id'] ) { echo ' selected="selected" '; } ?> ><?php echo $a_id_result_row['s_id_name']; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									QA >= 
								</td>
								<td>
									<select name="opt_filter_read_stocks_qa">
										<?php
											if( isset($a_expected_qa) && !empty($a_expected_qa) ):
												foreach( $a_expected_qa AS $s_key => $s_expected_qa_row ):
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['i_qa']) && !empty($a_assoc_uri['i_qa']) && $a_assoc_uri['i_qa'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_qa_row; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Product Status Names
								</td>
								<td>
									<select name="opt_filter_read_stocks_psn">
										<?php
											if( isset($a_expected_psn) && !empty($a_expected_psn) ):
												foreach( $a_expected_psn AS $s_key => $s_expected_psn_row ):
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['i_psn_id']) && !empty($a_assoc_uri['i_psn_id']) && $a_assoc_uri['i_psn_id'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_psn_row; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Order By
								</td>
								<td>
									Sort <select name="opt_filter_read_stocks_sort">
										<?php
											if( isset($a_expected_sort) && !empty($a_expected_sort) ):
												foreach( $a_expected_sort AS $s_key => $s_expected_sort_row ):
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['s_sort']) && !empty($a_assoc_uri['s_sort']) && $a_assoc_uri['s_sort'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_sort_row; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
									<br/>
									Order <select name="opt_filter_read_stocks_order">
										<?php
											if( isset($a_expected_order) && !empty($a_expected_order) ):
												foreach( $a_expected_order AS $s_key => $s_expected_order_row ):
										?>
										<option value="<?php echo $s_key; ?>" <?php if( isset($a_assoc_uri['s_order']) && !empty($a_assoc_uri['s_order']) && $a_assoc_uri['s_order'] == $s_key ) { echo ' selected="selected" '; } ?> ><?php echo $s_expected_order_row; ?></option>
										<?php
												endforeach;
											endif;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Limit
								</td>
								<td>
									<input type="text" name="txt_filter_read_stocks_limit" value="<?php if( isset($a_assoc_uri['i_limit']) && !empty($a_assoc_uri['i_limit'])) { echo $a_assoc_uri['i_limit']; } else { echo '500'; } ?>" />
								</td>
							</tr>
							
							
							<tr>
								<td colspan="2">
									<input type="submit" name="" value="Search" />
								</td>
							</tr>
						</table>
						<div style="clear:both;"></div>
					</form>
					
						
				</header>
				
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
				<table class="clstbl_basicquery_1">
					<?php
						if( isset($a_stock_quantity_result) && !empty($a_stock_quantity_result) ) :
					?>
					<thead style="background-color:white;">
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Name
						</th>
						<th>
							Model
						</th>
						<th>
							Color
						</th>
						<th>
							Dimension
						</th>
						<th>
							II
						</th>
						<th>
							EI
						</th>
						<th>
							OT
						</th>
						<th>
							RF
						</th>
						<th>
							MS
						</th>
						<th>
							WS
						</th>
						<th>
							AF
						</th>
						<th title="Total Released">
							TR
						</th>
						<th title="Total Sold">
							TS
						</th>
						<th title="Sales Return">
							SR
						</th>
						<th title="Quantity Available">
							QA
						</th>
						<th title="Quantity Low Alert">
							QLA
						</th>
						<th title="Need to reach 2 Doz">
							N2D
						</th>
						<th title="Need to reach 3 Doz">
							N3D
						</th>
						<th title="Need to reach 4 Doz">
							N4D
						</th>
						<th title="Need to reach 4 Doz">
							N5D
						</th>
						<th title="Need to reach 4 Doz">
							N6D
						</th>
						<th title="Need to reach 4 Doz">
							N7D
						</th>
					</tr>
					</thead>
						<?php
							foreach( $a_stock_quantity_result AS $a_stock_quantity_result_row ) :
								if( $a_stock_quantity_result_row['i_p_qty_low_alert'] > $a_stock_quantity_result_row['i_quantity_available'] )
								{ 
									array_push($a_qty_low_products_alerts, $a_stock_quantity_result_row ); 
								}
						?>
					<tr>
						<td>
							<a href="<?php echo base_url(); ?>product/update_form/product_id/<?php echo $a_stock_quantity_result_row['i_p_id']; ?>">
								<?php echo $a_stock_quantity_result_row['i_p_id']; ?>
							</a>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_p_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_p_model']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_pc_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['s_pd_name']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_included_inventory_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_excluded_inventory_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_received_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_missed_purchased_order_total']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_wrong_send_purchased_order_total']; ?>
						</td>
						<td title="PO - (RPO + MPO) ">
							<?php 
								echo $a_stock_quantity_result_row['i_af'];
							?>
						</td>
						<td>
							<?php
								echo $a_stock_quantity_result_row['i_tr'] ;
							?>
						</td>
						<td>
							<?php
								echo $a_stock_quantity_result_row['i_official_receipt_total'];
							?>
						</td>
						<td>
							<?php
								echo $a_stock_quantity_result_row['i_sales_return_total'] ;
							?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo $a_stock_quantity_result_row['i_p_qty_low_alert']; ?>
						</td>
						<td>
							<?php echo 24 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 36 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 48 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 60 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 72 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
						<td>
							<?php echo 84 - $a_stock_quantity_result_row['i_quantity_available']; ?>
						</td>
					</tr>
						<?php
							endforeach ;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif ;
					?>
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>

			</section>

			<?php
				if( isset($a_qty_low_products_alerts) && !empty($a_qty_low_products_alerts) ):
			?>
			<section class="products-table products-low-qty-alert">
				<table class="">
					<thead style="">
						<tr>
							<th>
								QTY LOW PRODUCT ALERT
							</th>
							<th>
								<a href="#" class="products-low-qty-alert-show-toggle"> -> Minimize / Maximize <- </a>
							</th>
						</tr>
					</thead>
					<thead style="">
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Name
						</th>
						<th>
							Model
						</th>
						<th>
							Color
						</th>
						<th>
							Dimension
						</th>
						<th>
							II
						</th>
						<th>
							EI
						</th>
						<th>
							OT
						</th>
						<th>
							RF
						</th>
						<th>
							MS
						</th>
						<th>
							WS
						</th>
						<th>
							AF
						</th>
						<th title="Total Released">
							TR
						</th>
						<th title="Total Sold">
							TS
						</th>
						<th title="Sales Return">
							SR
						</th>
						<th title="Quantity Available">
							QA
						</th>
						<th title="Quantity Low Alert">
							QLA
						</th>
						<th title="Need to reach 2 Doz">
							N2D
						</th>
						<th title="Need to reach 3 Doz">
							N3D
						</th>
						<th title="Need to reach 4 Doz">
							N4D
						</th>
						<th title="Need to reach 4 Doz">
							N5D
						</th>
						<th title="Need to reach 4 Doz">
							N6D
						</th>
						<th title="Need to reach 4 Doz">
							N7D
						</th>
					</tr>
					</thead>
					<tbody class="tbody">
							<?php
								foreach( $a_qty_low_products_alerts AS $a_qty_low_products_alerts_row ) :
							?>
						<tr>
							<td>
								<a href="<?php echo base_url(); ?>product/update_form/product_id/<?php echo $a_qty_low_products_alerts_row['i_p_id']; ?>">
									<?php echo $a_qty_low_products_alerts_row['i_p_id']; ?>
								</a>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['s_p_name']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['s_p_model']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['s_pc_name']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['s_pd_name']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_included_inventory_total']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_excluded_inventory_total']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_purchased_order_total']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_received_purchased_order_total']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_missed_purchased_order_total']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_wrong_send_purchased_order_total']; ?>
							</td>
							<td title="PO - (RPO + MPO) ">
								<?php 
									echo $a_qty_low_products_alerts_row['i_af'];
								?>
							</td>
							<td>
								<?php
									echo $a_qty_low_products_alerts_row['i_tr'] ;
								?>
							</td>
							<td>
								<?php
									echo $a_qty_low_products_alerts_row['i_official_receipt_total'];
								?>
							</td>
							<td>
								<?php
									echo $a_qty_low_products_alerts_row['i_sales_return_total'] ;
								?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo $a_qty_low_products_alerts_row['i_p_qty_low_alert']; ?>
							</td>
							<td>
								<?php echo 24 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo 36 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo 48 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo 60 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo 72 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
							<td>
								<?php echo 84 - $a_qty_low_products_alerts_row['i_quantity_available']; ?>
							</td>
						</tr>
							<?php
								endforeach ;
							?>
					</tbody>
				</table>
			</section>
			<?php
				endif;
			?>
			
			
			
			
			
		</main>
		
<?php
	endif;
?>		