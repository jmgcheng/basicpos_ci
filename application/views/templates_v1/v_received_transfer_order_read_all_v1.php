<?php
	$a_user_details = $this->session->userdata('a_user_details');
	if( !empty($a_user_details) ) :
?>
		<main>
			<section class="clssection_basicquery_1">
				<header>
					<h1>
						Show All Received Transfer Order
						
						<?php
							$s_status = '';
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 1 )
							{
								
								$s_status = 'Activated';
							}
							else if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] == 2 )
							{
								$s_status = 'DeActivated';
							}
							else
							{
								$s_status = $a_assoc_uri['status'];
							}
						?>
						
						- <?php echo $s_status; ?>
						
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 1 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>received_transfer_order/read_all/status/1" class="" >Open</a>]&nbsp;
						<?php
							endif ;
						?>
						<?php
							if( isset($a_assoc_uri['status']) && !empty($a_assoc_uri['status']) && $a_assoc_uri['status'] != 2 ) :
						?>
							&nbsp;[<a href="<?php echo base_url(); ?>received_transfer_order/read_all/status/2" class="">Closed</a>]&nbsp;
						<?php
							endif ;
						?>
						
					</h1>
				</header>
				
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
				<table class="clstbl_basicquery_1">
					<?php
						if( isset($a_received_to_header_result) && !empty($a_received_to_header_result) ) :
					?>
					<tr>
						<th>
							DB ID
						</th>
						<th>
							Receiving Depot
						</th>
						<th>
							Prepared By
						</th>
						<th>
							Date Registration
						</th>
						<th>
							Comment
						</th>
						<th>
							Status
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
						<?php
							foreach( $a_received_to_header_result AS $a_received_to_header_result_row ) :
						?>
					<tr>
						<td>
							<?php echo $a_received_to_header_result_row['i_rtoh_id']; ?>
						</td>
						<td>
							<?php echo $a_received_to_header_result_row['s_id_name']; ?>
						</td>
						<td>
							<?php echo $a_received_to_header_result_row['s_u_username']; ?>
						</td>
						<td>
							<?php echo $a_received_to_header_result_row['s_rtoh_date_registration']; ?>
						</td>
						<td>
							<?php echo $a_received_to_header_result_row['s_rtoh_comment']; ?>
						</td>
						<td>
							<?php echo $a_received_to_header_result_row['s_rtosn_name']; ?>
						</td>
						<td>
							<a href="<?php echo base_url(); ?>received_transfer_order/update_form/received_transfer_order_id/<?php echo $a_received_to_header_result_row['i_rtoh_id']; ?>">
								Update
							</a>
						</td>
					</tr>
						<?php
							endforeach;
						?>
					<?php
						else :
					?>
					<tr>
						<td>
							No Data Yet..
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
				<nav class="clsnav_basicquerypaging_1">
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</main>

<?php
	endif;
?>		
		