﻿$(document).ready(function($){

	o_navigator_useragent = {
		name: '',
		
		guess: function () {
			var s_navigator_useragent = navigator.userAgent.toLowerCase();
			if(s_navigator_useragent.indexOf("msie") != -1) 
			{
				o_navigator_useragent.name = 'msie';
			}
			else if(s_navigator_useragent.indexOf("chrome") != -1)
			{
				o_navigator_useragent.name = 'chrome';
			}
			else if(s_navigator_useragent.indexOf("firefox") != -1) 
			{
				o_navigator_useragent.name = 'firefox';
			}
		},
		
		add_document_body_class: function () {
			try
			{
				$("body").addClass( o_navigator_useragent.name );
			}
			catch(e)
			{}
		}
	};
	
	o_navigator_useragent.guess();
	o_navigator_useragent.add_document_body_class();
});