$(document).ready(function($){
	var o_register_form = {
		o_frm_user_registration : $('#frm_user_registration'),
		o_txt_registration_username : $('#txt_registration_username'),
		o_txt_registration_email : $('#txt_registration_email'),
		o_txt_registration_password : $('#txt_registration_password'),
		o_txt_registration_password_conf : $('#txt_registration_password_conf'),
		o_div_user_registration_captcha_image_id : $('#div_user_registration_captcha_image_id'),
		o_txt_registration_captcha : $('#txt_registration_captcha'),
		o_anc_user_registration_update_captcha_id : $('#anc_user_registration_update_captcha_id'),
		
		change_captcha_image : function(){
			s_params = '';
			$.ajax({
				type: 'post',
				data: s_params,
				dataType: 'json',
				url: 'user/update_captcha_register_form_ajx/',
				beforeSend: function(){ },
				success: function(s_result){
					if(s_result.s_act_result == 'success')
					{
						o_register_form.o_div_user_registration_captcha_image_id.html( s_result.s_captcha_img );
					}
					else
					{
						o_register_form.o_div_user_registration_captcha_image_id.html( '<p>Fail to reload captcha.</p>' );
					}
				}
			});
		},
		
	};
	
	
	o_register_form.o_anc_user_registration_update_captcha_id.on('click', function() {
		o_register_form.change_captcha_image();
		return false;
	});

});