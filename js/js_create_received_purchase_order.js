$(document).ready(function($){
	
	var o_form = {
		o_products : new Object,
		a_products : new Array,
		o_order : new Object,
		o_order_header : new Object,
		o_order_detail : new Object,
		a_order_detail : new Array,
		o_frm_received_purchase_order_create : $('#frm_received_purchase_order_create'),
		o_btn_received_purchase_order_submit : $('#btn_received_purchase_order_submit'),
		o_opt_received_purchase_order_receiving_depot : $('#opt_received_purchase_order_receiving_depot'),
		o_txt_received_purchase_order_poh_id : $('#txt_received_purchase_order_poh_id'),
		o_txt_received_purchase_order_comment : $('#txt_received_purchase_order_comment'),
		o_opt_received_purchase_order_product_select : $('#opt_received_purchase_order_product_select'),
		o_txt_received_purchase_order_product_select_quantity : $('#txt_received_purchase_order_product_select_quantity'),
		o_txt_received_purchase_order_product_select_serial_no : $('#txt_received_purchase_order_product_select_serial_no'),
		o_btn_received_purchase_order_product_select_add : $('#btn_received_purchase_order_product_select_add'),
		o_tbody_received_purchase_order_details_cls : $('.tbody_received_purchase_order_details_cls'),
		o_div_received_purchase_order_form_asset_id : $('#div_received_purchase_order_form_asset_id'),
		o_td_siteresponse_cls : $('.td_siteresponse_cls'),
		
		o_clstbl_basiccreatedetail_1 : $('.clstbl_basiccreatedetail_1'),
		
		
		init : function(){
			o_form.set_products();
			
			o_form.purge_select_products();
			
			o_form.purge_order_details();
			
			o_form.load_order_detail_ux();
		}, 
		
		
		set_products : function(){
			$.ajax({
				url: 'product/read_all_ajx',
				dataType: "json",
				type: "POST",
				data: '',
				beforeSend: function(){
				},
				success: function( o_result ) {

					if( o_result.s_result == 'success' )
					{
						o_form.o_products = o_result.a_product_result;
						for( o_counter in o_form.o_products)
						{ o_form.a_products[o_form.o_products[o_counter].i_p_id] = o_form.o_products[o_counter]; }
						
						o_form.populate_select_products();
					}
					else
					{
						
					}
					
				}
			});
		},
		
		
		purge_select_products : function(){
			o_form.o_opt_received_purchase_order_product_select.find('option').remove();
		},
		
		
		purge_order_details : function(){
			o_form.o_tbody_received_purchase_order_details_cls.html('');
		},
		
		
		populate_select_products : function(){
		
			/**/
			o_form.purge_select_products();
			
			/**/
			for( var i_counter = 0; i_counter < o_form.o_products.length; i_counter++ )
			{
				/*
					console.log( o_form.o_products[i_counter] );
				*/
				
				//
				o_form.o_opt_received_purchase_order_product_select.append('<option value="'+o_form.o_products[i_counter].i_p_id+'">' + o_form.o_products[i_counter].s_p_name + ' - ' + o_form.o_products[i_counter].s_p_model + ' - ' + o_form.o_products[i_counter].s_pd_name + ' - ' + o_form.o_products[i_counter].s_pc_name + ' - ' + o_form.o_products[i_counter].s_pu_name + '</option>');
			}
		},
		
		
		add_order_detail : function(){
			$b_stop_exec = false;
			$b_found_product = false;
			
			
			/*
				Check if a Product Detail is selected
			*/
			if( $b_stop_exec == false )
			{
				if( o_form.o_opt_received_purchase_order_product_select.val() == '' )
				{ 
					$b_stop_exec = true;
					alert('Select Product Detail'); 
				}
			}
			
			
			/*
				Check if quantity order was entered
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_txt_received_purchase_order_product_select_quantity.val(o_form.o_txt_received_purchase_order_product_select_quantity.val().replace(/^0+/, ''));
				if( o_form.o_txt_received_purchase_order_product_select_quantity.val() == '' || o_form.o_txt_received_purchase_order_product_select_quantity.val() <= 0 )
				{ 
					$b_stop_exec = true;
					alert('Enter Quantity'); 
				}
			}
			
			
			/*
				Check if has serial, quantity should only be 1
			*/
			if( $b_stop_exec == false )
			{
				if( o_form.o_txt_received_purchase_order_product_select_serial_no.val() != '' && o_form.o_txt_received_purchase_order_product_select_quantity.val() > 1 )
				{ 
					$b_stop_exec = true;
					alert('Enter only 1 quantity for Products with Serial No.'); 
				}
			}
			
			
			
			/*
			*/
			if( $b_stop_exec == false )
			{
				try
				{
					
					/*
					i_p_id s_p_serial s_p_name s_pd_name s_pc_name s_pu_name i_order_quantity
					o_form.o_order_detail[o_form.o_opt_received_purchase_order_product_select.val()] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()];
					o_form.o_order_detail[o_form.o_opt_received_purchase_order_product_select.val()].s_p_serial = o_form.o_txt_received_purchase_order_product_select_serial_no.val();
					o_form.o_order_detail[o_form.o_opt_received_purchase_order_product_select.val()].i_order_quantity = o_form.o_txt_received_purchase_order_product_select_quantity.val();
					*/
					
					var a_temp = new Array();
					a_temp['i_p_id'] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()].i_p_id;
					a_temp['s_p_serial'] = o_form.o_txt_received_purchase_order_product_select_serial_no.val();
					a_temp['s_p_name'] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()].s_p_name;
					a_temp['s_pd_name'] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()].s_pd_name;
					a_temp['s_pc_name'] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()].s_pc_name;
					a_temp['s_pu_name'] = o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()].s_pu_name;
					a_temp['i_order_quantity'] = o_form.o_txt_received_purchase_order_product_select_quantity.val();
					//console.log(a_temp);
					
					o_form.o_order_detail[o_form.o_opt_received_purchase_order_product_select.val() +"_"+ o_form.o_txt_received_purchase_order_product_select_serial_no.val()] = a_temp;
					//console.log(o_form.o_order_detail);
					
				}
				catch(e)
				{
					if (o_form.a_products[o_form.o_opt_received_purchase_order_product_select.val()]) 
					{}
					else
					{
						$b_stop_exec = true;
						alert('Product NOT Found'); 
					}
				}
			}
			
			
			/*
			*/
			o_form.load_order_detail_ux();
			
		},
		
		
		load_order_detail_ux : function(){
			
			/**/
			o_form.purge_order_details();
			
			/**/
			var i_counter = 0;
			for( o_counter in o_form.o_order_detail)
			{
				/*
					ready asset
				*/
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_p_id]').attr('value', o_form.o_order_detail[o_counter].i_p_id);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_p_id]').attr('i_p_id', o_form.o_order_detail[o_counter].i_p_id);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_serial]').attr('value', o_form.o_order_detail[o_counter].s_p_serial);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_serial]').attr('s_p_serial', o_form.o_order_detail[o_counter].s_p_serial);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_name]').attr('value', o_form.o_order_detail[o_counter].s_p_name);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_model]').attr('value', o_form.o_order_detail[o_counter].s_p_model);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pd_name]').attr('value', o_form.o_order_detail[o_counter].s_pd_name);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pc_name]').attr('value', o_form.o_order_detail[o_counter].s_pc_name);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pu_name]').attr('value', o_form.o_order_detail[o_counter].s_pu_name);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_order_quantity]').attr('value', o_form.o_order_detail[o_counter].i_order_quantity);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', o_form.o_order_detail[o_counter].i_p_id);
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_remove_p_serial]').attr('s_remove_p_serial', o_form.o_order_detail[o_counter].s_p_serial);
				
				/*
					append asset
				*/
				o_form.o_tbody_received_purchase_order_details_cls.append( o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id').html() );

				/*
					clean asset
				*/
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_p_id]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_serial]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_name]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_p_model]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pd_name]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pc_name]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[s_pu_name]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_order_quantity]').attr('value', '');
				o_form.o_div_received_purchase_order_form_asset_id.find('tbody#tbody_received_purchase_order_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', '');
				
				i_counter++;
			}
			
			/**/
			if( i_counter == 0 )
			{
				o_form.o_clstbl_basiccreatedetail_1.hide();
			}
			else
			{
				o_form.o_clstbl_basiccreatedetail_1.show();
			}
			
		},
		
		
		create_order : function(){
			$b_stop_exec = false;
			
			
			/*
				check header is ok
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_order_header.i_receiving_id_id = o_form.o_opt_received_purchase_order_receiving_depot.val();
				o_form.o_order_header.i_poh_id = o_form.o_txt_received_purchase_order_poh_id.val();
				o_form.o_order_header.s_comment = o_form.o_txt_received_purchase_order_comment.val();
				
				if( o_form.o_order_header.i_receiving_id_id == '' )
				{
					alert('Select Receiving Inventory Depot');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.i_poh_id == '' )
				{
					alert('Enter Transfer Order ID');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.s_comment == '' )
				{
					alert('Enter Comment');
					$b_stop_exec = true;
				}
				
			}
			
			
			/*
				check if details are ok
			*/
			if( $b_stop_exec == false )
			{
				var i_counter = 0;
				for( o_counter in o_form.o_order_detail)
				{
					i_counter++;
				}
				
				/**/
				if( i_counter == 0 )
				{
					alert('Add Detail to your Order');
					$b_stop_exec = true;
				}
				
				/**/
				o_form.load_order_detail_ux();
			}
			
			
			/*
				if( $b_stop_exec == false )
				{
					o_form.o_order.header = o_form.o_order_header
					o_form.o_order.detail = o_form.o_order_detail
					console.log(o_form.o_order);
					alert( JSON.stringify(o_form.o_order) );
					alert( o_form.o_order );
					
					return false;
				}
			*/
			
			
			/**/
			if( $b_stop_exec == false )
			{
				$.ajax({
					url: 'received_purchase_order/create_ajx',
					dataType: "json",
					type: "POST",
					data: o_form.o_frm_received_purchase_order_create.serialize(),
					beforeSend: function(){
					},
					success: function( o_result ) {
						
						o_form.o_td_siteresponse_cls.html( o_result.s_view_site_responses );
						
						if( o_result.s_result == 'success' )
						{
							o_form.o_btn_received_purchase_order_submit.remove();
							$('.clstbl_basiccreateadddetail_1').remove();
							$('.btn_received_purchase_order_remove_order_detail_cls').remove();
						}
						else
						{}
					}
				});
			}
		}
		
		
	};
	
	
	o_form.init();
	
	
	o_form.o_txt_received_purchase_order_product_select_quantity.keypress(function (e) {
		if( e.which == 48 && ( o_form.o_txt_received_purchase_order_product_select_quantity.val() == '' || o_form.o_txt_received_purchase_order_product_select_quantity.val() == 0 ) )	
		{ return false; }
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
		{ return false; }
	});
	
	
	o_form.o_txt_received_purchase_order_product_select_quantity.keyup(function (e) {
		if( o_form.o_txt_received_purchase_order_product_select_quantity.val() == 0 || o_form.o_txt_received_purchase_order_product_select_quantity.val() < 0 )
		{ o_form.o_txt_received_purchase_order_product_select_quantity.val('')	; }
	});
	
	
	o_form.o_btn_received_purchase_order_product_select_add.on( "click", function() {
		o_form.add_order_detail();
		return false;
	});
	
	
	// ..oh how i miss .live
	$(document).on('click', '.btn_received_purchase_order_remove_order_detail_cls', function() {
		//
		delete o_form.o_order_detail[$(this).attr('i_remove_p_id') +"_"+ $(this).attr('s_remove_p_serial')];
		
		//
		o_form.load_order_detail_ux();
		
		return false;
	}); 
	
	
	o_form.o_btn_received_purchase_order_submit.on( "click", function(){
		
		o_form.create_order();
		
		return false;
	});

	
	
});
