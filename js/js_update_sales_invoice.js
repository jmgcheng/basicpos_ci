$(document).ready(function($){
	
	var o_form = {
		o_products : new Object,
		a_products : new Array,
		o_order : new Object,
		o_order_header : new Object,
		o_order_detail : new Object,
		i_sales_invoice_id : $('#frm_sales_invoice_update').attr('i_sales_invoice_id'),
		o_frm_sales_invoice_update : $('#frm_sales_invoice_update'),
		o_btn_sales_invoice_submit : $('#btn_sales_invoice_submit'),
		o_opt_sales_invoice_releasing_depot : $('#opt_sales_invoice_releasing_depot'),
		o_txt_sales_invoice_title : $('#txt_sales_invoice_title'),
		o_txt_sales_invoice_comment : $('#txt_sales_invoice_comment'),
		o_opt_sales_invoice_product_select : $('#opt_sales_invoice_product_select'),
		o_txt_sales_invoice_product_select_quantity : $('#txt_sales_invoice_product_select_quantity'),
		o_btn_sales_invoice_product_select_add : $('#btn_sales_invoice_product_select_add'),
		o_tbody_sales_invoice_details_cls : $('.tbody_sales_invoice_details_cls'),
		o_div_sales_invoice_form_asset_id : $('#div_sales_invoice_form_asset_id'),
		o_td_siteresponse_cls : $('.td_siteresponse_cls'),
		
		o_txt_sales_invoice_details_template_json_id : $('#txt_sales_invoice_details_template_json_id'),
		
		
		o_clstbl_basicupdatedetail_1 : $('.clstbl_basicupdatedetail_1'),
		
		
		init : function(){

			o_form.set_products();
			
			o_form.purge_select_products();
			
			o_form.purge_order_details();
			
			o_form.load_order_detail_ux();
			
		}, 
		
		
		set_products : function(){
			$.ajax({
				url: 'product/read_all_ajx',
				dataType: "json",
				type: "POST",
				data: '',
				beforeSend: function(){
				},
				success: function( o_result ) {

					if( o_result.s_result == 'success' )
					{
						o_form.o_products = o_result.a_product_result;
						for( o_counter in o_form.o_products)
						{ o_form.a_products[o_form.o_products[o_counter].i_p_id] = o_form.o_products[o_counter]; }
						
						o_form.populate_select_products();
						
						o_form.set_sales_invoice_details();
					}
					else
					{
						
					}
					
				}
			});
		},
		
		
		set_sales_invoice_details : function(){
			var o_temp_object = new Object;
			
			if( o_form.o_txt_sales_invoice_details_template_json_id.val() == '' )
			{
				alert('No Details Found');
			}
			
			try
			{
				o_temp_object = jQuery.parseJSON( o_form.o_txt_sales_invoice_details_template_json_id.val() );
				//console.log( o_temp_object );
				
				for( o_counter in o_temp_object)
				{
					o_form.o_order_detail[o_temp_object[o_counter].i_sid_p_id] = o_form.a_products[o_temp_object[o_counter].i_sid_p_id];
					o_form.o_order_detail[o_temp_object[o_counter].i_sid_p_id].i_order_quantity = o_temp_object[o_counter].i_sid_quantity_to_release;
				}

				/*
				*/
				o_form.load_order_detail_ux();
				
				/*
				*/
				o_form.o_txt_sales_invoice_details_template_json_id.html('');
			}
			catch(e)
			{
				alert('Order Details Problem');
			}
			
		},
		
		
		purge_select_products : function(){
			o_form.o_opt_sales_invoice_product_select.find('option').remove();
		},
		
		
		purge_order_details : function(){
			o_form.o_tbody_sales_invoice_details_cls.html('');
		},
		
		
		populate_select_products : function(){
			
			/**/
			o_form.purge_select_products();
			
			/**/
			for( var i_counter = 0; i_counter < o_form.o_products.length; i_counter++ )
			{
				/*
					console.log( o_form.o_products[i_counter] );
				*/
				
				//
				o_form.o_opt_sales_invoice_product_select.append('<option value="'+o_form.o_products[i_counter].i_p_id+'">' + o_form.o_products[i_counter].s_p_name + ' - ' + o_form.o_products[i_counter].s_p_model + ' - ' + o_form.o_products[i_counter].s_pd_name + ' - ' + o_form.o_products[i_counter].s_pc_name + ' - ' + o_form.o_products[i_counter].s_pu_name + '</option>');
			}
		},
		
		
		add_order_detail : function(){
			$b_stop_exec = false;
			$b_found_product = false;
			
			
			/*
				Check if a Product Detail is selected
			*/
			if( $b_stop_exec == false )
			{
				if( o_form.o_opt_sales_invoice_product_select.val() == '' )
				{ 
					$b_stop_exec = true;
					alert('Select Product Detail'); 
				}
			}
			
			
			/*
				Check if quantity order was entered
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_txt_sales_invoice_product_select_quantity.val(o_form.o_txt_sales_invoice_product_select_quantity.val().replace(/^0+/, ''));
				if( o_form.o_txt_sales_invoice_product_select_quantity.val() == '' || o_form.o_txt_sales_invoice_product_select_quantity.val() <= 0 )
				{ 
					$b_stop_exec = true;
					alert('Enter Quantity'); 
				}
			}
			
			
			/*
			*/
			if( $b_stop_exec == false )
			{
				
				try
				{
					o_form.o_order_detail[o_form.o_opt_sales_invoice_product_select.val()] = o_form.a_products[o_form.o_opt_sales_invoice_product_select.val()];
					o_form.o_order_detail[o_form.o_opt_sales_invoice_product_select.val()].i_order_quantity = o_form.o_txt_sales_invoice_product_select_quantity.val();
				}
				catch(e)
				{
					if (o_form.a_products[o_form.o_opt_sales_invoice_product_select.val()]) 
					{}
					else
					{
						$b_stop_exec = true;
						alert('Product NOT Found'); 
					}
				}

			}
			
			
			/*
			*/
			o_form.load_order_detail_ux();
			
		},
		
		
		load_order_detail_ux : function(){
			
			/**/
			o_form.purge_order_details();
			
			/**/
			var i_counter = 0;
			for( o_counter in o_form.o_order_detail)
			{
				/*
					ready asset
				*/
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_p_id]').attr('value', o_form.o_order_detail[o_counter].i_p_id);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_p_name]').attr('value', o_form.o_order_detail[o_counter].s_p_name);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_p_model]').attr('value', o_form.o_order_detail[o_counter].s_p_model);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pd_name]').attr('value', o_form.o_order_detail[o_counter].s_pd_name);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pc_name]').attr('value', o_form.o_order_detail[o_counter].s_pc_name);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pu_name]').attr('value', o_form.o_order_detail[o_counter].s_pu_name);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_order_quantity]').attr('value', o_form.o_order_detail[o_counter].i_order_quantity);
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', o_form.o_order_detail[o_counter].i_p_id);
				
				/*
					append asset
				*/
				o_form.o_tbody_sales_invoice_details_cls.append( o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id').html() );

				/*
					clean asset
				*/
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_p_id]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_p_name]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_p_model]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pd_name]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pc_name]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[s_pu_name]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_order_quantity]').attr('value', '');
				o_form.o_div_sales_invoice_form_asset_id.find('tbody#tbody_sales_invoice_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', '');
				
				i_counter++;
			}
			
			/**/
			if( i_counter == 0 )
			{
				o_form.o_clstbl_basicupdatedetail_1.hide();
			}
			else
			{
				o_form.o_clstbl_basicupdatedetail_1.show();
			}
			
		},
		
		
		update_order : function(){
			$b_stop_exec = false;
			
			
			/*
				check header is ok
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_order_header.i_sid_id = o_form.o_opt_sales_invoice_releasing_depot.val();
				o_form.o_order_header.s_sid_title = o_form.o_txt_sales_invoice_title.val();
				o_form.o_order_header.s_sid_comment = o_form.o_txt_sales_invoice_comment.val();
				
				if( o_form.o_order_header.i_sid_id == '' )
				{
					alert('Select Releasing Inventory Depot');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.s_sid_title == '' )
				{
					alert('Order Title Needed');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.s_sid_order_description == '' )
				{
					alert('Order Description Needed');
					$b_stop_exec = true;
				}
			}
			
			
			/*
				check if details are ok
			*/
			if( $b_stop_exec == false )
			{
				var i_counter = 0;
				for( o_counter in o_form.o_order_detail)
				{
					i_counter++;
				}
				
				/**/
				if( i_counter == 0 )
				{
					alert('Add Detail to your Order');
					$b_stop_exec = true;
				}
				
				/**/
				o_form.load_order_detail_ux();
			}
			
			
			/*
				if( $b_stop_exec == false )
				{
					o_form.o_order.header = o_form.o_order_header
					o_form.o_order.detail = o_form.o_order_detail
					console.log(o_form.o_order);
					alert( JSON.stringify(o_form.o_order) );
					alert( o_form.o_order );
					
					return false;
				}
			*/
			
			
			/**/
			if( $b_stop_exec == false )
			{
				$.ajax({
					url: 'sales_invoice/update_ajx/sales_invoice_id/' + o_form.i_sales_invoice_id, 
					dataType: "json",
					type: "POST",
					data: o_form.o_frm_sales_invoice_update.serialize(),
					beforeSend: function(){
					},
					success: function( o_result ) {
						
						o_form.o_td_siteresponse_cls.html( o_result.s_view_site_responses );
						
						if( o_result.s_result == 'success' )
						{
							o_form.o_btn_sales_invoice_submit.remove();
							$('.clstbl_basicupdateadddetail_1').remove();
							$('.btn_sales_invoice_remove_order_detail_cls').remove();
						}
						else
						{}
					}
				});
			}
		}
		
		
	};
	
	
	o_form.init();
	
	
	o_form.o_txt_sales_invoice_product_select_quantity.keypress(function (e) {
		if( e.which == 48 && ( o_form.o_txt_sales_invoice_product_select_quantity.val() == '' || o_form.o_txt_sales_invoice_product_select_quantity.val() == 0 ) )	
		{ return false; }
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
		{ return false; }
	});
	
	
	o_form.o_txt_sales_invoice_product_select_quantity.keyup(function (e) {
		if( o_form.o_txt_sales_invoice_product_select_quantity.val() == 0 || o_form.o_txt_sales_invoice_product_select_quantity.val() < 0 )
		{ o_form.o_txt_sales_invoice_product_select_quantity.val('')	; }
	});
	
	
	o_form.o_btn_sales_invoice_product_select_add.on( "click", function() {
		o_form.add_order_detail();
		return false;
	});
	
	
	// ..oh how i miss .live
	$(document).on('click', '.btn_sales_invoice_remove_order_detail_cls', function() {
		
		//
		delete o_form.o_order_detail[$(this).attr('i_remove_p_id')];
		//
		o_form.load_order_detail_ux();
		
		return false;
	}); 
	
	
	o_form.o_btn_sales_invoice_submit.on( "click", function(){
		
		o_form.update_order();
		
		return false;
	});

	
	
});
