$(document).ready(function($){
	
	var o_form = {
		o_products : new Object,
		a_products : new Array,
		o_order : new Object,
		o_order_header : new Object,
		o_order_detail : new Object,
		o_frm_purchase_order_create : $('#frm_purchase_order_create'),
		o_btn_purchase_order_submit : $('#btn_purchase_order_submit'),
		o_opt_purchase_order_receiving_depot : $('#opt_purchase_order_receiving_depot'),
		o_txt_purchase_order_title : $('#txt_purchase_order_title'),
		o_txt_purchase_order_description : $('#txt_purchase_order_description'),
		o_txt_purchase_order_comment : $('#txt_purchase_order_comment'),
		o_opt_purchase_order_product_select : $('#opt_purchase_order_product_select'),
		o_txt_purchase_order_product_select_quantity : $('#txt_purchase_order_product_select_quantity'),
		o_btn_purchase_order_product_select_add : $('#btn_purchase_order_product_select_add'),
		o_tbody_purchase_order_details_cls : $('.tbody_purchase_order_details_cls'),
		o_div_purchase_order_form_asset_id : $('#div_purchase_order_form_asset_id'),
		o_td_siteresponse_cls : $('.td_siteresponse_cls'),
		
		o_clstbl_basiccreatedetail_1 : $('.clstbl_basiccreatedetail_1'),
		
		
		init : function(){

			o_form.set_products();
			
			o_form.purge_select_products();
			
			o_form.purge_order_details();
			
			o_form.load_order_detail_ux();
		}, 
		
		
		set_products : function(){
			$.ajax({
				url: 'product/read_all_ajx',
				dataType: "json",
				type: "POST",
				data: '',
				beforeSend: function(){
				},
				success: function( o_result ) {

					if( o_result.s_result == 'success' )
					{
						o_form.o_products = o_result.a_product_result;
						for( o_counter in o_form.o_products)
						{ o_form.a_products[o_form.o_products[o_counter].i_p_id] = o_form.o_products[o_counter]; }
						
						o_form.populate_select_products();
					}
					else
					{
						
					}
					
				}
			});
		},
		
		
		purge_select_products : function(){
			o_form.o_opt_purchase_order_product_select.find('option').remove();
		},
		
		
		purge_order_details : function(){
			o_form.o_tbody_purchase_order_details_cls.html('');
		},
		
		
		populate_select_products : function(){
			
			/**/
			o_form.purge_select_products();
			
			/**/
			for( var i_counter = 0; i_counter < o_form.o_products.length; i_counter++ )
			{
				/*
					console.log( o_form.o_products[i_counter] );
				*/
				
				//
				o_form.o_opt_purchase_order_product_select.append('<option value="'+o_form.o_products[i_counter].i_p_id+'">' + o_form.o_products[i_counter].s_p_name + ' - ' + o_form.o_products[i_counter].s_p_model + ' - ' + o_form.o_products[i_counter].s_pd_name + ' - ' + o_form.o_products[i_counter].s_pc_name + ' - ' + o_form.o_products[i_counter].s_pu_name + '</option>');
			}
		},
		
		
		add_order_detail : function(){
			$b_stop_exec = false;
			$b_found_product = false;
			
			
			/*
				Check if a Product Detail is selected
			*/
			if( $b_stop_exec == false )
			{
				if( o_form.o_opt_purchase_order_product_select.val() == '' )
				{ 
					$b_stop_exec = true;
					alert('Select Product Detail'); 
				}
			}
			
			
			/*
				Check if quantity order was entered
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_txt_purchase_order_product_select_quantity.val(o_form.o_txt_purchase_order_product_select_quantity.val().replace(/^0+/, ''));
				if( o_form.o_txt_purchase_order_product_select_quantity.val() == '' || o_form.o_txt_purchase_order_product_select_quantity.val() <= 0 )
				{ 
					$b_stop_exec = true;
					alert('Enter Quantity'); 
				}
			}
			
			
			/*
			*/
			if( $b_stop_exec == false )
			{
				/*
				for( var i_counter = 0; i_counter < o_form.o_products.length; i_counter++ )
				{
					if( o_form.o_opt_purchase_order_product_select.val() == o_form.o_products[i_counter].i_p_id )
					{
						$b_found_product = true;
						$o_new_data = new Object;
						
						$o_new_data.i_p_id = o_form.o_products[i_counter].i_p_id;
						$o_new_data.s_p_name = o_form.o_products[i_counter].s_p_name;
						$o_new_data.s_p_model = o_form.o_products[i_counter].s_p_model;
						$o_new_data.s_pd_name = o_form.o_products[i_counter].s_pd_name;
						$o_new_data.s_pc_name = o_form.o_products[i_counter].s_pc_name;
						$o_new_data.s_pu_name = o_form.o_products[i_counter].s_pu_name;
						$o_new_data.i_order_quantity = o_form.o_txt_purchase_order_product_select_quantity.val();
						
						o_form.o_order_detail[$o_new_data.i_p_id] = $o_new_data;
						
					}
				}
				
				if( !$b_found_product )
				{
					$b_stop_exec = true;
					alert('Product NOT Found'); 
				}
				*/
				
				try
				{
					o_form.o_order_detail[o_form.o_opt_purchase_order_product_select.val()] = o_form.a_products[o_form.o_opt_purchase_order_product_select.val()];
					o_form.o_order_detail[o_form.o_opt_purchase_order_product_select.val()].i_order_quantity = o_form.o_txt_purchase_order_product_select_quantity.val();
				}
				catch(e)
				{
					if (o_form.a_products[o_form.o_opt_purchase_order_product_select.val()]) 
					{}
					else
					{
						$b_stop_exec = true;
						alert('Product NOT Found'); 
					}
				}
			}
			
			
			/*
			*/
			o_form.load_order_detail_ux();
			
		},
		
		
		load_order_detail_ux : function(){
			
			/**/
			o_form.purge_order_details();
			
			/**/
			var i_counter = 0;
			for( o_counter in o_form.o_order_detail)
			{
				/*
					ready asset
				*/
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_p_id]').attr('value', o_form.o_order_detail[o_counter].i_p_id);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_p_name]').attr('value', o_form.o_order_detail[o_counter].s_p_name);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_p_model]').attr('value', o_form.o_order_detail[o_counter].s_p_model);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pd_name]').attr('value', o_form.o_order_detail[o_counter].s_pd_name);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pc_name]').attr('value', o_form.o_order_detail[o_counter].s_pc_name);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pu_name]').attr('value', o_form.o_order_detail[o_counter].s_pu_name);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_order_quantity]').attr('value', o_form.o_order_detail[o_counter].i_order_quantity);
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', o_form.o_order_detail[o_counter].i_p_id);
				
				/*
					append asset
				*/
				o_form.o_tbody_purchase_order_details_cls.append( o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id').html() );

				/*
					clean asset
				*/
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_p_id]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_p_name]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_p_model]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pd_name]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pc_name]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[s_pu_name]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_order_quantity]').attr('value', '');
				o_form.o_div_purchase_order_form_asset_id.find('tbody#tbody_purchase_order_details_template_id input[i_remove_p_id]').attr('i_remove_p_id', '');
				
				i_counter++;
			}
			
			/**/
			if( i_counter == 0 )
			{
				o_form.o_clstbl_basiccreatedetail_1.hide();
			}
			else
			{
				o_form.o_clstbl_basiccreatedetail_1.show();
			}
			
		},
		
		
		create_order : function(){
			$b_stop_exec = false;
			
			
			/*
				check header is ok
			*/
			if( $b_stop_exec == false )
			{
				o_form.o_order_header.i_poh_id_id = o_form.o_opt_purchase_order_receiving_depot.val();
				o_form.o_order_header.s_poh_title = o_form.o_txt_purchase_order_title.val();
				o_form.o_order_header.s_poh_order_description = o_form.o_txt_purchase_order_description.val();
				o_form.o_order_header.s_poh_comment = o_form.o_txt_purchase_order_comment.val();
				
				if( o_form.o_order_header.i_poh_id_id == '' )
				{
					alert('Select Receiving Inventory Depot');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.s_poh_title == '' )
				{
					alert('Order Title Needed');
					$b_stop_exec = true;
				}
				
				if( o_form.o_order_header.s_poh_order_description == '' )
				{
					alert('Order Description Needed');
					$b_stop_exec = true;
				}
			}
			
			
			/*
				check if details are ok
			*/
			if( $b_stop_exec == false )
			{
				var i_counter = 0;
				for( o_counter in o_form.o_order_detail)
				{
					i_counter++;
				}
				
				/**/
				if( i_counter == 0 )
				{
					alert('Add Detail to your Order');
					$b_stop_exec = true;
				}
				
				/**/
				o_form.load_order_detail_ux();
			}
			
			
			/*
				if( $b_stop_exec == false )
				{
					o_form.o_order.header = o_form.o_order_header
					o_form.o_order.detail = o_form.o_order_detail
					console.log(o_form.o_order);
					alert( JSON.stringify(o_form.o_order) );
					alert( o_form.o_order );
					
					return false;
				}
			*/
			
			
			/**/
			if( $b_stop_exec == false )
			{
				$.ajax({
					url: 'purchase_order/create_ajx',
					dataType: "json",
					type: "POST",
					data: o_form.o_frm_purchase_order_create.serialize(),
					beforeSend: function(){
					},
					success: function( o_result ) {
						
						o_form.o_td_siteresponse_cls.html( o_result.s_view_site_responses );
						
						if( o_result.s_result == 'success' )
						{
							o_form.o_btn_purchase_order_submit.remove();
							$('.clstbl_basiccreateadddetail_1').remove();
							$('.btn_purchase_order_remove_order_detail_cls').remove();
						}
						else
						{}
					}
				});
			}
		}
		
		
	};
	
	
	o_form.init();
	
	
	o_form.o_txt_purchase_order_product_select_quantity.keypress(function (e) {
		if( e.which == 48 && ( o_form.o_txt_purchase_order_product_select_quantity.val() == '' || o_form.o_txt_purchase_order_product_select_quantity.val() == 0 ) )	
		{ return false; }
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
		{ return false; }
	});
	
	
	o_form.o_txt_purchase_order_product_select_quantity.keyup(function (e) {
		if( o_form.o_txt_purchase_order_product_select_quantity.val() == 0 || o_form.o_txt_purchase_order_product_select_quantity.val() < 0 )
		{ o_form.o_txt_purchase_order_product_select_quantity.val('')	; }
	});
	
	
	o_form.o_btn_purchase_order_product_select_add.on( "click", function() {
		o_form.add_order_detail();
		return false;
	});
	
	
	// ..oh how i miss .live
	$(document).on('click', '.btn_purchase_order_remove_order_detail_cls', function() {
		
		//
		delete o_form.o_order_detail[$(this).attr('i_remove_p_id')];
		//
		o_form.load_order_detail_ux();
		
		return false;
	}); 
	
	
	o_form.o_btn_purchase_order_submit.on( "click", function(){
		
		o_form.create_order();
		
		return false;
	});

	
	
});
